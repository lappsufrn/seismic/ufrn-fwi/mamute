\page overview Overview

# 1. Introduction

Mamute is a set of seismic application built to be executed in large scale systems. The main applications which requires high performance are written in C++ language, while other minor applications are written in Python. To improve performance, Mamute implements high performance techniques using well known tools like OpenMP, MPI, and CUDA. 

> For a full Mamute example, please see [`Quick start`](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#quick-start). For more detailed information access the [`Mamute website`](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/).

# 2. Table of Content

|       **Main Applications**      |
|:--------------------------------:|
| 3D Acoustic Modeling             |
| 3D Full Waveform Inversion (FWI) |

|            **HPC Techniques**           | **Available for** |
|:---------------------------------------:|-------------------|
| Autotuning OpenMP scheduling parameters | Modeling, FWI     |
| Wavedfield checkpointing                | FWI               |
| Fault tolerance                         | FWI               |
| MPI distributed shots scheduling        | Modeling, FWI     |
| Sphere modeling                         | Modeling, FWI     |


# 3. Requirements

A C++11 compatible compiler is required. The library has been tested to compile with (but is not necessarily limited to):

* CMake >= 3.5
* GCC >= 6.4.0
* OpenMPI >= 2.1.1

### Optional Requirements
<a id="optional-requirements">

As an optional requirements, if you would like to use the provided scripts to generate input, you will also need:

* Python3 >= 3.6
* Python3-pip

And install the python packages specified in [./scripts/requirements.txt](./scripts/requirements.txt). This can be done after you installed python3 and pip using the following command:
```sh
$ python -m pip install --user -r ./scripts/requirements.txt
```

# 4. Installation

## 4.1. Clone Project

```sh
$ export MAMUTE=$(pwd)/mamute
$ git clone -b master https://gitlab.com/ufrn-fwi/mamute.git $MAMUTE
```

You can also download it directly from Gitlab. More details on how to use git can be found at its official [doc webpage](https://git-scm.com/doc).

**note:** the `dev` branch should be used instead of `master`

## 4.2. Compile and Install

The following steps are an example of compiling the 3D-FWI.

```sh
$ cd $MAMUTE
$ cmake -B build -DCMAKE_INSTALL_PREFIX=$MAMUTE
$ make -j 4 -C build install
```

> **Note 1:** all the compilation at this example uses **G++/GNU** compiler and enables all warning messages (e.g. `-Wall`). <br>
> **Note 2:** the number after `-j` should be the number of cores in your machine. <br>
> **Note 3:** to install to the system default directory (e.g. `/usr/local`), just remove `-DCMAKE_INSTALL_PREFIX=$MAMUTE` option.

## 4.3. Compilation Options
<a id="compilation-options">

You can customize the compilation process to your requirements.

Option name  | Default value | Description
-------------|---------------|------------
NATIVE       | ON            | Enable code generation for highest instruction set available on the compilation host processor.
OPTIMIZATION | OFF           | Enable aggressive optimization.
OPENMP       | ON            | Enable OpenMP pragma inside FWI.
VERBOSE      | OFF           | Enable verbose mode.
TEST         | OFF           | Enable test mode.
FREE_SURFACE | OFF           | Enable free surface.
SPHERE       | OFF           | Enable sphere modeling.
FT           | OFF           | Enable Fault Tolerance. ([More details](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/hpc.html#fault-tolerance))
AUTOTUNING   | OFF           | Enable Auto-tuning. ([More details](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/hpc.html#autotuning.md))
BORDER       | DAMPING       | DAMPING enables damping absorbing boundary <br> CPML enables Convolutional Perfectly Matched Layer absorbing boundary.
WAVEFIELD_MANAGEMENT | MEMORY| Forward wavefield management. <br> - DISK : store on disk <br> - CHECKPOINTING : optimal checkpointing <br> - MEMORY : store in memory.
DENSITY      | OFF           | Enable the modeling with density

For example, to disable the `NATIVE` option and enable `VERBOSE` option, you should use this command:
```sh
$ cmake -B build -DCMAKE_INSTALL_PREFIX=$MAMUTE -DNATIVE=OFF -DVERBOSE=ON
```

### 4.3.1. Creating a binary file for synthetic modeling the data

In order to create the binary `modeling`, used for modeling synthetic data, the user must enable test mode, as in:
```sh
$ cmake -B build -DCMAKE_INSTALL_PREFIX=$MAMUTE -DTEST=ON
```
The binary is created on the folder `$MAMUTE/bin`.


### 4.3.2. Using another compiler

To select another compiler, you must set the `CC` and `CXX` variables. For example, you can use the command below to choose Intel(R) C/C++ Compiler:
```sh
$ CC=icc CXX=icpc cmake ../ -DCMAKE_INSTALL_PREFIX=$MAMUTE
```


### 4.3.3. Compiling for debug

To remove any optimization option from the compilation and enable debug, you can use the command below:
```sh
$ cmake -B build -DCMAKE_INSTALL_PREFIX=$MAMUTE -DCMAKE_BUILD_TYPE=Debug
```

### 4.3.4. Adding more compiler flags

You can also add more compiler flags using `CFLAGS` and `CXXFLAGS` variables. For example, the command below enable 'Release' version with debug symbol and
profile information:
```sh
$ CFLAGS='-g -pg' CXXFLAGS=$CFLAGS cmake ../ -DCMAKE_INSTALL_PREFIX=$MAMUTE
```

# 5. How to Use

For a full Mamute example, please see [`Quick start`](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#quick-start). For more detailed information access the [`Mamute website`](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/).