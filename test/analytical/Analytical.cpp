#include "Config.h"
#include "Grid.h"
#include "SeismicData.h"
#include "Utils.h"
#include "utilities/BinaryFileHandler.h"
#include <fstream>
#include <iostream>

#define FILENAME_ANALYTICAL_RESULT "analytical_#.bin"

double ricker(double t, double fpeak);
void AnalyticalTrace(std::string proj_dir, double dt, double pf, double v, double R, int nt,
                     int ishot);
double DistanceSourceReceiver(SeismicData *src, SeismicData *rcv, Grid *grid);

int main(int argc, char *argv[]) {

  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " <config_file>" << std::endl;
    return 1;
  }

  try {
    Config *config = Config::ReadConfigFile(argc, argv);
    SimpleConfigTextFile *sconfig = static_cast<SimpleConfigTextFile *>(config);
    long dataLength = 0;
    std::string proj_dir = config->Get("proj_dir");
    int nsrc = sconfig->Get<double>("n_src");
    int *rcv_per_shot = nullptr;   /// number of receivers
    int ns = sconfig->Get<double>("ns");
    int max_rcv;
    double fpeak = sconfig->Get<double>("fpeak");
    double dt = sconfig->Get<double>("dt");
    double vel = sconfig->Get<double>("vel");
    double raio;

    // Compute number of receivers from files
    try {
      // cppcheck-suppress noOperatorEq
      rcv_per_shot = new int[nsrc];
    } catch (const std::exception &e) {
      std::string s("rcv_per_shot allocation failed: ");
      throw s + e.what();
    }
    max_rcv = 0;
    BinaryFileHandler fileHandler(proj_dir);
    for (int i = 0; i < nsrc; i++) {
      // Get number of data of type double in the `rcv_coord_i.bin`
      dataLength = fileHandler.CountData<double>(FILENAME_RECEIVER_COORDINATES, i);

      rcv_per_shot[i] = dataLength / 3;
      if (rcv_per_shot[i] > max_rcv) {
        max_rcv = rcv_per_shot[i];
      }
    }

    double ox, oy, oz, dx, dy, dz;
    int nx, ny, nz, border, stencil;
    ox = sconfig->Get<double>("ox");
    oy = sconfig->Get<double>("oy");
    oz = sconfig->Get<double>("oz");
    dx = sconfig->Get<double>("dx");
    dy = sconfig->Get<double>("dy");
    dz = sconfig->Get<double>("dz");
    nx = sconfig->Get<int>("nx");
    ny = sconfig->Get<int>("ny");
    nz = sconfig->Get<int>("nz");
    border = sconfig->Get<int>("border");
    stencil = sconfig->Get<int>("stencil");
    checkInputData(FILENAME_SOURCE_COORDINATES, FILENAME_RECEIVER_COORDINATES, proj_dir,
                   rcv_per_shot, ox, oy, oz, dx, dy, dz, nx, ny, nz, nsrc);

    Grid *grid = new Grid(stencil, dx, dy, dz, border, nx, ny, nz);
    SeismicData *dobs = new SeismicData(max_rcv, ns, dt);
    SeismicData *src = new SeismicData(1, ns, dt);
    SL *location = new SL(nsrc);
    location->setPositionFromFile(FILENAME_SOURCE_COORDINATES, proj_dir, ox, oy, oz, dx, dy, dz,
                                  nsrc);
    src->x[0] = location->x[0];
    src->y[0] = location->y[0];
    src->z[0] = location->z[0];
    src->ReadSourceFromFile(config->Get("proj_dir"));

    for (int ishot = 0; ishot < nsrc; ishot++) {
      std::cout << "MODELING SHOT: " << ishot << std::endl;
      src->x[0] = location->x[ishot];
      src->y[0] = location->y[ishot];
      src->z[0] = location->z[ishot];
      dobs->setPositionFromFile(proj_dir, ox, oy, oz, dx, dy, dz, ishot, rcv_per_shot[ishot]);
      raio = DistanceSourceReceiver(dobs, src, grid);
      AnalyticalTrace(proj_dir, dt, fpeak, vel, raio, ns, ishot);
    }

    delete[] rcv_per_shot;
    delete dobs;
    delete src;
    delete location;
    delete grid;

  } catch (std::exception &e) {
    std::cerr << "Exception caught: " << e.what() << std::endl;
    return -1;
  } catch (std::string &e) {
    std::cerr << "Exception caught: " << e << std::endl;
    return -1;
  } catch (const char *e) {
    std::cerr << "Exception caught: " << e << std::endl;
    return -1;
  } catch (...) {
    std::cerr << "Unknown error running FWI@main()!" << std::endl;
    return -1;
  }

  return 0;
}

void AnalyticalTrace(std::string proj_dir, double dt, double pf, double v, double R, int nt,
                     int ishot) {
  double t0, *s;

  // Initialize values
  s = new double[nt];   // computation of analytical acoustic wave
  t0 = R / v;

  for (int i = 0; i < nt; i++) {
    double ti = (i + 1) * dt - t0;
    s[i] = ricker(ti, pf) / (4 * M_PI * R);
  }

  BinaryFileHandler fileHandler(proj_dir);
  fileHandler.WriteFile<double>(FILENAME_ANALYTICAL_RESULT, ishot, s, nt);

  delete[] s;
}

double ricker(double t, double fpeak) {   // Ricker
  double x, xx, tdelay = 1.0 / fpeak;
  double source;

  x = M_PI * fpeak * (t - tdelay);
  xx = x * x;
  source = exp(-xx) * (1.0 - 2.0 * xx);
  return source;
}

/**
  Calculate the distance between source and receiver.
  Consider only the first source and receiver informed.
  @param *src Source data.
  @param *rcv Receiver data.
  @param *grid Grid data, for spacing.
  @return Distance, in meters, between source and receiver.
 */
double DistanceSourceReceiver(SeismicData *src, SeismicData *rcv, Grid *grid) {
  double distance = 0;

  double x_temp = src->x[0] * grid->dx - rcv->x[0] * grid->dx;
  double y_temp = src->y[0] * grid->dy - rcv->y[0] * grid->dy;
  double z_temp = src->z[0] * grid->dz - rcv->z[0] * grid->dz;

  distance = sqrt(x_temp * x_temp + y_temp * y_temp + z_temp * z_temp);

  return distance;
}
