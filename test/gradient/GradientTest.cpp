#include "FWI.h"
#include "SimpleConfigTextFile.h"
#include <exception>
#include <iostream>

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " <config_file>" << std::endl;
    return 1;
  }
  try {
    int id, comm_sz, provided;
    Config *config = Config::ReadConfigFile(argc, argv);
    MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    int n_point = 10;
    std::string proj_dir = config->Get("proj_dir");
    std::string velocity_filename = config->Get("vel");
    std::string velocity_filename_plus(velocity_filename + "_plus");
    std::string velocity_filename_minus(velocity_filename + "_minus");
    double *gradientResult = new double[n_point]();
    double *gradientAprox = new double[n_point]();
    double epsilon = 5;   // 5 m/s
    SimpleConfigTextFile *sconfig = static_cast<SimpleConfigTextFile *>(config);
    int stencil = sconfig->Get<int>("stencil");
    double dx = sconfig->Get<double>("dx");
    double dy = sconfig->Get<double>("dy");
    double dz = sconfig->Get<double>("dz");
    double ox = sconfig->Get<double>("ox");
    double oy = sconfig->Get<double>("oy");
    double oz = sconfig->Get<double>("oz");
    double fpeak = sconfig->Get<double>("fpeak");
    int n_src = sconfig->Get<int>("n_src");
    int nx = sconfig->Get<int>("nx");
    int ny = sconfig->Get<int>("ny");
    int nz = sconfig->Get<int>("nz");
    int border = sconfig->Get<int>("border");
    double dt = sconfig->Get<double>("dt");
    double ns = sconfig->Get<double>("ns");
    int gradient_preconditioning_mode = sconfig->Get<int>("gradient_preconditioning_mode");
    int ws_flag = sconfig->Get<int>("ws_flag");
    Grid *grid = new Grid(stencil, dx, dy, dz, border, nx, ny, nz);
    int zp = 15;
    int start = 10;
    for (int i = 0; i < n_point; i++) {
      int oeps = (start + i * grid->ony + start + i) * grid->onz + zp;

      // Velocity Model plus perturbation
      VelocityModel *model_plus = new VelocityModel(proj_dir, velocity_filename, *grid);
      model_plus->fOriginalModel[oeps] += epsilon;
      MyFile::Save(model_plus->fOriginalModel, (proj_dir + "/" + velocity_filename_plus).c_str(),
                   model_plus->getSize());

      // Velocity Model minus perturbation
      VelocityModel *model_minus = new VelocityModel(proj_dir, velocity_filename, *grid);
      model_minus->fOriginalModel[oeps] -= epsilon;
      MyFile::Save(model_minus->fOriginalModel, (proj_dir + "/" + velocity_filename_minus).c_str(),
                   model_minus->getSize());

      double *gradient = new double[nx * ny * nz]();
      double *gradient_minus = new double[nx * ny * nz]();
      double *gradient_plus = new double[nx * ny * nz]();

      double misfit;
      double misfit_minus;
      double misfit_plus;

      Adjoint::StaticRun(gradient, &misfit, proj_dir, velocity_filename, n_src, fpeak, ws_flag, ox,
                         oy, oz, dx, dy, dz, nx, ny, nz, border, stencil, ns, dt,
                         gradient_preconditioning_mode);
      Adjoint::StaticRun(gradient_plus, &misfit_plus, proj_dir, velocity_filename_plus, n_src,
                         fpeak, ws_flag, ox, oy, oz, dx, dy, dz, nx, ny, nz, border, stencil, ns,
                         dt, gradient_preconditioning_mode);
      Adjoint::StaticRun(gradient_minus, &misfit_minus, proj_dir, velocity_filename_minus, n_src,
                         fpeak, ws_flag, ox, oy, oz, dx, dy, dz, nx, ny, nz, border, stencil, ns,
                         dt, gradient_preconditioning_mode);

      if (id == 0) {
        gradientResult[i] = gradient[oeps];
        gradientAprox[i] = (misfit_plus - misfit_minus) / (2 * epsilon);
      }
      delete model_plus;
      delete model_minus;
      delete[] gradient;
      delete[] gradient_minus;
      delete[] gradient_plus;
    }
    if (id == 0) {
      MyFile::Save(gradientResult, "grad.bin", n_point);
      MyFile::Save(gradientAprox, "grad_aprox.bin", n_point);
    }
    delete[] gradientResult;
    delete[] gradientAprox;
    delete grid;

  } catch (std::exception &e) {
    std::cerr << "Exception caught: " << e.what() << std::endl;
    return -1;
  } catch (std::string &e) {
    std::cerr << "Exception caught: " << e << std::endl;
    return -1;
  } catch (const char *e) {
    std::cerr << "Exception caught: " << e << std::endl;
    return -1;
  } catch (...) {
    std::cerr << "Unknown error running FWI@main()!" << std::endl;
    return -1;
  }
  MPI_Finalize();
  return 0;
}