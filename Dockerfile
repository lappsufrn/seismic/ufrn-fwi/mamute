# Base image
FROM italoassis/cuda12-gcc12

# Set ambient variables
ENV CC=gcc
ENV CXX=g++

# Install dependencies
RUN apt-get update && \
  apt-get install -y \
  gfortran \
  libopenmpi-dev \
  openmpi-bin \
  cppcheck \
  clang-tidy \
  clang-format \
  fftw3 \
  fftw3-dev \
  pkg-config \
  python3 \
  python3-pip \
  valgrind \
  && ln -s /installdir/bin/gfortran /usr/local/bin/gfortran \
  && update-alternatives --install /usr/bin/gfortran gfortran /usr/local/bin/gfortran 999 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Sets work directory
WORKDIR /app
COPY . /app

# Install Python dependencies
RUN python3 -m pip install -r scripts/requirements.txt