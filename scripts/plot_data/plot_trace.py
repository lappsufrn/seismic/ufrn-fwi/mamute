import matplotlib.pyplot as plt
import numpy as np
import sys
import os
from typing import Optional
from .save_plot import save_plot

sys.path.insert(
    0,
    os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "plot_data")
    ),
)


def plot_trace(
    data,
    xlabel="index",
    ylabel="value",
    title="",
    save: bool = False,
    plot_filename: Optional[str] = None,
):
    fig, ax = plt.subplots()

    plt.title(title, fontsize=16)
    plt.xlabel(xlabel, fontsize=14)
    plt.ylabel(ylabel, fontsize=14)

    ax.set_xlim(left=0, right=len(data))

    ax.plot(data)

    if save:
        plot_filename = "trace" if not plot_filename else plot_filename
        save_plot(plot_filename)
        return

    plt.show()


if __name__ == "__main__":

    if len(sys.argv) >= 2:
        input_data = np.fromfile(sys.argv[1], np.float64)

        plot_trace(
            data=input_data,
            save="-s" in sys.argv,
            plot_filename=sys.argv[3] if len(sys.argv) >= 4 else None,
        )

    else:
        print("+----------------------------+")
        print("Parameters: ")
        print("  Path to data bin file with float64 values.")
        print("  Optional flag -s to save the plot instead of displaying it.")
        print("  Optional filename for the saved plot.")
        print("+----------------------------+")
