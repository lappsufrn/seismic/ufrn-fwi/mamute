import matplotlib.pyplot as plt
import numpy as np
import sys
from save_plot import save_plot
from typing import Optional

# flake8: noqa: F841


def plot_src_rcv(
    src_file_name,
    rcv_file_name,
    save: bool = False,
    plot_filename: Optional[str] = None,
):

    figPCA = plt.figure()
    ax = plt.axes(projection="3d")
    ax.set_title("Acquisiton map")
    a = np.fromfile(src_file_name, dtype=np.float64)
    nsrc = len(a) // 3
    src_coords = a.reshape((nsrc, 3))
    p = ax.scatter(
        src_coords[:, 0],
        src_coords[:, 1],
        src_coords[:, 2],
        s=50,
        c="red",
        label="sources",
    )
    a = np.fromfile(rcv_file_name, dtype=np.float64)
    nrec = len(a) // 3
    coords = a.reshape((nrec, 3))
    p = ax.scatter(
        coords[:, 0],
        coords[:, 1],
        coords[:, 2],
        s=50,
        c="green",
        label="receivers",
    )
    plt.legend(loc="best")

    if save:
        plot_filename = "src_rcv" if not plot_filename else plot_filename
        save_plot(plot_filename)
    else:
        plt.show()


if __name__ == "__main__":

    if len(sys.argv) >= 3:
        plot_src_rcv(
            sys.argv[1],
            sys.argv[2],
            save="-s" in sys.argv,
            plot_filename=sys.argv[4] if len(sys.argv) >= 5 else None,
        )
    else:
        print("+----------------------------+")
        print("Parameters: ")
        print("  Path to src bin")
        print("  Path to rcv bin")
        print("  Optional flag -s to save the plot instead of displaying it.")
        print("  Optional filename for the saved plot.")
        print("+----------------------------+")
