import sys
import numpy as np
import matplotlib.pyplot as plt
from save_plot import save_plot
from typing import Optional


def plot_seismogram(
    ns, filename, save: bool = False, plot_filename: Optional[str] = None
):

    with open(filename, "rb") as fid:
        data = np.fromfile(fid, np.float64)

    n = len(data) / ns
    data = data.reshape(int(n), int(ns))
    data = np.transpose(data)
    plt.imshow(data, cmap="gray", aspect="auto")
    plt.colorbar()
    plt.tight_layout()

    if save:
        plot_filename = "seismogram" if not plot_filename else plot_filename
        save_plot(plot_filename)
    else:
        plt.show()


if __name__ == "__main__":
    if len(sys.argv) >= 3:
        plot_seismogram(
            ns=int(sys.argv[1]),
            filename=sys.argv[2],
            save="-s" in sys.argv,
            plot_filename=sys.argv[4] if len(sys.argv) >= 5 else None,
        )
    else:
        print("+----------------------------+")
        print("--  Seismogram plot  --")
        print("Parameters: ")
        print("  ns.")
        print("  Path to bin file.")
        print("  Optional flag -s to save the plot instead of displaying it.")
        print("  Optional filename for the saved plot.")
        print("+----------------------------+")
