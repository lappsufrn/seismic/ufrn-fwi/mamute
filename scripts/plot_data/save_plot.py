import os
import matplotlib.pyplot as plt
import plotly.graph_objs as go


def save_plot(
    plot_filename: str, is_3d: bool = False, fig: go.Figure = None
) -> None:
    project_root = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "..")
    )
    plots_dir = os.path.join(project_root, "plots")

    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)

    if is_3d:
        plot_filename = plot_filename + ".html"
        fig.write_html(os.path.join(plots_dir, plot_filename))
    else:
        plot_filename = plot_filename + ".png"
        plt.savefig(os.path.join(plots_dir, plot_filename))

    print(f"Plot saved to {os.path.join(plots_dir, plot_filename)}")
