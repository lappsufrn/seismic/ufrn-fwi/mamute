import plotly.graph_objs as go
import numpy as np
import sys
from save_plot import save_plot
from typing import Optional


def plot_3d(
    nx,
    ny,
    nz,
    data_filename,
    save: bool = False,
    plot_filename: Optional[str] = None,
):
    """Plot 3D of geophysical model. Only plot some surfaces of the model."""
    data = np.fromfile(data_filename, np.float64)
    data = data.reshape((nx, ny, nz))
    print(data.shape)

    grid_x = np.linspace(0, nx - 1, num=nx)
    grid_y = np.linspace(0, ny - 1, num=ny)
    grid_z = np.linspace(0, nz - 1, num=nz)

    xy, yx = np.meshgrid(grid_y, grid_x)
    xz, zx = np.meshgrid(grid_z, grid_x)
    yz, zy = np.meshgrid(grid_z, grid_y)

    min_value = data.min()
    max_value = data.max()

    plots = []
    # Surfaces YZ
    val_1 = int(nx * 0.5)
    surf_1 = go.Surface(
        x=np.full_like(yz, val_1),
        y=zy,
        z=yz,
        surfacecolor=data[val_1, :, :],
        colorscale="Viridis",
        cmin=min_value,
        cmax=max_value,
    )
    plots.append(surf_1)

    # Surfaces XZ
    val_2 = int(ny * 0.5)
    surf_2 = go.Surface(
        x=zx,
        y=np.full_like(xz, val_2),
        z=xz,
        surfacecolor=data[:, val_2, :],
        colorscale="Viridis",
        showscale=False,
        cmin=min_value,
        cmax=max_value,
    )
    plots.append(surf_2)

    # Surfaces XY
    val_3 = int(nz * 0.50)
    surf_3 = go.Surface(
        x=yx,
        y=xy,
        z=np.full_like(xy, val_3),
        surfacecolor=data[:, :, val_3],
        colorscale="Viridis",
        showscale=False,
        cmin=min_value,
        cmax=max_value,
    )
    plots.append(surf_3)

    fig = go.Figure(data=plots)
    fig.update_layout(scene=dict(zaxis=dict(range=[nz - 1, 0])))

    if save:
        plot_filename = "plot_3d" if not plot_filename else plot_filename
        save_plot(plot_filename=plot_filename, is_3d=True, fig=fig)
    else:
        fig.show()


if __name__ == "__main__":

    if len(sys.argv) >= 5:
        plot_3d(
            nx=int(sys.argv[1]),
            ny=int(sys.argv[2]),
            nz=int(sys.argv[3]),
            data_filename=sys.argv[4],
            save="-s" in sys.argv,
            plot_filename=sys.argv[6] if len(sys.argv) >= 7 else None,
        )
    else:
        print("+----------------------------+")
        print("--  3D plot  --")
        print("Parameters: ")
        print("  nx.")
        print("  ny.")
        print("  nz.")
        print("  Path to bin file.")
        print("  Optional flag -s to save the plot instead of displaying it.")
        print("  Optional filename for the saved plot.")
        print("+----------------------------+")
