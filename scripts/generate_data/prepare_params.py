import json


def read_params(filename):
    data = {}
    with open(filename) as my_file:
        for line in my_file:
            if line[0] != "/":
                attribute = line.replace(" ", "")
                attribute = attribute.replace("\n", "")
                attribute = attribute.split("=")
                if attribute[1].isnumeric():
                    attribute[1] = int(attribute[1])
                else:
                    try:
                        attribute[1] = float(attribute[1])
                    except ValueError:
                        pass

                data[attribute[0]] = attribute[1]
    return data


def params_to_json(filename):
    data = read_params(filename)
    data = json.dumps(data)
    params_json = open("params.json", "w")
    params_json.write(data)


def params_to_mamute(filename, data):
    params_mamute = open(filename, "w")
    for key, value in data.items():
        params_mamute.write(key + "=" + str(value) + "\n")
