# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import json
import sys
from prepare_params import read_params


def gaussian_pertubation(vel_data, mamute_data):

    vel = vel_data["v"] * np.ones(
        (mamute_data["nx"], mamute_data["ny"], mamute_data["nz"])
    )
    xm = mamute_data["nx"] // 2
    ym = mamute_data["ny"] // 2
    zm = mamute_data["nz"] // 2
    v_max = vel_data["v"]
    for i in range(mamute_data["nx"]):
        for j in range(mamute_data["ny"]):
            for k in range(mamute_data["nz"]):
                r = (i - xm) ** 2 + (j - ym) ** 2 + (k - zm) ** 2
                vel[i, j, k] += 1000 * np.exp(
                    -0.5 * r / vel_data["sigma"] ** 2
                )
                v_max = np.max([v_max, vel[i, j, k]])

    calculate_max_dt(
        v_max, mamute_data["dx"], mamute_data["dy"], mamute_data["dz"]
    )

    return vel


def circle(vel_data, mamute_data):
    vel = vel_data["v_out"] * np.ones(
        (mamute_data["nx"], mamute_data["ny"], mamute_data["nz"])
    )
    xm = (mamute_data["nx"] // 2) * mamute_data["dx"]
    ym = (mamute_data["ny"] // 2) * mamute_data["dy"]
    zm = (mamute_data["nz"] // 2) * mamute_data["dz"]
    v_max = np.max([vel_data["v_out"], vel_data["v_in"]])
    for i in range(mamute_data["nx"]):
        for j in range(mamute_data["ny"]):
            for k in range(mamute_data["nz"]):
                x = i * mamute_data["dx"]
                y = j * mamute_data["dy"]
                z = k * mamute_data["dz"]
                if inside_cirle(x, y, z, xm, ym, zm, vel_data["r"]):
                    vel[i, j, k] = vel_data["v_in"]

    calculate_max_dt(
        v_max, mamute_data["dx"], mamute_data["dy"], mamute_data["dz"]
    )

    return vel


def inside_cirle(x, y, z, x0, y0, z0, r):
    d = (x - x0) ** 2 + (y - y0) ** 2 + (z - z0) ** 2
    d = np.sqrt(d)
    return d < r


def constant(vel_data, mamute_data):
    vel = vel_data["v"] * np.ones(
        (mamute_data["nx"], mamute_data["ny"], mamute_data["nz"])
    )
    calculate_max_dt(
        vel_data["v"], mamute_data["dx"], mamute_data["dy"], mamute_data["dz"]
    )
    return vel


def calculate_max_dt(v_max, dx, dy, dz):
    d = np.min([dx, dy, dz])
    dt = (2 * d) / (np.pi * v_max * np.sqrt(3))
    print("------------------------------------------------------")
    print("Following the CFL(Courant–Friedrichs–Lewy) criteria!")
    print("The maximum value for the dt is %.6f" % dt)
    print("when dx=", dx, " dz=", dz, "and dy=", dy)
    print("with the maximum value for the velocity model = %.2f" % v_max)
    print("------------------------------------------------------")


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("--------------------------")
        print("Arguments:")
        print("  Params file about the velocity")
        print("  Params file about params to mamute")
        print("--------------------------")
        exit(-1)

    file_json = sys.argv[1]
    with open(file_json, "r") as json_file:
        vel_data = json.load(json_file)
    mamute_data = read_params(sys.argv[2])

    if vel_data["type"] == "gaussian_pertubation":
        vel = gaussian_pertubation(vel_data, mamute_data)
    elif vel_data["type"] == "circle":
        vel = circle(vel_data, mamute_data)
    elif vel_data["type"] == "constant":
        vel = constant(vel_data, mamute_data)
    else:
        print("Invalid type")
        exit(-1)

    vel[:].tofile(mamute_data["vel"])
