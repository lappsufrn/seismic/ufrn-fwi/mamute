import numpy as np
import sys
import json
import prepare_params


def create_coordinates(name, data):
    xf = data[name]["x"]["first"]
    if "last" in data[name]["x"]:
        xl = data[name]["x"]["last"]
        xd = data[name]["x"]["delta"]
    else:
        xl = xf + 1
        xd = 1

    yf = data[name]["y"]["first"]
    if "last" in data[name]["y"]:
        yl = data[name]["y"]["last"]
        yd = data[name]["y"]["delta"]
    else:
        yl = yf + 1
        yd = 1

    zf = data[name]["z"]["first"]
    if "last" in data[name]["z"]:
        zl = data[name]["z"]["last"]
        zd = data[name]["z"]["delta"]
    else:
        zl = zf + 1
        zd = 1

    n = 0
    coordinates = np.array([])
    x = xf
    while x < xl:
        y = yf
        while y < yl:
            z = zf
            while z < zl:
                coord = np.array([x, y, z], dtype=np.float64)
                coordinates = np.append(coordinates, coord, axis=0)
                z = z + zd
                n = n + 1
            y = y + yd
        x = x + xd

    data[name]["n"] = n
    print("n of ", name, " is: ", n)
    coordinates = coordinates.reshape(n, 3)
    return coordinates


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("--------------------------")
        print("Arguments:")
        print("  Params file about the rcv and src ")
        print(
            "  Params file about params to mamute if you want update "
            "automatically "
        )
        print("--------------------------")
        exit(-1)

    file_json = sys.argv[1]
    with open(file_json, "r") as json_file:
        data = json.load(json_file)

    coordinates = create_coordinates("sources", data)
    with open("src_coord.bin", "wb") as f:
        coordinates.tofile(f)
        f.close()

    coordinates = create_coordinates("receivers", data)

    for s in range(data["sources"]["n"]):
        name = "rcv_coord_" + str(s) + ".bin"
        with open(name, "wb") as f:
            coordinates.tofile(f)
            f.close()

    if len(sys.argv) > 2:
        data_mamute = prepare_params.read_params(sys.argv[2])
        data_mamute["n_src"] = data["sources"]["n"]
        prepare_params.params_to_mamute(sys.argv[2], data_mamute)
    else:
        print(
            "The number of sources generates is :" + str(data["sources"]["n"])
        )
