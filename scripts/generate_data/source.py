import numpy as np
import struct
import sys
from prepare_params import read_params

SOURCE_VALUES_FILENAME = "source.bin"


def save_ricker_src(fpeak, dt, ns, amplitude):
    """Generate binary file using ricker source signature.

    fpeak: Peak frequency
    dt: time step
    ns: number of samples
    """
    source = []

    with open(SOURCE_VALUES_FILENAME, "wb") as f:
        for i in range(ns):
            ti = i * dt
            x = np.pi * fpeak * (ti - 1.0 / fpeak)
            xx = x * x
            a1 = amplitude * np.exp(-xx) * (1.0 - 2.0 * xx)

            source.append(a1)
            f.write(struct.pack("d", a1))

    print(f"File {SOURCE_VALUES_FILENAME} written")


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("--------------------------")
        print("Arguments:")
        print("  Params file    ")
        print("--------------------------")
        exit(-1)

    data = read_params(sys.argv[1])

    save_ricker_src(data["fpeak"], data["dt"], data["ns"], data["amplitude"])
