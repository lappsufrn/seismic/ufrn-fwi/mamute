import numpy as np
import sys
from prepare_params import read_params


def density_model(vel_model: str, mamute_data):
    """
    Generate density model following Gardner's equation
    """
    with open(vel_model, "rb") as arq:
        vel = np.fromfile(arq, dtype=np.float64)

    density = 1000 * 0.31 * (vel**0.25)
    print("-------------------------------------------------------------")
    print("Density model has been generated following Gardner's equation")
    print("-------------------------------------------------------------")
    return density


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print(
            "--------------------------------------------------"
            "-------------------------------------------------"
        )
        print(
            "Insufficient parameters, you must pass the following parameters:"
        )
        print(".: Velocity Model binary file.")
        print(".: Mamute's configuration text file.\n")
        print(
            "Example: python ./scripts/generate_data/density.py "
            "velocity_model.bin./projects/example/modeling.txt"
        )
        print(
            "--------------------------------------------------"
            "-------------------------------------------------"
        )
        exit(-1)

    velocity_data = sys.argv[1]
    mamute_data = read_params(sys.argv[2])
    density = density_model(velocity_data, mamute_data)
    density[:].tofile(mamute_data["density"])
