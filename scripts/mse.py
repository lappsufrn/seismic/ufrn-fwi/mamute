import numpy as np
import sys

from plot_data.plot_trace import plot_trace

MSE_ACCEPTANCE_CRITERIA = 1e-12


def meanSquareError(data_1, data_2):
    MSE = np.square(np.subtract(data_1, data_2)).mean()
    print("MSE: ", MSE)
    assert MSE < MSE_ACCEPTANCE_CRITERIA, (
        f"MSE calculated [{MSE}] is greater than the accpectance "
        f"criteria [{MSE_ACCEPTANCE_CRITERIA}]."
    )


if __name__ == "__main__":

    if len(sys.argv) != 3:
        print("+----------------------------+")
        print("Parameters: ")
        print("  Path to dobs binary data")
        print("  Path to analytical binary data")
        print("+----------------------------+")
        exit(-1)

    f1 = np.fromfile(sys.argv[1], dtype=np.float64)
    f2 = np.fromfile(sys.argv[2], dtype=np.float64)

    meanSquareError(f1, f2)

    plot_trace(
        f1 - f2,
        xlabel="i",
        ylabel="xi - yi",
        title="Difference between analytical and modeled traces",
    )
