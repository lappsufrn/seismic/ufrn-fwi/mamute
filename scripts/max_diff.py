import numpy as np
import sys


def diff_model(data_1, data_2, max_diff_perc, can_save):
    data = data_1 - data_2
    data_err_r = (data / data_1) * 100
    diff_perc = np.max(data_err_r)

    if can_save:
        data[:].tofile("diff_absolute.bin")
        data_err_r[:].tofile("diff_relative.bin")

    assert diff_perc < max_diff_perc, (
        f"DIFFERENCE BETWEEN THE DATA SHOULD BE LESS THAN {max_diff_perc}%."
        f"IT WAS {diff_perc}."
    )

    if can_save:
        print(
            "Difference between the data are less than ",
            max_diff_perc,
            "%, they were:",
            round(diff_perc, 3),
        )


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("+----------------------------+")
        print("Parameters: ")
        print("  Path to first binary data")
        print("  Path to second binary data")
        print("  Maximinium difference in p")
        print("  Optional : if you want save the diff data")
        print("+----------------------------+")
        exit(-1)

    data1 = np.fromfile(sys.argv[1], dtype=np.float64)
    data2 = np.fromfile(sys.argv[2], dtype=np.float64)
    max_diff_perc = float(sys.argv[3])
    can_save = False
    if len(sys.argv) == 5:
        can_save = sys.argv[4]
    diff_model(data1, data2, max_diff_perc, can_save)
