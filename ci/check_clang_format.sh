#!/bin/bash

set -o pipefail

clang_format_file=".clang-format"

exclude_file="clang-format-exclude.txt"

error_file="errors.txt"

function is_excluded_directory {
    local dir_name=$1
    grep -Fxq "$dir_name" "$exclude_file"
}

if [ ! -f "$clang_format_file" ]; then
    echo ".clang-format file not found"
    exit 1
fi

if [ ! -f "$exclude_file" ]; then
    echo "clang-format-exclude file not found."
    exit 1
fi

> "$error_file"

excluded_dirs=$(cat "$exclude_file" | tr '\n' '|')
excluded_dirs=${excluded_dirs%|}

cpp_files=$(find . -name "*.cpp" -o -name "*.hpp" -o -name "*.h" | grep -vE "$excluded_dirs")

for file in $cpp_files; do
    parent_dir=$(dirname "$file")
    if is_excluded_directory "$parent_dir"; then
        continue
    fi

    clang-format -style=file -Werror --dry-run "$file"

    if [ $? -ne 0 ]; then
        echo "$file" >> "$error_file"
    fi
done

if [ -s "$error_file" ]; then
    echo "Error: The code isn't formatted correctly. Check errors.txt."
    exit 1
else
    echo "Success: All files are correctly formatted."
    exit 0
fi
