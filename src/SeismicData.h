#ifndef SEISMICDATA_H
#define SEISMICDATA_H
#include "Config.h"
#include "SimpleConfigTextFile.h"
#include "Utils.h"
#include "Wavefield.h"
#include "utilities/BinaryFileHandler.h"
#include <cstring>

// File name with receiver coordiantes.
// Each file contains all the receivers coordiantes for the `#` shot.
#define FILENAME_RECEIVER_COORDINATES "rcv_coord_#.bin"

// File name for the propagated data.
// Each file contains the observed data in the receiver for the `#` shot.
#define FILENAME_OBSERVED_DATA "dobs_#.bin"

// File containing the values of the source for each time step propagated.
#define FILENAME_SOURCE_DATA "source.bin"

// File containing all source coordiantes
#define FILENAME_SOURCE_COORDINATES "src_coord.bin"

class SeismicData {
public:
  int *x = nullptr;         // x coordinate
  int *y = nullptr;         // y coordinate
  int *z = nullptr;         // z coordinate
  int n_trace;              // number of traces
  int n_samples;            // number of samples
  int nf;                   // data size = n_trace * n_samples
  double dt;                // time sampling
  double *data = nullptr;   // traces data

  explicit SeismicData(int n, int ns, double dt);
  /**
   * Default destructor
   */
  ~SeismicData();
  void subData(SeismicData const &);
#ifdef VERBOSE
  void Print(std::string const &, int id = 0) const;
#endif
  void setPositionFromFile(const std::string &projDir, double ox, double oy, double oz, double dx,
                           double dy, double dz, int ishot, int n_rcv);
  double misfit() const;
  void ReadSourceFromFile(std::string projDir);
  void Resize(int p_ns, double p_dt);
};
#endif
