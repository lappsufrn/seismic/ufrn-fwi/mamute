#include "Modeling.h"
#include "Utils.h"
#include <iostream>

constexpr double Modeling::gC[];

Modeling::Modeling(Grid &grid, VelocityModel &vmodel, int ns, double dt, double fpeak,
                   DensityModel &density)
    : fGrid(grid), fVelModel(vmodel) {
  try {
    // cppcheck-suppress noCopyConstructor
    // cppcheck-suppress noOperatorEq
#ifdef MAMUTE_DAMPING
    fG1 = new double[fGrid.n];
    fG2 = new double[fGrid.n];
#endif
    fDudx = new double[fGrid.n];
    fDudy = new double[fGrid.n];
    fDudz = new double[fGrid.n];
    fNabla2 = new double[fGrid.n];
#ifdef MAMUTE_CPML
    fCPML = new CPML(grid, vmodel, dt, fpeak);
#endif
#ifdef MAMUTE_DENSITY
    fDensity = &density;
    fWave_q = new Wavefield(fGrid.n);
#endif
  } catch (const std::exception &e) {
    std::string s("Modeling::Modeling allocation failed: ");
    throw s + e.what();
  }
  // Before checking conditions, data and modeling time sampling are the same
  fNs = fOns = ns;
  fDt = fOdt = dt;
  fTimeFactor = 1;

#ifdef MAMUTE_AUTOTUNING
  // initial chunksize for PATSMA
  omp_chunk = fGrid.n / omp_get_max_threads();
#endif
}

//!
//! * Valid Conditions - Carcione Equation.
//!
int Modeling::ValidConditions(double fpeak, int id) {
  int oldTimeFactor = fTimeFactor;
  double dx = fGrid.dx;
  double dy = fGrid.dy;
  double dz = fGrid.dz;
  double vmin = fVelModel.getVMin();
  double vmax = fVelModel.getVMax();
#ifdef VERBOSE
  if (!id) {
    std::cout << "--- CHECKING CONDITIONS" << std::endl;
  }
#endif
  //! Carcione (2002) Equation 35
  int W = 4;   //! grid points per minimum wavelength
  double maxf = 2.5 * fpeak;
  double maxid = vmin / (W * maxf);
  double maxd = fmax(dx, fmaxf(dy, dz));
  double maxif = vmin / (W * maxd);
  //! Carcione (2002) Equation 12
  double mind = fminf(dx, fminf(dy, dz));
  double maxidt = 2 * mind / (M_PI * sqrt(3) * vmax);

  if (maxd > maxid) {
    if (!id) {
      std::cout << "Reduce fpeak and/or reduce max(dx,dy,dz)" << std::endl;
      std::cout << "max(dx,dy,dz) <= " << maxid << " for fpeak = " << fpeak << std::endl;
      std::cout << "fpeak <= " << maxif / 2.5 << " for max(dx,dy,dz) = " << maxd << std::endl;
    }
    return 0;
  }

  if (fDt > maxidt) {
    fTimeFactor = ((int) ceil(fOdt / maxidt));
    if (fTimeFactor != oldTimeFactor) {
      fDt = fOdt / fTimeFactor;
      fNs = (1 + (fOns - 1) * fTimeFactor);
      if (!id)
        std::cout << "Modeling time sampling: [dt = " << fDt << ", ns = " << fNs << "]"
                  << std::endl;
    }
  }

#ifdef SINC_OMP
  if (!fftw_init_threads()) {
    std::cout << "fftw_init_threads failed!" << std::endl;
    return 0;
  }
  fftw_plan_with_nthreads(omp_get_max_threads());
#endif
#ifdef SINC
  fFfty = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (fOns / 2 + 1));
  fFftyp = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (fNs / 2 + 1));
#endif

  return 1;
}

#ifdef MAMUTE_SPHERE
void Modeling::Sphere(const SeismicData &in, VelocityModel &vmodel) {
  double vmax = fVelModel.getVMax();
  double dcc, cr;
  int xmin, xmax, ymin, ymax, zmin, zmax;
  int xs, ys, zs;

  xs = fGrid.border + in.x[0];
  ys = fGrid.border + in.y[0];
  zs = fGrid.border + in.z[0];

  xmin = ymin = zmin = gNc - 1;
  xmax = fGrid.nx - gNc + 1;
  ymax = fGrid.ny - gNc + 1;
  zmax = fGrid.nz - gNc + 1;

  xinf = new int[fNs];
  xsup = new int[fNs];
  yinf = new int *[fNs];
  ysup = new int *[fNs];
  zinf = new int *[fNs];
  zsup = new int *[fNs];
  for (int ti = 0; ti < fNs; ti++) {
    double radius = vmax * (ti + 1) * in.dt;
    xinf[ti] = std::fmax(xmin, xs - (int) ceil(radius / fGrid.dx));
    xsup[ti] = std::fmin(xmax - 1, xs + (int) ceil(radius / fGrid.dx)) + 1;

    int sphereX = xsup[ti] - xinf[ti];
    yinf[ti] = new int[sphereX];
    ysup[ti] = new int[sphereX];
    zinf[ti] = new int[sphereX];
    zsup[ti] = new int[sphereX];

    for (int xi = xinf[ti], i = 0; xi < xsup[ti]; xi++, i++) {
      dcc = std::min(radius, fabs(xi - xs) * fGrid.dx);
      cr = sqrt(radius * radius - dcc * dcc);

      yinf[ti][i] = std::max(ymin, ys - (int) ceil(cr / fGrid.dy));
      ysup[ti][i] = std::min(ymax - 1, ys + (int) ceil(cr / fGrid.dy)) + 1;
      zinf[ti][i] = std::max(zmin, zs - (int) ceil(cr / fGrid.dz));
      zsup[ti][i] = std::min(zmax - 1, zs + (int) ceil(cr / fGrid.dz)) + 1;
    }
  }
}
#endif

// Interpolate SeismicData
void Modeling::Interpolate(SeismicData *seisd) {
  if (seisd->n_samples == fNs)
    return;
  try {
#ifdef _OPENMP
#pragma omp single
#endif
    fInterpolationData = new double[seisd->n_trace * fNs];
    for (int k = 0; k < seisd->n_trace; k++) {
#ifdef SINC
      SincInterp(&seisd->data[k * seisd->n_samples], seisd->n_samples, &fInterpolationData[k * fNs],
                 fNs, fFfty, fFftyp);
#else
      NNInterp(&seisd->data[k * seisd->n_samples], seisd->n_samples, &fInterpolationData[k * fNs],
               fNs);
#endif
    }
#ifdef _OPENMP
#pragma omp single
#endif
    {
      delete[] seisd->data;
      seisd->data = fInterpolationData;
      seisd->n_samples = fNs;
      seisd->dt = fDt;
      seisd->nf = seisd->n_trace * seisd->n_samples;
    }
  } catch (const std::exception &e) {
#ifdef _OPENMP
#pragma omp single
#endif
    {
      std::string s("Modeling::Interpolate failed: ");
      throw s + e.what();
    }
  }
}

// cppcheck-suppress constParameter
void Modeling::ReadReceiver(SeismicData &seisd, Wavefield &wave, int ti) const {
  int lti = ti / fTimeFactor;
  if (ti % fTimeFactor != 0)
    return;   // subsampling
#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (int iout = 0; iout < seisd.n_trace; iout++) {
    int xs, ys, zs;
    xs = seisd.x[iout] + fGrid.border;
    ys = seisd.y[iout] + fGrid.border;
    zs = seisd.z[iout] + fGrid.border;
    seisd.data[iout * seisd.n_samples + lti] = wave.current[(xs * fGrid.ny + ys) * fGrid.nz + zs];
  }
}

void Modeling::ModelingStep(const SeismicData &in, Wavefield &wave, int ti, int direction) {
  // cppcheck-suppress variableScope
  int ind, xs, ys, zs, iin;
  double dh3 = 1 / (fGrid.dx * fGrid.dy * fGrid.dz);

  Wavefield *fWave = nullptr;

#ifdef MAMUTE_DENSITY
  fWave = fWave_q;
#else
  fWave = &wave;
#endif

  Utils::Laplacian(fWave->current, fNabla2, fGrid, fDudx, fDudy, fDudz);

#ifdef MAMUTE_SPHERE
#ifdef _OPENMP
#pragma omp for collapse(1) OMP_SCHEDULE_AUTOTUNING
#endif
  for (int xi = xinf[ti]; xi < xsup[ti]; xi++) {
    for (int yi = yinf[ti][xi - xinf[ti]]; yi < ysup[ti][xi - xinf[ti]]; yi++) {
      for (int zi = zinf[ti][xi - xinf[ti]]; zi < zsup[ti][xi - xinf[ti]]; zi++) {
#else
#ifdef _OPENMP
#pragma omp for collapse(3) OMP_SCHEDULE_AUTOTUNING
#endif
  for (int xi = gNc - 1; xi < fGrid.nx - gNc + 1; xi++) {
    for (int yi = gNc - 1; yi < fGrid.ny - gNc + 1; yi++) {
      for (int zi = gNc - 1; zi < fGrid.nz - gNc + 1; zi++) {
#endif
        ind = (xi * fGrid.ny + yi) * fGrid.nz + zi;
        ModelWave(*fWave, ind);
      }
    }
  }

#ifdef MAMUTE_CPML
  fCPML->ApplyCPML(*fWave, fDudx, fDudy, fDudz);
#endif

#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (iin = 0; iin < in.n_trace; iin++) {
    xs = in.x[iin] + fGrid.border;
    ys = in.y[iin] + fGrid.border;
    zs = in.z[iin] + fGrid.border;
    ind = (xs * fGrid.ny + ys) * fGrid.nz + zs;
    SourceInsertion(in, *fWave, ti, direction, ind, iin, dh3);
  }

#ifdef MAMUTE_FREE_SURFACE
  FreeSurface(*fWave);
#endif
#ifdef MAMUTE_DENSITY
  RestorePressureField(wave);
#endif
#ifdef _OPENMP
#pragma omp single
#endif
  {
    if (direction != 0)
      std::swap<double *>(fWave->p_new, fWave->current);
  }
}

#ifdef MAMUTE_FREE_SURFACE
void Modeling::FreeSurface(Wavefield &wave) const {
  int xi, yi, ind, d;
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(2)
#endif
  for (xi = gNc - 1; xi < fGrid.nx - gNc + 1; xi++) {
    for (yi = gNc - 1; yi < fGrid.ny - gNc + 1; yi++) {
      ind = (xi * fGrid.ny + yi) * fGrid.nz + fGrid.border - 1;
      wave.p_new[ind] = 0.0;
      for (d = 1; d < gNc; d++) {
        wave.p_new[ind - d] = -wave.p_new[ind + d];
      }
    }
  }
}
#endif

#ifdef MAMUTE_DENSITY
void Modeling::RestorePressureField(Wavefield &wave) const {
  int ind;
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(3)
#endif
  for (int xi = 0; xi < fGrid.nx; xi++) {
    for (int yi = 0; yi < fGrid.ny; yi++) {
      for (int zi = 0; zi < fGrid.nz; zi++) {
        ind = (xi * fGrid.ny + yi) * fGrid.nz + zi;
        wave.current[ind] = fWave_q->current[ind] * fDensity->fExpandedModel[ind];
      }
    }
  }
}
#endif

inline void Modeling::SourceInsertion(const SeismicData &in, Wavefield &wave, const int ti,
                                      const int direction, const int ind, const int iin,
                                      const double dh3) {

  double source = fVelModel.fExpandedModel[ind] * in.data[iin * in.n_samples + ti];

#ifdef MAMUTE_DAMPING
  source *= fG1[ind];
#endif
#ifdef MAMUTE_DENSITY
  source *= fDensity->fExpandedModel[ind];
#endif
  switch (direction) {
  case 1:
    wave.p_new[ind] += source * dh3;
    break;
  case -1:
    wave.p_new[ind] += source;
    break;
  }
}

inline void Modeling::ModelWave(Wavefield &wave, const int ind) {
#ifdef MAMUTE_DAMPING
#ifndef MAMUTE_DENSITY
  wave.p_new[ind] = fG1[ind] * (fVelModel.fExpandedModel[ind] * fNabla2[ind] +
                                2 * wave.current[ind] - fG2[ind] * wave.p_new[ind]);
#else
  wave.p_new[ind] = fG1[ind] * (fVelModel.fExpandedModel[ind] *
                                    (fNabla2[ind] - fDensity->fMass[ind] * wave.current[ind]) +
                                2 * wave.current[ind] - fG2[ind] * wave.p_new[ind]);
#endif
#endif
#ifdef MAMUTE_CPML
#ifndef MAMUTE_DENSITY
  wave.p_new[ind] =
      fVelModel.fExpandedModel[ind] * fNabla2[ind] + 2 * wave.current[ind] - wave.p_new[ind];
#else
  wave.p_new[ind] =
      fVelModel.fExpandedModel[ind] * (fNabla2[ind] - fDensity->fMass[ind] * wave.current[ind]) +
      2 * wave.current[ind] - wave.p_new[ind];
#endif
#endif
}

Modeling::~Modeling() {

#ifdef MAMUTE_SPHERE
  for (int i = 0; i < fNs; i++) {
    delete[] yinf[i];
    delete[] ysup[i];
    delete[] zinf[i];
    delete[] zsup[i];
  }

  delete[] xinf;
  delete[] xsup;
  delete[] yinf;
  delete[] ysup;
  delete[] zinf;
  delete[] zsup;
#endif

#ifdef MAMUTE_DAMPING
  delete[] fG1;
  delete[] fG2;
#endif
  delete[] fNabla2;
  delete[] fDudx;
  delete[] fDudy;
  delete[] fDudz;
#ifdef MAMUTE_CPML
  delete fCPML;
#endif
#ifdef MAMUTE_DENSITY
  delete fWave_q;
#endif
#ifdef SINC
  fftw_free(fFfty);
  fftw_free(fFftyp);
#ifdef SINC_OMP
  fftw_cleanup_threads();
#else
  fftw_cleanup();
#endif
#endif
}
