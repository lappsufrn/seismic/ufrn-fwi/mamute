#include "ManageWavefieldDisk.h"

ManageWavefieldDisk::ManageWavefieldDisk(Wavefield &wf, const Grid &grid,
                                         const std::string &projDir)
    : ManageWavefield{wf, grid}, fProjDir(projDir) {}

void ManageWavefieldDisk::SaveWavefield(int ti, int ishot) { fWf.Save(fProjDir, fGrid, ti, ishot); }

void ManageWavefieldDisk::RetrieveWavefield(double *wf, int ti, int ishot) {
  fWf.getFwd(wf, fProjDir, fGrid, ti, ishot);
}

void ManageWavefieldDisk::EndShot(int ishot) {
#ifdef _OPENMP
#pragma omp single
#endif
  {
    BinaryFileHandler fileHandler(fProjDir);
    fileHandler.DeleteFile(FILENAME_WAVEFIELD_SHOT, ishot);
  }
}

ManageWavefieldDisk::~ManageWavefieldDisk() {}