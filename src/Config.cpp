#include "Config.h"
#include <cstring>

#ifdef DEBUG
#include <cstdio>
#endif

// cppcheck-suppress unusedFunction
Config *Config::ReadConfigFile(int argc, char *argv[]) {
  const char *filename = argv[1];
  const char *dot = std::strrchr(filename, '.');
#ifdef DEBUG
  fprintf(stdout, "Creating a parser to config file \'%s\'.\n", filename);
  if (dot == NULL) {
    fprintf(stdout, "No filetype to define config class/object.\n");
  }
#endif
  if (dot == NULL || *(dot + 1) == '\0') {
    throw std::runtime_error("Config filetype unknown.");
  }
  return ConfigFactory::Create(dot + 1)(argc, argv);
}

std::map<std::string, factoryMethod> ConfigFactory::RegisteredConfigs = {};

// cppcheck-suppress unusedFunction
bool ConfigFactory::Register(const char *filetype, factoryMethod createMethod) {
#ifdef DEBUG
  fprintf(stdout, "Registering \'%s\' filetype using \'%s\' class/method.\n", filetype,
          typeid(createMethod).name());
#endif
  std::pair<std::map<std::string, factoryMethod>::iterator, bool> registeredPair =
      ConfigFactory::RegisteredConfigs.insert(std::make_pair(filetype, createMethod));
  return registeredPair.second;
}

#include "SimpleConfigTextFile.h"
void staticRegister(std::map<std::string, factoryMethod> &registeredConfigs) {
  registeredConfigs.insert(std::make_pair("txt", &ConfigHelper<SimpleConfigTextFile>::Create));
}

factoryMethod ConfigFactory::Create(const char *filetype) {
  std::map<std::string, factoryMethod>::iterator registeredPair =
      ConfigFactory::RegisteredConfigs.find(filetype);
#ifdef DEBUG
  if (registeredPair == ConfigFactory::RegisteredConfigs.end()) {
    fprintf(stdout, "Failed to load \'%s\' config object. Creating it manually...\n", filetype);
  } else {
    fprintf(stderr, "Loading \'%s\' config object ", filetype);
    fprintf(stdout, "from \'%s\'.\n", typeid((registeredPair->second)).name());
  }
#endif
  if (registeredPair == ConfigFactory::RegisteredConfigs.end()) {
    // The next line registers *all* config files classes
    // FIXME change to a more intelligent way (for instance, CRTP)
    staticRegister(ConfigFactory::RegisteredConfigs);
    registeredPair = ConfigFactory::RegisteredConfigs.find(filetype);

    if (registeredPair == ConfigFactory::RegisteredConfigs.end()) {
      throw std::runtime_error("Config filetype unknown and/or unregistered.");
    }
  }
  return registeredPair->second;
}
