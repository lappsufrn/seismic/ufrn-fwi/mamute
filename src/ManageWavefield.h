#ifndef MAMAGE_WAVEFIELD_H
#define MAMAGE_WAVEFIELD_H

#include "Wavefield.h"

class ManageWavefield {
protected:
  Wavefield &fWf;
  const Grid &fGrid;

public:
  explicit ManageWavefield(Wavefield &wf, const Grid &grid) : fWf(wf), fGrid(grid){};
  virtual void SaveWavefield(int ti, int ishot) = 0;
  virtual void RetrieveWavefield(double *wf, int ti, int ishot) = 0;
  virtual void EndShot(int ishot) = 0;
  virtual ~ManageWavefield(){};
};

#endif
