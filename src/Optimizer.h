#ifndef OPTIMIZER_H
#define OPTIMIZER_H
#include "Adjoint.h"
#include "Config.h"
#include "SimpleConfigTextFile.h"
#include "Utils.h"
#include "VelocityModel.h"
#ifdef FT
#include "lbfgsb/lbfgsb.h"
#endif

class Optimizer {
private:
#ifndef FT
  bool fConverged;
#endif
  Config *fConfig = nullptr;
  int fMaxSteps = 0;
  int fMaxUpdatesIteration = 0;
  int fCurUpdateIteration = 0;

public:
  // cppcheck-suppress noExplicitConstructor
#ifdef FT
  bool fConverged;
  double factr, pgtol;
  integer nmax, mmax;
  int iter = 0;
  int nwa;
  integer *iwa, *nbd, taskValue, *task, iprint, csaveValue, *csave, isave[44];
  logical lsave[4];
  double *wa, *l, *u, dsave[29];
#endif
  Optimizer(VelocityModel *velocityModel, Config *config);
  ~Optimizer();
  // cppcheck-suppress unusedFunction
  bool HasConverged() const { return fConverged; }
  void Update(VelocityModel *model, Adjoint *adjoint);
  void Serialize(std::string filename);
  bool Deserialize(std::string filename);
#ifdef TEST
  bool TestRecover(std::string filename);
#endif
};
#endif   // OPTIMIZER_H
