#include "Grid.h"
#include "cmath"

Grid::Grid(int _stencil, double _dx, double _dy, double _dz, int _border, int _nx, int _ny, int _nz)
    : onx(_nx), ony(_ny), onz(_nz), dx(_dx), dy(_dy), dz(_dz), border(_border), stencil(_stencil) {

  start_x = border;
  start_y = border;
  start_z = border;
  on = onx * ony * onz;

  nx = onx + 2 * border;
  ny = ony + 2 * border;
  nz = onz + 2 * border;

  end_x = nx - border;
  end_y = ny - border;
  end_z = nz - border;
  n = nx * ny * nz;
}

void Grid::ComputeDampingCoefficients(double *coef1, double *coef2, double fpeak, double dt,
                                      int id) const {
#ifdef VERBOSE
  if (!id) {
    std::cout << "--- COMPUTING ABSORBING COEFFICIENTS" << std::endl;
  }
#endif
  double *gamax, *gamay, *gamaz;
  const int nc = 5;

  gamax = new double[nx]();
  if (gamax == nullptr)
    std::cout << "Memory not allocated for gamax" << std::endl;
  gamay = new double[ny]();
  if (gamay == nullptr)
    std::cout << "Memory not allocated for gamay" << std::endl;
  gamaz = new double[nz]();
  if (gamaz == nullptr)
    std::cout << "Memory not allocated for gamaz" << std::endl;

  if (border <= nc) {
    for (int ind = 0; ind < n; ind++) {
      coef1[ind] = 1;
      coef2[ind] = 1;
    }
  } else {
    double aux = 2 * M_PI * fpeak * dt;
    // top
    for (int zi = 0; zi < border; zi++) {
      gamaz[zi] = aux * pow((border - zi) / ((double) border), 2);
    }
    // front
    for (int xi = 0; xi < border; xi++) {
      gamax[xi] = aux * pow((border - xi) / ((double) border), 2);
    }
    // left
    for (int yi = 0; yi < border; yi++) {
      gamay[yi] = aux * pow((border - yi) / ((double) border), 2);
    }
    // bottom
    for (int zi = end_z; zi < nz; zi++) {
      gamaz[zi] = aux * pow((zi - end_z + 1) / ((double) border), 2);
    }
    // right
    for (int yi = end_y; yi < ny; yi++) {
      gamay[yi] = aux * pow((yi - end_y + 1) / ((double) border), 2);
    }
    // back
    for (int xi = end_x; xi < nx; xi++) {
      gamax[xi] = aux * pow((xi - end_x + 1) / ((double) border), 2);
    }

    for (int xi = 0; xi < nx; xi++) {
      for (int yi = 0; yi < ny; yi++) {
        for (int zi = 0; zi < nz; zi++) {
          int ind = (xi * ny + yi) * nz + zi;
          aux = gamax[xi] + gamay[yi] + gamaz[zi];
          coef1[ind] = 1 / (1 + aux);
          coef2[ind] = 1 - aux;
        }
      }
    }
  }
  delete[] gamax;
  delete[] gamay;
  delete[] gamaz;
}