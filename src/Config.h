#ifndef CONFIG_H
#define CONFIG_H

#include <map>
#include <stdexcept>
#include <string>

// This file should work like:
// - https://gist.github.com/sacko87/3359911
// Perhaps, migrate to:
// - http://www.nirfriedman.com/2018/04/29/unforgettable-factory/
// But, unfortunately, both are not working... :(
class Config {
private:
  const int fArgc;
  char **fArgv;
  const char *fConfigFileName;

public:
  static Config *ReadConfigFile(int argc, char *argv[]);
  virtual ~Config() {}
  // cppcheck-suppress unusedFunction
  const int GetArgc() const { return fArgc; }
  // cppcheck-suppress unusedFunction
  char **GetArgv() const { return fArgv; }
  // cppcheck-suppress unusedFunction
  virtual const std::string Get(const char *key) {
    throw std::runtime_error("Get not implemented.");
  }
  // cppcheck-suppress unusedFunction
  const char *GetFileName() const { return fConfigFileName; }

protected:
  const bool fRegistered;
  Config(int argc, char *argv[])
      : fArgc(argc), fArgv(argv), fConfigFileName(argv[1]), fRegistered(false) {}
  Config(int argc, char *argv[], const bool registered)
      : fArgc(argc), fArgv(argv), fConfigFileName(argv[1]), fRegistered(registered) {}
};

typedef Config *(*factoryMethod)(int, char **);

class ConfigFactory {
public:
  static bool Register(const char *filetype, factoryMethod createMethod);
  static factoryMethod Create(const char *filetype);

protected:
  static std::map<std::string, factoryMethod> RegisteredConfigs;
};

template <typename T> class ConfigHelper : public Config {
public:
  // cppcheck-suppress unusedFunction
  static Config *Create(int argc, char *argv[]) { return new T(argc, argv); }

protected:
  static const bool gRegistered;
  ConfigHelper(int argc, char *argv[]) : Config(argc, argv, gRegistered) {}
};

// This registration is also not working
#define REGISTER_CONFIG(filetype, classname)                                                       \
  template <typename T>                                                                            \
  const bool ConfigHelper<T>::gRegistered = true;   // ConfigFactory::Register(filetype,
                                                    // &ConfigHelper<T>::Create);

#endif   // CONFIG_H
