#include "Checkpoint.h"

#include "revolve/revolve.h"

// This solution came from https://docs.rs/crate/sys-info/
#ifdef __APPLE__
#include <mach/mach_host.h>
#include <sys/sysctl.h>
#else
#include <sys/sysinfo.h>
#endif

double FWI::Checkpoint::MEM_PERCENT = 0.8;

//!
//! Create the Checkpoint struct.
//!
FWI::Checkpoint::Checkpoint(int s, int gridN, double mem_percent) {
  snaps = 0;
  n_check = 0;
  n_buffers = 0;
  current = 0;
  enum ACTION::action whatodo;
  int i;
  double aux;

  MEM_PERCENT = mem_percent;

  n_buffers = getNBuffers(gridN, s);
  ::Checkpoint *ch = new ::Checkpoint(s);
  Offline *of = new Offline(s, n_buffers, ch);
  //! Calculation of Number of Checkpoint by Revolve lib.
  do {
    whatodo = of->revolve();
  } while ((whatodo != ACTION::terminate) && (whatodo != ACTION::error));

  n_check = of->get_CP()->takeshots;
  if (n_buffers > n_check) {
    n_buffers = n_check;
  }
  current = 0;
  // cppcheck-suppress noCopyConstructor
  // cppcheck-suppress noOperatorEq
  chk = new int[n_check];
  ord_chk = new int[n_buffers];
  ib_to_ck = new int[n_buffers];

  for (i = 0; i < n_buffers; i++) {
    buffer.push_back(new Wavefield(gridN));
  }
  aux = ((double) (s - 1)) / (n_check - 1);

  for (i = 0; i < n_check; i++) {
    chk[i] = ((int) (i * aux));
  }

  resetCheck();
  delete ch;
}

/**
 * Reset the buffer of Chekckpoint
 * **/
void FWI::Checkpoint::resetCheck() {
  double aux;
  int i;

  aux = ((double) (n_check - 1)) / (n_buffers - 1);

  ord_chk[0] = chk[0];
  ib_to_ck[0] = 0;
  ord_chk[n_buffers - 1] = chk[n_check - 1];
  ib_to_ck[n_buffers - 1] = n_check - 1;
  for (i = 1; i < n_buffers - 1; i++) {
    ib_to_ck[i] = ((int) (i * aux));
    ord_chk[i] = chk[ib_to_ck[i]];
  }
}

/**
 * Get the number of buffers to memory free.
 * **/
int FWI::Checkpoint::getNBuffers(int n, int ns) {
  unsigned long total_bytes;

  if (MEM_PERCENT <= 0.0 || MEM_PERCENT > 1.0) {
    throw std::runtime_error("Checkpoint memory percent is wrong. The value must be (0,1].");
  }

#ifdef __APPLE__
  vm_statistics_data_t vm_stat;
  mach_msg_type_number_t count = HOST_VM_INFO_COUNT;
  host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t) &vm_stat, &count);
  total_bytes = (vm_stat.free_count * PAGE_SIZE / 1024) * MEM_PERCENT;
#else
  struct sysinfo myinfo;
  sysinfo(&myinfo);
  total_bytes = (myinfo.mem_unit * myinfo.totalram) * MEM_PERCENT;
#endif
  int nb = (total_bytes) / (2 * n * sizeof(double));

  if (nb <= 0) {
    throw std::runtime_error("Checkpoint memory is insufficient. Try to increase the memory "
                             "percentage.");
  } else if (nb > ns) {
    return ns;
  } else {
    return nb;
  }
}

/**
 * Save the Wavefield in memory buffers
 **/
// cppcheck-suppress unusedFunction
void FWI::Checkpoint::saveCheck(Wavefield *wf, int n, int ti) {
#ifdef _OPENMP
#pragma omp single
#endif
  {
    if (ti == ord_chk[current]) {
      memcpy(buffer[current]->current, wf->current, n * sizeof(double));
      memcpy(buffer[current]->p_new, wf->p_new, n * sizeof(double));
      current++;
    }
  }
}

void FWI::Checkpoint::UpdateBuffers(Adjoint *adj, WorkStealing *ws, Wavefield *cwf,
                                    SeismicData *src, int n) {
  int buffer_index, lti, i1, i2;
  int nab = n_buffers - 1 - current;   // number of available buffers
  int nbr = 0;                         // number of buffers to replace
  int cchk = 0;                        // current checkpoint index
  if (nab > 0) {
    cchk = ib_to_ck[current + 1] - 1;   // first checkpoint before the next buffer
    while ((nbr < nab) && (chk[cchk] > ord_chk[current])) {
      cchk--;
      nbr++;
    }
    cchk++;
  }
  i1 = current + 1;
  i2 = current + nbr;

#ifdef _OPENMP
#pragma omp single
#endif
  {
    memcpy(cwf->current, buffer[current]->current, n * sizeof(double));
    memcpy(cwf->p_new, buffer[current]->p_new, n * sizeof(double));
  }
  for (buffer_index = i1; buffer_index <= i2; buffer_index++) {
    int tii = chk[ib_to_ck[current]] + 1;
    int tif = chk[cchk];

    for (lti = tii; lti <= tif; lti++) {
      ws->checkToken();
      adj->modl->ModelingStep(*src, *cwf, lti, 1);
    }
#ifdef _OPENMP
#pragma omp single
#endif
    {
      memcpy(buffer[buffer_index]->current, cwf->current, n * sizeof(double));
      memcpy(buffer[buffer_index]->p_new, cwf->p_new, n * sizeof(double));
      current++;
      ib_to_ck[buffer_index] = cchk;
      ord_chk[buffer_index] = tif;
    }
    cchk++;
  }
}

// cppcheck-suppress unusedFunction
void FWI::Checkpoint::bwdChkSetup() { current = n_buffers - 1; }

void FWI::Checkpoint::getWavefieldWithoutBorders(double *out, double *in, const Grid &grid) {
#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (int xi = grid.border; xi < grid.end_x; xi++) {
    for (int yi = grid.border; yi < grid.end_y; yi++) {
      memcpy(&(out[((xi - grid.border) * grid.ony + (yi - grid.border)) * grid.onz]),
             &(in[(xi * grid.ny + yi) * grid.nz + grid.border]), grid.onz * sizeof(double));
    }
  }
}

// cppcheck-suppress unusedFunction
void FWI::Checkpoint::getFwdChk(Adjoint *adj, WorkStealing *ws, double *fwdwf, Wavefield *cwf,
                                int ti, const Grid &grid, SeismicData *src) {
  int cti = ord_chk[current];   // current buffer ti
  // Get wavefield from buffer
  if (cti == ti) {
    getWavefieldWithoutBorders(fwdwf, buffer[current]->current, grid);
    return;
  }
  if (cti - 1 == ti) {
    getWavefieldWithoutBorders(fwdwf, buffer[current]->p_new, grid);
    if (ti != 0) {
#ifdef _OPENMP
#pragma omp single
#endif
      { current--; }
      // update the buffers after remove the last buffer used.
      UpdateBuffers(adj, ws, cwf, src, grid.n);
    }
    return;
  }

// Compute wavefields without buffers.
#ifdef _OPENMP
#pragma omp single
#endif
  {
    memcpy(cwf->current, buffer[current]->current, grid.n * sizeof(double));
    memcpy(cwf->p_new, buffer[current]->p_new, grid.n * sizeof(double));
  }

  for (int lti = cti + 1; lti <= ti; lti++) {
    ws->checkToken();
    adj->modl->ModelingStep(*src, *cwf, lti, 1);
  }

  getWavefieldWithoutBorders(fwdwf, cwf->current, grid);
}

#ifdef VERBOSE
// cppcheck-suppress unusedFunction
void FWI::Checkpoint::print(int n, int verb) {
  std::cout << "------------ PRINT CHECKPOINTING INFO ------------" << std::endl;
  std::cout << "Memory for checkpoints: "
            << (((long int) n_buffers) * 2 * n * sizeof(double)) / 1000000000.0 << " GB"
            << std::endl;
  std::cout << "n_check: " << n_check << std::endl;
  std::cout << "n_buffers: " << n_buffers << std::endl;
  std::cout << "current: " << current << std::endl;
  switch (n_check) {
  case 1:
    std::cout << "chk: " << chk[0] << "\t";
    break;
  case 2:
    std::cout << "chk: " << chk[0] << " " << chk[1] << "\t";
    break;
  case 3:
    std::cout << "chk: " << chk[0] << " " << chk[1] << " " << chk[2] << "\t";
    break;
  default:
    std::cout << "chk: " << chk[0] << " " << chk[1] << "..." << chk[n_check - 2] << " "
              << chk[n_check - 1] << "\t";
  }
  switch (n_buffers) {
  case 1:
    std::cout << "ord_chk: " << ord_chk[0] << "\t";
    std::cout << "ib_to_ck: " << ib_to_ck[0] << "\n";
    break;
  case 2:
    std::cout << "ord_chk: " << ord_chk[0] << " " << ord_chk[1] << "\t";
    std::cout << "ib_to_ck: " << ib_to_ck[0] << " " << ib_to_ck[1] << "\n";
    break;
  case 3:
    std::cout << "ord_chk: " << ord_chk[0] << " " << ord_chk[1] << " " << ord_chk[2] << "\t";
    std::cout << "ib_to_ck: " << ib_to_ck[0] << " " << ib_to_ck[1] << " " << ib_to_ck[2] << "\n";
    break;
  default:
    std::cout << "ord_chk: " << ord_chk[0] << " " << ord_chk[1] << "..." << ord_chk[n_buffers - 2]
              << " " << ord_chk[n_buffers - 1] << "\t";
    std::cout << "ib_to_ck: " << ib_to_ck[0] << " " << ib_to_ck[1] << "..."
              << ib_to_ck[n_buffers - 2] << " " << ib_to_ck[n_buffers - 1] << "\n";
  }
  std::cout << "------------------------------------------------" << std::endl;
  if (verb >= 1) {
    for (int i = 0; i < n_buffers; i++) {
      FILE *f = fopen("chkbuf.bin", "ab");
      fwrite(buffer[i]->current, sizeof(double), n, f);
      fclose(f);
    }
  }
}
#endif
