#include "ManageWavefieldMemory.h"
#include <cstring>

ManageWavefieldMemory::ManageWavefieldMemory(Wavefield &wf, const Grid &grid, int Ti)
    : ManageWavefield{wf, grid} {
  fForwardWavefield = new double[Ti * fGrid.on];
  std::cout << "Memory for wavefield: "
            << (((long int) Ti) * fGrid.on * sizeof(double)) / 1073741824.0 << " GB" << std::endl;
}

void ManageWavefieldMemory::SaveWavefield(int ti, int ishot) {
  int x2, y2;
#ifdef _OPENMP
#pragma omp for collapse(2)
#endif
  for (int xi = fGrid.border; xi < fGrid.end_x; xi++) {
    for (int yi = fGrid.border; yi < fGrid.end_y; yi++) {
      x2 = xi - fGrid.border;
      y2 = yi - fGrid.border;
      std::memcpy(&fForwardWavefield[((ti * fGrid.onx + x2) * fGrid.ony + y2) * fGrid.onz],
                  &(fWf.current[(xi * fGrid.ny + yi) * fGrid.nz + fGrid.border]),
                  fGrid.onz * sizeof(double));
    }
  }
}

void ManageWavefieldMemory::RetrieveWavefield(double *wf, int ti, int ishot) {
#ifdef _OPENMP
#pragma omp single
#endif
  { std::memcpy(wf, &fForwardWavefield[ti * fGrid.on], fGrid.on * sizeof(double)); }
}

void ManageWavefieldMemory::EndShot(int ishot) {}

ManageWavefieldMemory::~ManageWavefieldMemory() { delete[] fForwardWavefield; }