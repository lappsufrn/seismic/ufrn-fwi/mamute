#include "Config.h"
#include "FWI.h"
#include <exception>
#include <iostream>

int main(int argc, char *argv[]) {
  int provided, id;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (argc < 2) {
    if (!id)
      std::cout << "Usage: " << argv[0] << " <config_file>" << std::endl;
    return 1;
  }

  try {
    Config *config = Config::ReadConfigFile(argc, argv);
    FWI::FWI *fwi = new FWI::FWI(config);
    fwi->Run();
    delete fwi;
    delete config;
  } catch (std::exception &e) {
    if (!id)
      std::cerr << "Exception caught: " << e.what() << std::endl;
    return -1;
  } catch (std::string &e) {
    if (!id)
      std::cerr << "Exception caught: " << e << std::endl;
    return -1;
  } catch (const char *e) {
    if (!id)
      std::cerr << "Exception caught: " << e << std::endl;
    return -1;
  } catch (...) {
    if (!id)
      std::cerr << "Unknown error running FWI@main()!" << std::endl;
    return -1;
  }

  MPI_Finalize();

  return 0;
}
