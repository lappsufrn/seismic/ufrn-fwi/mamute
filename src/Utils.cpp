#include "Utils.h"
#include "Modeling.h"

namespace My_MPI {
void printTT(double *tt, int id, int npt) {
  std::cout << id;
  for (int i = 0; i < npt; i++) {
    std::cout << std::setw(10) << tt[i];
  }
  std::cout << std::endl;
};

// cppcheck-suppress unusedFunction
void printTimes(double *tt, int id, int npt, int nsrc, int comm_sz) {
  MPI_Barrier(MPI_COMM_WORLD);
  if (id) {
    MPI_Send(&npt, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    MPI_Send(tt, nsrc, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
  } else {
    printTT(tt, 0, npt);

    for (int i = 1; i < comm_sz; i++) {
      MPI_Recv(&npt, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Recv(tt, nsrc, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      printTT(tt, i, npt);
    }
  }
};

// cppcheck-suppress unusedFunction
void SaveFile(const std::string &fileName, int ishot, int nsrc, int comm_sz, int id, double *data,
              int nf, std::string projectPath) {
  BinaryFileHandler fileHandler(projectPath);
  fileHandler.WriteFile<double>(fileName, ishot, data, nf);
};
}   // namespace My_MPI

namespace MyFile {
// cppcheck-suppress unusedFunction
void Save(double *img, const char name[], int size) {
  FILE *f = fopen(name, "wb");
  if (f == NULL) {
    std::cout << "File not opened : " << name << std::endl;
    throw std::runtime_error("File not opened");
  }
  fwrite(img, sizeof(double), size, f);
  fclose(f);
}

// cppcheck-suppress unusedFunction
void SaveI(double *img, const char name[], int size) {
#ifdef _OPENMP
#pragma omp single
#endif
  {
    FILE *f = fopen(name, "ab");
    if (f == NULL) {
      std::cout << "File not opened : " << name << std::endl;
      throw std::runtime_error("File not opened");
    }
    fwrite(img, sizeof(double), size, f);
    fclose(f);
  }
}

}   // namespace MyFile

// cppcheck-suppress unusedFunction
void d2uLast(double *fwD2u, double *fwdwf, double *fwdm1, int ntotal, double dt) {
  int xi;
  double dt2 = dt * dt;

#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (xi = 0; xi < ntotal; xi++) {
    fwD2u[xi] = (-2.0 * fwdwf[xi] + fwdm1[xi]) / dt2;
  }
};

// cppcheck-suppress unusedFunction
void d2uFirst(double *fwD2u, double *fwdwf, double *fwdp1, int ntotal, double dt) {
  int xi;
  double dt2 = dt * dt;

#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (xi = 0; xi < ntotal; xi++) {
    fwD2u[xi] = (fwdp1[xi] - 2. * fwdwf[xi]) / dt2;
  }
};

// cppcheck-suppress unusedFunction
void d2uCenter(double *fwD2u, double *fwdwf, double *fwdp1, double *fwdm1, int ntotal, double dt) {
  int xi;
  double dt2 = dt * dt;

#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (xi = 0; xi < ntotal; xi++) {
    fwD2u[xi] = (fwdp1[xi] - 2. * fwdwf[xi] + fwdm1[xi]) / dt2;
  }
};

/*
 * Check if a file exist using POSIX function
 * return true if the file exist otherwise return false
 */
bool FileExists(const char *filename) {
#if __cplusplus >= 201703L
#error This function must upgrade!
#else
  return (access(filename, F_OK) != -1);
#endif
}

void checkInputData(const std::string &fileNameSourceCoordinates,
                    const std::string &fileNameReceiverCoordinates, const std::string &proj_dir,
                    int *rcv_per_shot, double ox, double oy, double oz, double dx, double dy,
                    double dz, int nx, int ny, int nz, int n_src) {
  int fx, fy, fz, i, j;
  // compute coordinates limits
  fx = ox + (nx - 1) * dx;
  fy = oy + (ny - 1) * dy;
  fz = oz + (nz - 1) * dz;

  BinaryFileHandler fileHandler(proj_dir);
  double *coords = nullptr;

  // Read Source coordinates
  coords = fileHandler.ReadFile<double>(fileNameSourceCoordinates);
  for (i = 0; i < n_src * 3; i += 3) {
    // Check if coordinates values is inside grid's limit
    if (coords[i] < ox || coords[i] > fx || coords[i + 1] < oy || coords[i + 1] > fy ||
        coords[i + 2] < oz || coords[i + 2] > fz) {
      throw std::runtime_error("The coordinates of source " + std::to_string(i / 3) + ": (" +
                               std::to_string(coords[i]) + ", " + std::to_string(coords[i + 1]) +
                               ", " + std::to_string(coords[i + 2]) + ") are invalid\n");
    }
  }
  if (coords != nullptr) {
    delete[] coords;
  }

  // For each source, read its receiver's coordinates
  for (i = 0; i < n_src; i++) {
    // Read reciever's coordiante for source `i`
    coords = fileHandler.ReadFile<double>(fileNameReceiverCoordinates, i);
    // Each 3 values make the components X, Y and Z of a coordinate
    for (j = 0; j < rcv_per_shot[i] * 3; j += 3) {
      if (coords[j] < ox || coords[j] > fx || coords[j + 1] < oy || coords[j + 1] > fy ||
          coords[j + 2] < oz || coords[j + 2] > fz) {
        throw std::runtime_error("The coordinates of receiver " + std::to_string(i / 3) +
                                 " trace " + std::to_string(j) + ": (" + std::to_string(coords[j]) +
                                 ", " + std::to_string(coords[j]) + ", " +
                                 std::to_string(coords[j]) + ") are invalid\n");
      }
    }
    if (coords != nullptr) {
      delete[] coords;
    }
  }
}

void SL::setPositionFromFile(const std::string &fileNameSourceCoordinates,
                             const std::string &proj_dir, const double ox, const double oy,
                             const double oz, const double dx, const double dy, const double dz,
                             const int n_src) {
  double *coords1D = nullptr;
  BinaryFileHandler fileHandler(proj_dir);
  // Read data as 1D
  coords1D = fileHandler.ReadFile<double>(fileNameSourceCoordinates);

  for (int i = 0; i < n_src; i++) {
    this->x[i] = int((coords1D[i * 3] - ox) / dx);
    this->y[i] = int((coords1D[i * 3 + 1] - oy) / dy);
    this->z[i] = int((coords1D[i * 3 + 2] - oz) / dz);
  }

  if (coords1D != nullptr) {
    delete[] coords1D;
  }
}

void OneMinusWeightedLaplacian(double *saida, double *vetor, int nx, int ny, int nz, double dx,
                               double dy, double dz, double Lx, double Ly, double Lz) {
  double Lx_1 = Lx * Lx / (dx * dx);
  double Ly_1 = Ly * Ly / (dy * dy);
  double Lz_1 = Lz * Lz / (dz * dz);
  double Lx_0 = -2.0 * Lx_1;
  double Ly_0 = -2.0 * Ly_1;
  double Lz_0 = -2.0 * Lz_1;
  for (int i = 0; i < nx * ny * nz; ++i) {
    saida[i] = vetor[i];
  }
  // x derivative
  for (int yi = 0; yi < ny; ++yi) {
    for (int zi = 0; zi < nz; ++zi) {
      int ind = (yi) *nz + zi;
      saida[ind] -= Lx_0 * vetor[ind] + Lx_1 * (vetor[ind] + vetor[ind + ny * nz]);
    }
  }
  for (int xi = 1; xi < nx - 1; ++xi) {
    for (int yi = 0; yi < ny; ++yi) {
      for (int zi = 0; zi < nz; ++zi) {
        int ind = (xi * ny + yi) * nz + zi;
        saida[ind] -= Lx_0 * vetor[ind] + Lx_1 * (vetor[ind - ny * nz] + vetor[ind + ny * nz]);
      }
    }
  }
  for (int yi = 0; yi < ny; ++yi) {
    for (int zi = 0; zi < nz; ++zi) {
      int ind = ((nx - 1) * ny + yi) * nz + zi;
      saida[ind] -= Lx_0 * vetor[ind] + Lx_1 * (vetor[ind] + vetor[ind - ny * nz]);
    }
  }
  // y derivative
  for (int xi = 0; xi < nx; ++xi) {
    for (int zi = 0; zi < nz; ++zi) {
      int ind = (xi * ny) * nz + zi;
      saida[ind] -= Ly_0 * vetor[ind] + Ly_1 * (vetor[ind] + vetor[ind + nz]);
    }
    for (int yi = 1; yi < ny - 1; ++yi) {
      for (int zi = 0; zi < nz; ++zi) {
        int ind = (xi * ny + yi) * nz + zi;
        saida[ind] -= Ly_0 * vetor[ind] + Ly_1 * (vetor[ind - nz] + vetor[ind + nz]);
      }
    }
    for (int zi = 0; zi < nz; ++zi) {
      int ind = (xi * ny + ny - 1) * nz + zi;
      saida[ind] -= Ly_0 * vetor[ind] + Ly_1 * (vetor[ind] + vetor[ind - nz]);
    }
  }
  // z derivative
  for (int xi = 0; xi < nx; ++xi) {
    for (int yi = 0; yi < ny; ++yi) {
      int ind = (xi * ny + yi) * nz;
      saida[ind] -= Lz_0 * vetor[ind] + Lz_1 * (vetor[ind] + vetor[ind + 1]);
      for (int zi = 1; zi < nz - 1; ++zi) {
        ind = (xi * ny + yi) * nz + zi;
        saida[ind] -= Lz_0 * vetor[ind] + Lz_1 * (vetor[ind - 1] + vetor[ind + 1]);
      }
      ind = (xi * ny + yi) * nz + nz - 1;
      saida[ind] -= Lz_0 * vetor[ind] + Lz_1 * (vetor[ind] + vetor[ind - 1]);
    }
  }
}

void BesselSmoothing3D(double *data, int nx, int ny, int nz, double dx, double dy, double dz,
                       double Lx, double Ly, double Lz) {
  int it = 0;
  int l_on = nx * ny * nz;
  double nr;
  double *p = new double[l_on];
  double *r = new double[l_on];
  double *Ab = new double[l_on];
  double *Ap = new double[l_on];

  OneMinusWeightedLaplacian(Ab, data, nx, ny, nz, dx, dy, dz, Lx, Ly, Lz);
  for (int i = 0; i < l_on; ++i) {
    r[i] = Ab[i] - data[i];
    p[i] = -r[i];
  }
  nr = Norm(r, l_on);
  while (nr > BESSEL_TOL) {
    double pAp, rTr, alpha, beta;
    if (it == l_on) {
      break;
    }
    OneMinusWeightedLaplacian(Ap, p, nx, ny, nz, dx, dy, dz, Lx, Ly, Lz);
    pAp = Dot(p, Ap, l_on);
    rTr = Dot(r, r, l_on);
    alpha = rTr / pAp;
    for (int i = 0; i < l_on; ++i) {
      data[i] += alpha * p[i];
      r[i] += alpha * Ap[i];
    }
    beta = Dot(r, r, l_on) / rTr;
    for (int i = 0; i < l_on; ++i) {
      p[i] = beta * p[i] - r[i];
    }
    it++;
    nr = Norm(r, l_on);
  }

  delete[] r;
  delete[] p;
  delete[] Ap;
  delete[] Ab;
}

void LaplaceSmoothing3D(double *data, int nx, int ny, int nz, double dx, double dy, double dz,
                        double Lx, double Ly, double Lz) {
  BesselSmoothing3D(data, nx, ny, nz, dx, dy, dz, Lx, Ly, Lz);
  BesselSmoothing3D(data, nx, ny, nz, dx, dy, dz, Lx, Ly, Lz);
}

double Dot(double *vetor1, double *vetor2, int n) {
  double saida = 0.0;
  for (int i = 0; i < n; ++i) {
    saida += vetor1[i] * vetor2[i];
  }
  return saida;
}

double Norm(double *vetor, int n) { return sqrt(Dot(vetor, vetor, n)); }

void NNInterp(double *input, int ons, double *output, int ns) {
  double factor = ((double) ns) / ons;
#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (int i = 0; i < ns; i++) {
    output[i] = input[(int) round(i / factor)];
  }
}

#ifdef SINC
void SincInterp(double *input, int ons, double *output, int ns, fftw_complex *ffty,
                fftw_complex *fftyp) {
  static fftw_plan p_fwd, p_bwd;
#ifdef _OPENMP
#pragma omp single
#endif
  p_fwd = fftw_plan_dft_r2c_1d(ons, input, ffty, FFTW_ESTIMATE);
  fftw_execute(p_fwd);
#ifdef _OPENMP
#pragma omp barrier
#endif
#ifdef _OPENMP
#pragma omp single nowait
#endif
  fftw_destroy_plan(p_fwd);
#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (int i = 0; i < (ons / 2 + 1); ++i) {
    fftyp[i][0] = ffty[i][0];
    fftyp[i][1] = ffty[i][1];
  }
#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (int i = (ons / 2 + 1); i < (ns / 2 + 1); ++i) {
    fftyp[i][0] = fftyp[i][1] = 0.0;
  }
#ifdef _OPENMP
#pragma omp single
#endif
  p_bwd = fftw_plan_dft_c2r_1d(ns, fftyp, output, FFTW_ESTIMATE);
  fftw_execute(p_bwd);
#ifdef _OPENMP
#pragma omp barrier
#endif
#ifdef _OPENMP
#pragma omp single nowait
#endif
  fftw_destroy_plan(p_bwd);
#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (int i = 0; i < ns; ++i) {
    output[i] /= ons;
  }
}
#endif

void Utils::Laplacian(double *arr, double *nabla2, Grid &grid, double *dudx = nullptr,
                      double *dudy = nullptr, double *dudz = nullptr, int omp_chunk) {
  int xi, yi, zi, ind, d;
  float _dudx, _dudy, _dudz;
  const int fNc = Modeling::gNc;
  const double *fC = Modeling::gC;
#ifdef _OPENMP
#pragma omp for collapse(3) OMP_SCHEDULE_AUTOTUNING
#endif
  for (xi = fNc - 1; xi < grid.nx - fNc + 1; xi++) {
    for (yi = fNc - 1; yi < grid.ny - fNc + 1; yi++) {
      for (zi = fNc - 1; zi < grid.nz - fNc + 1; zi++) {
        ind = (xi * grid.ny + yi) * grid.nz + zi;
        // dudx
        _dudx = fC[0] * arr[ind];
        for (d = 1; d < fNc; d++) {
          _dudx += fC[d] * (arr[ind + d * grid.ny * grid.nz] + arr[ind - d * grid.ny * grid.nz]);
        }
        _dudx /= grid.dx * grid.dx;
        // dudy
        _dudy = fC[0] * arr[ind];
        for (d = 1; d < fNc; d++) {
          _dudy += fC[d] * (arr[ind + d * grid.nz] + arr[ind - d * grid.nz]);
        }
        _dudy /= grid.dy * grid.dy;
        // dudz
        _dudz = fC[0] * arr[ind];
        for (d = 1; d < fNc; d++) {
          _dudz += fC[d] * (arr[ind + d] + arr[ind - d]);
        }
        _dudz /= grid.dz * grid.dz;
        // nabla2
        nabla2[ind] = _dudx + _dudy + _dudz;

        if (dudx != nullptr)
          dudx[ind] = _dudx;
        if (dudy != nullptr)
          dudy[ind] = _dudy;
        if (dudz != nullptr)
          dudz[ind] = _dudz;
      }
    }
  }
}