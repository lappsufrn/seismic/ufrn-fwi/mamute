#ifndef WAVEFIELD_H
#define WAVEFIELD_H

#include "Grid.h"

#define FILENAME_WAVEFIELD_SHOT "wf_shot_#.bin"

class Wavefield {
public:
  explicit Wavefield(int n);
  void Save(const std::string &projDir, const Grid &grid, int ti, int ishot) const;
  static void getFwd(double *fwdwf, const std::string &projDir, const Grid &grid, int ti,
                     int ishot);
  void Reset(int n);
  ~Wavefield() {
    delete[] p_new;
    delete[] current;
  }

public:
  double *p_new = nullptr;
  double *current = nullptr;
};

#endif   // Wavefield_h
