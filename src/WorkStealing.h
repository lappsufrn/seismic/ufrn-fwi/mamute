#ifndef WORKSTEALING_H
#define WORKSTEALING_H
#include <mpi.h>

//!**
//!  * Work Stealing class with Windows MPI method.
//!  * Creates the struct of Work Stealing and apply One-Sided communication
//!  (MPI)
//!  */
class WorkStealing {

private:
  int wsf;   // with (1) ; without (0) workstealing
  int *token = nullptr;
  int n_process;              // number of process
  int id;                     // process id
  int next_id;                // next process id
  int max_p_node;             // maximum number of tasks per process
  int *n_task_p = nullptr;    // list of number of tasks per process
  int *list_tp = nullptr;     // list of tasks of each process
  int *list_head = nullptr;   // list head
  int *list_tail = nullptr;   // list tail
  int n_task;                 // total number of tasks

  // Windows
  MPI_Win tokenW;
  MPI_Win ntpW;
  MPI_Win ltpW;
  MPI_Win lhW;
  MPI_Win ltW;

public:
  WorkStealing(int n_task, int n_process, int id, int ws_flag);
  void getTask(int &tid);
  int stealTasks(int, int &tid);
  int findVictim();
  void checkToken();
  void passToken();
#ifdef VERBOSE
  void printWindows() const;
#endif
  ~WorkStealing() {
    MPI_Win_free(&tokenW);
    MPI_Win_free(&ntpW);
    MPI_Win_free(&ltpW);
    MPI_Win_free(&lhW);
    MPI_Win_free(&ltW);
  }
};
#endif
