#include "Adjoint.h"
#include "WorkStealing.h"

#ifdef MAMUTE_WF_MGMT_CHK
#include "ManageWavefieldChk.h"
#elif MAMUTE_WF_MGMT_DISK
#include "ManageWavefieldDisk.h"
#else   // MAMUTE_WF_MGMT_MEM
#include "ManageWavefieldMemory.h"
#endif

#ifdef VERBOSE
int rep = 0;
#endif

/**
 * Constructor.
 * @param m Velocity model.
 * @param proj_dir Project directory.
 * @param n_src Number of sources.
 * @param fpeak Peak frequency.
 * @param ws_flag Work Stealing flag.
 * @param ox Origin of coordinate x in meters.
 * @param oy Origin of coordinate y in meters.
 * @param oz Origin of coordinate z in meters.
 * @param dx Discretization of coordinate x in meters.
 * @param dy Discretization of coordinate y in meters.
 * @param dz Discretization of coordinate z in meters.
 * @param ns Number of time samples.
 * @param dt Length of time samples in meters.
 * @param gradient_preconditioning_mode Gradient preconditioning mode.
 * @param Lx Bessel filter of coordinate x (optional, default = 2 * dx).
 * @param Ly Bessel filter of coordinate y (optional, default = 2 * dy).
 * @param Lz Bessel filter of coordinate z (optional, default = 2 * dz).
 * @param zeroes_nplanes_gradient Number of planes to set the gradient = 0 (optional, default = 0).
 * @param use_default_check_mem (optional, default = true, used if MAMUTE_WF_MGMT_CHK is defined).
 * @param check_mem Ckpt memory (optional, default = 0.8, used if MAMUTE_WF_MGMT_CHK is defined).
 * @param chk_verb Ckpt verbose (optional, default = 0, used if MAMUTE_WF_MGMT_CHK is defined).
 */
Adjoint::Adjoint(VelocityModel *m, const std::string &proj_dir, int n_src, double fpeak,
                 int ws_flag, double ox, double oy, double oz, double dx, double dy, double dz,
                 int ns, double dt, int gradient_preconditioning_mode, double Lx, double Ly,
                 double Lz, int zeroes_nplanes_gradient, bool use_default_check_mem,
                 double check_mem, int chk_verb, DensityModel *density)
    : model(m), density(density) {

  // cppcheck-suppress noCopyConstructor
  // cppcheck-suppress noOperatorEq
  wf = new Wavefield(model->fgrid.n);
  gradient = new double[model->fgrid.on];   // gradient volume
#ifdef FT
  gradient_parcial = new double[model->fgrid.on];   // gradient volume to save local
#endif
  setProjDir(proj_dir);
  setNumberSrc(n_src);
  setFreq(fpeak);
  setFlagWS(ws_flag);
  setOrigins(ox, oy, oz);
  setDiscretization(dx, dy, dz);
  setGradientPreconditioningMode(gradient_preconditioning_mode);
  if (Lx == -1.0) {
    Lx = 2 * model->fgrid.dx;
  }
  if (Ly == -1.0) {
    Ly = 2 * model->fgrid.dy;
  }
  if (Lz == -1.0) {
    Lz = 2 * model->fgrid.dz;
  }
  setGradientPreconditioningFilters(Lx, Ly, Lz);
  setNplanes(zeroes_nplanes_gradient);

#ifdef MAMUTE_WF_MGMT_CHK
  this->use_default_check_mem = use_default_check_mem;
  if (!use_default_check_mem) {
    this->check_mem = check_mem;
  }
  this->chk_verb = chk_verb;
#endif

  misfit = 0;
  t_misfit = 0;

  modl = new Modeling(model->fgrid, *model, ns, dt, fpeak, *density);

#ifdef MAMUTE_AUTOTUNING
  int nthd = omp_get_max_threads();
  at = new Autotuning(1, 1, model->fgrid.n / (nthd * 2), 1, 4, 100);
#endif

  try {
    // cppcheck-suppress noOperatorEq
    rcv_per_shot = new int[n_src];
  } catch (const std::exception &e) {
    std::string s("rcv_per_shot allocation failed: ");
    throw s + e.what();
  }

  // Compute number of receivers from files
  max_rcv = 0;
  BinaryFileHandler fileHandler(proj_dir);
  for (int i = 0; i < n_src; i++) {
    long fileSize = fileHandler.CountData<double>(FILENAME_RECEIVER_COORDINATES, i);
    rcv_per_shot[i] = fileSize / 3;
    if (rcv_per_shot[i] > max_rcv) {
      max_rcv = rcv_per_shot[i];
    }
  }
  n_rcv = max_rcv;

  // Check if source and receivers coordinates are inside grid's limit
  checkInputData(FILENAME_SOURCE_COORDINATES, FILENAME_RECEIVER_COORDINATES, proj_dir, rcv_per_shot,
                 ox, oy, oz, dx, dy, dz, model->fgrid.onx, model->fgrid.ony, model->fgrid.onz,
                 n_src);
}

/**
 * Destructor.
 */
Adjoint::~Adjoint() {
  delete[] gradient;
  delete[] rcv_per_shot;
  delete wf;
  delete modl;
#ifdef FT
  delete[] gradient_parcial;
#endif
#ifdef MAMUTE_AUTOTUNING
  delete at;
#endif
}

// cppcheck-suppress unusedFunction
void Adjoint::Run() {
  int id, comm_sz;
  MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);
  // cppcheck-suppress variableScope
  int ishot;
  double *lgradient, *fwd_wavefield, *fwd_wfp1, *fwd_wfm1, *fwd_D2u;
  WorkStealing *ws;
  SeismicData *src;   // source
#ifdef MAMUTE_WF_MGMT_CHK
  ManageWavefieldChk *wfMgmt;
#elif MAMUTE_WF_MGMT_DISK
  ManageWavefieldDisk *wfMgmt;
#else   // MAMUTE_WF_MGMT_MEM
  ManageWavefieldMemory *wfMgmt;
#endif

  //! try allocation
  try {
    lgradient = new double[model->fgrid.on];       // gradient volume for each process
    fwd_wavefield = new double[model->fgrid.on];   // forward wavefield
    fwd_wfp1 = new double[model->fgrid.on];        // forward wavefield plus 1,
                                                   // used for d2udt2
    fwd_wfm1 = new double[model->fgrid.on];        // forward wavefield minus 1,
                                                   // used for d2udt2
    fwd_D2u = new double[model->fgrid.on];         // second derivative of forward
                                                   // wavefield
    ws = new WorkStealing(n_src, comm_sz, id, WS_FLAG);
    src = new SeismicData(1, modl->GetOns(), modl->GetOdt());
#ifdef MAMUTE_WF_MGMT_CHK
    wfMgmt = new ManageWavefieldChk(*wf, model->fgrid, *this, *ws, *src, modl->GetNs(),
                                    use_default_check_mem, check_mem);
    wfMgmt->Print(model->fgrid.n, id, rep, chk_verb);
#elif MAMUTE_WF_MGMT_DISK
    wfMgmt = new ManageWavefieldDisk(*wf, model->fgrid, proj_dir);
#else   // MAMUTE_WF_MGMT_MEM
    wfMgmt = new ManageWavefieldMemory(*wf, model->fgrid, modl->GetNs());
#endif
  } catch (const std::exception &e) {
    std::cout << "Memory allocation failed: " << e.what() << std::endl;
    throw std::runtime_error(e.what());
  }

  // Print information about execution parameters.
#ifdef VERBOSE
  ws->printWindows();
  if (!id && !rep) {
    std::cout << "-----------3D FWI and MODELING-----------" << std::endl;
#ifdef MAMUTE_WF_MGMT_CHK
    std::cout << "--- CHECKPOINTING ----" << std::endl;
#elif MAMUTE_WF_MGMT_DISK
    std::cout << "--- DISK ----" << std::endl;
#else   // MAMUTE_WF_MGMT_MEM
    std::cout << "--- MEMORY ----" << std::endl;
#endif
    if (WS_FLAG) {
      std::cout << "--- WORK STEALING ----" << std::endl;
    } else {
      std::cout << "--- WITHOUT WORK STEALING ----" << std::endl;
    }
    std::cout << "-------------------------------------------" << std::endl;
  }
#endif

  SL *position = new SL(n_src);   // save all position
  position->setPositionFromFile(FILENAME_SOURCE_COORDINATES, proj_dir, ox, oy, oz, dx, dy, dz,
                                n_src);
  src->x[0] = position->x[0];
  src->y[0] = position->y[0];
  src->z[0] = position->z[0];
  src->ReadSourceFromFile(proj_dir);   // readSource into data!
#ifdef VERBOSE
  src->Print("SOURCES", id);
#endif

  // Valid condition
  if (!modl->ValidConditions(fpeak, id)) {
    throw std::runtime_error("Check CFL (Courant–Friedrichs–Lewy) criteria!");
  }
  modl->Interpolate(src);

  SeismicData *dobs = new SeismicData(max_rcv, modl->GetOns(), modl->GetOdt());   // Observed
  SeismicData *dcalc = new SeismicData(max_rcv, modl->GetOns(),
                                       modl->GetOdt());   // calculated

  model->FindMaxMin();
  model->ExpandModel(modl->GetDt());
#ifdef MAMUTE_DAMPING
  model->fgrid.ComputeDampingCoefficients(modl->fG1, modl->fG2, fpeak, modl->GetDt(), id);
#endif

#ifdef MAMUTE_AUTOTUNING
#define OMP_SHARED_AUTOTUNING shared(at)
#else
#define OMP_SHARED_AUTOTUNING
#endif

#ifdef _OPENMP
#pragma omp parallel default(none)                                                                 \
    shared(model, wf, position, dcalc, src, fwd_wavefield, fwd_wfm1, fwd_wfp1, fwd_D2u, dobs,      \
               lgradient, ws, ishot, proj_dir, wfMgmt) OMP_SHARED_DELIA OMP_SHARED_AUTOTUNING
#endif

  {
    SetZero(lgradient, model->fgrid.on);
    SetZero(gradient, model->fgrid.on);
    ws->getTask(ishot);
#ifdef FT
    if (DeLIA_CanRecoverLocalCheckpointing()) {
      SetZero(gradient_parcial, model->fgrid.on);
#ifdef _OPENMP
#pragma omp single
#endif
      {
        shots_processed.clear();
        DeLIA_ReadLocalData();
        while (shots_processed.find(ishot) != shots_processed.end()) {
          std::cerr << "Already processed in " << id << ":" << ishot << std::endl;
          ws->getTask(ishot);
          if (ishot == -1)
            break;
        }
      }
    } else {
#ifdef _OPENMP
#pragma omp single
#endif
      { shots_processed.clear(); }
    }
#endif

    t_misfit = 0;
    misfit = 0;

#ifdef MAMUTE_AUTOTUNING
    wStart(at, &(modl->omp_chunk));
    modl->ModelingStep(*src, *wf, 0, 0);
    wEnd(at);

    SetZero(lgradient, model->fgrid.on);
    SetZero(gradient, model->fgrid.on);
#endif

    while (ishot != -1) {
      dcalc->Resize(modl->GetOns(), modl->GetOdt());
      readShotFromFiles(ishot, src, position, dobs);
      dobs->setPositionFromFile(proj_dir, ox, oy, oz, dx, dy, dz, ishot, rcv_per_shot[ishot]);
      dcalc->setPositionFromFile(proj_dir, ox, oy, oz, dx, dy, dz, ishot, rcv_per_shot[ishot]);
      wf->Reset(model->fgrid.n);
#ifdef MAMUTE_CPML
      modl->fCPML->Reset();
#endif
      for (int ti = 0; ti < modl->GetNs(); ti++) {
        ws->checkToken();
        modl->ModelingStep(*src, *wf, ti, 1);
        modl->ReadReceiver(*dcalc, *wf, ti);
        wfMgmt->SaveWavefield(ti, ishot);
      }
      // BACKWARD
      dcalc->subData(*dobs);      // sub and save in dcalc.
      misfit = dcalc->misfit();   // return the misfit for dcalc
      modl->Interpolate(dcalc);
#ifdef _OPENMP
#pragma omp single
#endif
      { t_misfit += misfit; }

      wf->Reset(model->fgrid.n);
#ifdef MAMUTE_CPML
      modl->fCPML->Reset();
#endif
#ifdef MAMUTE_WF_MGMT_CHK
      wfMgmt->BackwardSetup();
#endif
      for (int ti = modl->GetNs() - 1; ti >= 0; ti--) {
        ws->checkToken();
        modl->ModelingStep(*dcalc, *wf, ti, -1);
        if (ti >= modl->GetNs() - 1) {   // Computing d2udt for the last time
                                         // sample of the forward wavefield
          wfMgmt->RetrieveWavefield(fwd_wavefield, ti, ishot);
          wfMgmt->RetrieveWavefield(fwd_wfm1, ti - 1, ishot);
          d2uLast(fwd_D2u, fwd_wavefield, fwd_wfm1, model->fgrid.on, modl->GetDt());
        } else if (ti == 0) {   // Computing d2udt for the first time
                                // sample of the forward wavefield
          d2uFirst(fwd_D2u, fwd_wavefield, fwd_wfp1, model->fgrid.on, modl->GetDt());
        } else {   // Computing d2udt for the central time samples of
                   // the forward wavefield
          wfMgmt->RetrieveWavefield(fwd_wfm1, ti - 1, ishot);
          d2uCenter(fwd_D2u, fwd_wavefield, fwd_wfp1, fwd_wfm1, model->fgrid.on, modl->GetDt());
        }
        imgCond(lgradient, fwd_D2u);
#ifdef _OPENMP
#pragma omp single
#endif
        {
          double *tmp;
          tmp = fwd_wfp1;
          fwd_wfp1 = fwd_wavefield;
          fwd_wavefield = fwd_wfm1;
          fwd_wfm1 = tmp;
        }
      }
#ifndef MAMUTE_WF_MGMT_MEM
      wfMgmt->EndShot(ishot);
#endif
#ifdef FT
#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
      for (int i = 0; i < model->fgrid.on; i++) {
        gradient_parcial[i] = lgradient[i];
      }
#ifdef _OPENMP
#pragma omp single
#endif
      {
        misfit_parcial = t_misfit;
        shots_processed.insert(ishot);
      }
#endif
      ws->getTask(ishot);
    }   // End while
  }   // End OPEMMP

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &t_misfit, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(lgradient, gradient, model->fgrid.on, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  MultiplyAdjoint();   //  2/v^3

  GradientPreconditioning();

  ZeroesNearSurface(gradient);

#ifdef VERBOSE
  rep++;
#endif
  delete wfMgmt;
  delete ws;
  delete src;
  delete position;
  delete dobs;
  delete dcalc;
  delete[] lgradient;
  delete[] fwd_wavefield;
  delete[] fwd_wfm1;
  delete[] fwd_wfp1;
  delete[] fwd_D2u;
}

/**
 * Static function that calculates the gradient.
 * @param gradient Calculated gradient (output).
 * @param total_misfit Calculated total misfit (output).
 * @param proj_dir Project directory.
 * @param velocity_filename Velocity filename.
 * @param n_src Number of sources.
 * @param fpeak Peak frequency.
 * @param ws_flag Work Stealing flag.
 * @param ox Origin of coordinate x in meters.
 * @param oy Origin of coordinate y in meters.
 * @param oz Origin of coordinate z in meters.
 * @param dx Discretization of coordinate x in meters.
 * @param dy Discretization of coordinate y in meters.
 * @param dz Discretization of coordinate z in meters.
 * @param nx Number of points in x direction.
 * @param ny Number of points in y direction.
 * @param nz Number of points in z direction.
 * @param border Size of the absorbing border.
 * @param stencil Stencil.
 * @param ns Number of time samples.
 * @param dt Length of time samples in meters.
 * @param gradient_preconditioning_mode Gradient preconditioning mode.
 * @param Lx Bessel filter of coordinate x (optional, default = 2 * dx).
 * @param Ly Bessel filter of coordinate y (optional, default = 2 * dy).
 * @param Lz Bessel filter of coordinate z (optional, default = 2 * dz).
 * @param zeroes_nplanes_gradient Number of planes to set the gradient = 0 (optional, default = 0).
 * @param use_default_check_mem (optional, default = true, used if MAMUTE_WF_MGMT_CHK is defined).
 * @param check_mem Ckpt memory (optional, default = 0.8, used if MAMUTE_WF_MGMT_CHK is defined).
 * @param chk_verb Ckpt verbose (optional, default = 0, used if MAMUTE_WF_MGMT_CHK is defined).
 */
// cppcheck-suppress unusedFunction
void Adjoint::StaticRun(double *gradient, double *total_misfit, const std::string &proj_dir,
                        const std::string &velocity_filename, int n_src, double fpeak, int ws_flag,
                        double ox, double oy, double oz, double dx, double dy, double dz, int nx,
                        int ny, int nz, int border, int stencil, int ns, double dt,
                        int gradient_preconditioning_mode, double Lx, double Ly, double Lz,
                        int zeroes_nplanes_gradient, bool use_default_check_mem, double check_mem,
                        int chk_verb, const std::string &density_filename) {
  Grid *g = new Grid(stencil, dx, dy, dz, border, nx, ny, nz);
  VelocityModel *m = new VelocityModel(proj_dir, velocity_filename, *g);
  DensityModel *d = nullptr;
#ifdef MAMUTE_DENSITY
  d = new DensityModel(proj_dir, density_filename, *g);
#endif
  Adjoint *adjoint = new Adjoint(m, proj_dir, n_src, fpeak, ws_flag, ox, oy, oz, dx, dy, dz, ns, dt,
                                 gradient_preconditioning_mode, Lx, Ly, Lz, zeroes_nplanes_gradient,
                                 use_default_check_mem, check_mem, chk_verb, d);

  adjoint->Run();

  *total_misfit = adjoint->t_misfit;
  for (int i = 0; i < m->getSize(); i++) {
    gradient[i] = adjoint->gradient[i];
  }

  delete g;
  delete m;
#ifdef MAMUTE_DENSITY
  delete d;
#endif
  delete adjoint;
}

void Adjoint::GradientPreconditioning() {
  switch (gradient_preconditioning_mode) {
  case 1:
    BesselSmoothing3D(gradient, model->fgrid.onx, model->fgrid.ony, model->fgrid.onz,
                      model->fgrid.dx, model->fgrid.dy, model->fgrid.dz, Lx, Ly, Lz);
    break;
  case 2:
    LaplaceSmoothing3D(gradient, model->fgrid.onx, model->fgrid.ony, model->fgrid.onz,
                       model->fgrid.dx, model->fgrid.dy, model->fgrid.dz, Lx, Ly, Lz);
    break;
  }
}

/** Initialize shot data
 * Read observed data from files proj_dir/dobs_0.bin, ...,
 * proj_dir/dobs_{n_src-1}.bin n_rcv is yet the same for all shots
 */
void Adjoint::readShotFromFiles(int ishot, SeismicData *src, SL *src_pos, SeismicData *dobs) {
#ifdef _OPENMP
#pragma omp single
#endif
  {
    n_rcv = rcv_per_shot[ishot];
    src->x[0] = src_pos->x[ishot];
    src->y[0] = src_pos->y[ishot];
    src->z[0] = src_pos->z[ishot];

    BinaryFileHandler fileHandler(proj_dir);
    dobs->data = fileHandler.ReadFile<double>(FILENAME_OBSERVED_DATA, ishot);
  }
}

void Adjoint::imgCond(double *grad, double *fwdwf) const {
  int xi, yi, zi, ind, ind2;
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(3)
#endif
  for (xi = 0; xi < model->fgrid.onx; xi++) {
    for (yi = 0; yi < model->fgrid.ony; yi++) {
      for (zi = 0; zi < model->fgrid.onz; zi++) {
        ind = (xi * model->fgrid.ony + yi) * model->fgrid.onz + zi;
        ind2 = ((model->fgrid.border + xi) * model->fgrid.ny + (model->fgrid.border + yi)) *
                   model->fgrid.nz +
               (model->fgrid.border + zi);
        grad[ind] += (fwdwf[ind] * wf->current[ind2]);
      }
    }
  }
}

void Adjoint::MultiplyAdjoint() {
  int xi;

#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (xi = 0; xi < model->fgrid.on; xi++) {
    double v3 =
        -2 / (model->fOriginalModel[xi] * model->fOriginalModel[xi] * model->fOriginalModel[xi]);
    gradient[xi] = v3 * gradient[xi];
  }
}

// Zeroes the gradient near the surface
void Adjoint::ZeroesNearSurface(double *gradient) {
  if (nplanes <= 0)
    return;
  for (int i = 0; i < model->fgrid.onx; i++) {
    for (int j = 0; j < model->fgrid.ony; j++) {
      for (int k = 0; k < nplanes; k++) {
        gradient[(i * model->fgrid.ony + j) * model->fgrid.onz + k] = 0;
      }
    }
  }
}
