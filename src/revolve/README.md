# Revolve library

# **Reference**

Griewank, A., & Walther, A. (2000). Algorithm 799: revolve: an implementation of checkpointing for the reverse or adjoint mode of computational differentiation. ACM Transactions on Mathematical Software. https://doi.org/10.1145/347837.347846

From: https://math.uni-paderborn.de/ag/mathematik-und-ihre-anwendungen/software/
