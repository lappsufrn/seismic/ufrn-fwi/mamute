#ifdef FT
#ifndef FAULTTOLERANCE_H
#define FAULTTOLERANCE_H

#include "Adjoint.h"
#include "DeLIA.h"
#include "Optimizer.h"
#include "SimpleConfigTextFile.h"
#include <string>

class FaultTolerance {

public:
  FaultTolerance(Config *config, VelocityModel *model, Optimizer *optimizer, Adjoint *adjoint);
  ~FaultTolerance() {}
};

#endif   // FAULTTOLERANCE_H

#endif