#ifndef MAMAGE_WAVEFIELD_DISK_H
#define MAMAGE_WAVEFIELD_DISK_H

#include "ManageWavefield.h"
#include "utilities/BinaryFileHandler.h"

class ManageWavefieldDisk : public ManageWavefield {
private:
  std::string fProjDir;

public:
  ManageWavefieldDisk(Wavefield &wf, const Grid &grid, const std::string &projDir);
  void SaveWavefield(int ti, int ishot) override;
  void RetrieveWavefield(double *wf, int ti, int ishot) override;
  void EndShot(int ishot) override;
  ~ManageWavefieldDisk();
};

#endif
