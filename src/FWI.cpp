#include "FWI.h"

FWI::FWI::FWI(Config *config) : fConfig(config) {
  SimpleConfigTextFile *sconfig = static_cast<SimpleConfigTextFile *>(fConfig);
  fgrid =
      new Grid(sconfig->Get<int>("stencil"), sconfig->Get<double>("dx"), sconfig->Get<double>("dy"),
               sconfig->Get<double>("dz"), sconfig->Get<int>("border"), sconfig->Get<int>("nx"),
               sconfig->Get<int>("ny"), sconfig->Get<int>("nz"));
  fVelocityModel = new VelocityModel(fConfig->Get("proj_dir"), fConfig->Get("vel"), *fgrid);
#ifdef MAMUTE_DENSITY
  fDensity = new DensityModel(fConfig->Get("proj_dir"), fConfig->Get("density"), *fgrid);
#endif
  // cppcheck-suppress noCopyConstructor
  // cppcheck-suppress noOperatorEq
  fAdjoint = new Adjoint(
      fVelocityModel, fConfig->Get("proj_dir"), sconfig->Get<int>("n_src"),
      sconfig->Get<double>("fpeak"), sconfig->Get<int>("ws_flag"), sconfig->Get<double>("ox"),
      sconfig->Get<double>("oy"), sconfig->Get<double>("oz"), sconfig->Get<double>("dx"),
      sconfig->Get<double>("dy"), sconfig->Get<double>("dz"), sconfig->Get<double>("ns"),
      sconfig->Get<double>("dt"), sconfig->Get<int>("gradient_preconditioning_mode"),
      sconfig->exist("bessel_filter_lx") ? sconfig->Get<double>("bessel_filter_lx") : -1.0,
      sconfig->exist("bessel_filter_ly") ? sconfig->Get<double>("bessel_filter_ly") : -1.0,
      sconfig->exist("bessel_filter_lz") ? sconfig->Get<double>("bessel_filter_lz") : -1.0,
      sconfig->exist("zeroes_nplanes_gradient") ? sconfig->Get<int>("zeroes_nplanes_gradient") : 0,
      sconfig->exist("check_mem") ? false : true,
      sconfig->exist("check_mem") ? sconfig->Get<double>("check_mem") : 0.8,
      sconfig->exist("chk_verb") ? sconfig->Get<int>("chk_verb") : 0, fDensity);

  fOptimizer = new Optimizer(fVelocityModel, fConfig);
}

void FWI::FWI::Run() {
#ifdef FT
  FaultTolerance *ft = new FaultTolerance(fConfig, fVelocityModel, fOptimizer, fAdjoint);
#endif

#ifdef FT
  DeLIA_HeartbeatMonitoring_Init();
#endif

  while (!fOptimizer->HasConverged()) {
    fOptimizer->Update(fVelocityModel, fAdjoint);
// FT function
#ifdef FT
    DeLIA_SaveGlobalData();
    MPI_Barrier(MPI_COMM_WORLD);
#endif
  }
// FT function
#ifdef FT
  DeLIA_HeartbeatMonitoring_Finalize();
  DeLIA_Finalize();
  delete ft;
#endif
}
