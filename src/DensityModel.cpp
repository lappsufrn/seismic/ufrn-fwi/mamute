#include "DensityModel.h"
#include "Utils.h"

#include "cmath"

DensityModel::DensityModel(const std::string &proj_dir, const std::string &density_filename,
                           Grid &grid)
    : Model(proj_dir, density_filename, grid) {
  try {
    fMass = new double[fgrid.n];
    fDensityNabla2 = new double[fgrid.n];
    fInvSqrtRho = new double[fgrid.n];
  } catch (const std::exception &e) {
    std::string s("DensityModel::DensityModel allocation failed: ");
    throw s + e.what();
  }
  SetModel(proj_dir, density_filename);
  ExpandModel();
  SetDensityRequisits();
  Utils::Laplacian(fInvSqrtRho, fDensityNabla2, grid, nullptr, nullptr, nullptr);
  SetMass();
}

void DensityModel::SetDensityRequisits() {
  int ind;
  double aux;
#ifdef _OPENMP
#pragma omp parallel for schedule(auto) collapse(3) default(none) shared(fgrid) private(ind, aux)
#endif
  for (int xi = 0; xi < fgrid.nx; xi++) {
    for (int yi = 0; yi < fgrid.ny; yi++) {
      for (int zi = 0; zi < fgrid.nz; zi++) {
        ind = (xi * fgrid.ny + yi) * fgrid.nz + zi;
        aux = sqrt(fExpandedModel[ind]);
        fExpandedModel[ind] = aux;
        fInvSqrtRho[ind] = 1.0 / aux;
      }
    }
  }
}

void DensityModel::SetMass() {
  int ind;
#ifdef _OPENMP
#pragma omp parallel for schedule(auto) collapse(3) default(none) shared(fgrid) private(ind)
#endif
  for (int xi = 0; xi < fgrid.nx; xi++) {
    for (int yi = 0; yi < fgrid.ny; yi++) {
      for (int zi = 0; zi < fgrid.nz; zi++) {
        ind = (xi * fgrid.nx + yi) * fgrid.nz + zi;
        fMass[ind] = fExpandedModel[ind] * fDensityNabla2[ind];
      }
    }
  }
}

DensityModel::~DensityModel() {
  delete[] fMass;
  delete[] fDensityNabla2;
  delete[] fInvSqrtRho;
}