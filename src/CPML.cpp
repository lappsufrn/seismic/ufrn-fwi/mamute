#ifdef MAMUTE_CPML

#include "CPML.h"
#include <cmath>
#include <cstring>

constexpr double CPML::fC[];

CPML::CPML(Grid &grid, VelocityModel &vmodel, double dt, double fpeak)
    : fModel(vmodel), fGrid(grid) {
  // sizes of arrays
  int size_x = fGrid.border * fGrid.ny * fGrid.nz;
  int size_y = fGrid.nx * fGrid.border * fGrid.nz;
  int size_z = fGrid.nx * fGrid.ny * fGrid.border;
  try {
    fCoeffa.x = new double[fGrid.border]();
    fCoeffa.y = new double[fGrid.border]();
    fCoeffa.z = new double[fGrid.border]();
    fCoeffb.x = new double[fGrid.border]();
    fCoeffb.y = new double[fGrid.border]();
    fCoeffb.z = new double[fGrid.border]();
    fCoeffar.x = new double[fGrid.border]();
    fCoeffar.y = new double[fGrid.border]();
    fCoeffar.z = new double[fGrid.border]();
    fCoeffbr.x = new double[fGrid.border]();
    fCoeffbr.y = new double[fGrid.border]();
    fCoeffbr.z = new double[fGrid.border]();
    fEta.x = new double[size_x]();
    fEta.y = new double[size_y]();
    fEta.z = new double[size_z]();
    fEtar.x = new double[size_x]();
    fEtar.y = new double[size_y]();
    fEtar.z = new double[size_z]();
    fPsi.x = new double[size_x]();
    fPsi.y = new double[size_y]();
    fPsi.z = new double[size_z]();
    fPsir.x = new double[size_x]();
    fPsir.y = new double[size_y]();
    fPsir.z = new double[size_z]();
  } catch (const std::exception &e) {
    std::string s("CPML::CPML allocation failed: ");
    throw s + e.what();
  }
  SetCPMLCoefficients(dt, fpeak);
}

void CPML::SetCPMLCoefficients(double dt, double fpeak) {
  // Auxiliar coefficients
  const double aux_vmax = (-1.5 * fModel.getVMax() * log(fCoeff)) / fGrid.border;
  const double aux_fpeak = M_PI * fpeak;
  // cppcheck-suppress variableScope
  double abs, abs2, alpha, sigma;
  // cppcheck-suppress variableScope
  int ir;
#ifdef _OPENMP
#pragma omp parallel for schedule(auto) default(none)                                              \
    shared(fGrid, aux_vmax, aux_fpeak, dt) private(abs, abs2, alpha, sigma, ir)
#endif
  for (int i = 0; i < fGrid.border - fNc + 1; i++) {
    abs = (fGrid.border - fNc + 1 - i) / (double) fGrid.border;
    abs2 = abs * abs;
    alpha = aux_fpeak * abs;
    ir = fGrid.border - 1 - i;
    // x direction coefficients
    sigma = aux_vmax * abs2 / fGrid.dx;
    fCoeffa.x[i] = fCoeffar.x[ir] = exp(-(sigma + alpha) * dt);
    fCoeffb.x[i] = fCoeffbr.x[ir] = sigma * (fCoeffa.x[i] - 1) / (sigma + alpha);
    // y direction coefficients
    sigma = aux_vmax * abs2 / fGrid.dy;
    fCoeffa.y[i] = fCoeffar.y[ir] = exp(-(sigma + alpha) * dt);
    fCoeffb.y[i] = fCoeffbr.y[ir] = sigma * (fCoeffa.y[i] - 1) / (sigma + alpha);
    // z direction coefficients
    sigma = aux_vmax * abs2 / fGrid.dz;
    fCoeffa.z[i] = fCoeffar.z[ir] = exp(-(sigma + alpha) * dt);
    fCoeffb.z[i] = fCoeffbr.z[ir] = sigma * (fCoeffa.z[i] - 1) / (sigma + alpha);
  }
}

void CPML::ApplyCPML(Wavefield &wf, double *dudx, double *dudy, double *dudz) {
  int ind, indl, indaux;
  double dudq, dpsidq;
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(3)
#endif
  // Front and back border coefficients
  for (int xi = fNc - 1; xi < fGrid.border - fNc + 1; xi++) {
    for (int yi = fNc - 1; yi < fGrid.ny - fNc + 1; yi++) {
      for (int zi = fNc - 1; zi < fGrid.nz - fNc + 1; zi++) {
        // Back
        ind = (xi * fGrid.ny + yi) * fGrid.nz + zi;
        dudq = fC[0] * wf.current[ind];
        for (int d = 1; d < fNc; d++) {
          dudq += fC[d] * (wf.current[ind + d * fGrid.ny * fGrid.nz] -
                           wf.current[ind - d * fGrid.ny * fGrid.nz]);
        }
        dudq /= fGrid.dx;
        fPsi.x[ind] = fCoeffa.x[xi] * fPsi.x[ind] + fCoeffb.x[xi] * dudq;
        // Front
        indl = ((fGrid.end_x + xi) * fGrid.ny + yi) * fGrid.nz + zi;
        dudq = fC[0] * wf.current[indl];
        for (int d = 1; d < fNc; d++) {
          dudq += fC[d] * (wf.current[indl + d * fGrid.ny * fGrid.nz] -
                           wf.current[indl - d * fGrid.ny * fGrid.nz]);
        }
        dudq /= fGrid.dx;
        fPsir.x[ind] = fCoeffar.x[xi] * fPsir.x[ind] + fCoeffbr.x[xi] * dudq;
      }
    }
  }
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(3)
#endif
  // Front and back border apply boundary
  for (int xi = fNc - 1; xi < fGrid.border - fNc + 1; xi++) {
    for (int yi = fNc - 1; yi < fGrid.ny - fNc + 1; yi++) {
      for (int zi = fNc - 1; zi < fGrid.nz - fNc + 1; zi++) {
        // Back
        ind = (xi * fGrid.ny + yi) * fGrid.nz + zi;
        dpsidq = fC[0] * fPsi.x[ind];
        for (int d = 1; d < fNc; d++) {
          dpsidq += fC[d] *
                    (fPsi.x[ind + d * fGrid.ny * fGrid.nz] - fPsi.x[ind - d * fGrid.ny * fGrid.nz]);
        }
        dpsidq /= fGrid.dx;
        fEta.x[ind] = fCoeffa.x[xi] * fEta.x[ind] + fCoeffb.x[xi] * (dpsidq + dudx[ind]);
        wf.p_new[ind] += fModel.fExpandedModel[ind] * (fEta.x[ind] + dpsidq);
        // Front
        indl = ((fGrid.end_x + xi) * fGrid.ny + yi) * fGrid.nz + zi;
        dpsidq = fC[0] * fPsir.x[ind];
        for (int d = 1; d < fNc; d++) {
          dpsidq += fC[d] * (fPsir.x[ind + d * fGrid.ny * fGrid.nz] -
                             fPsir.x[ind - d * fGrid.ny * fGrid.nz]);
        }
        dpsidq /= fGrid.dx;
        fEtar.x[ind] = fCoeffar.x[xi] * fEtar.x[ind] + fCoeffbr.x[xi] * (dpsidq + dudx[indl]);
        wf.p_new[indl] += fModel.fExpandedModel[indl] * (dpsidq + fEtar.x[ind]);
      }
    }
  }
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(3)
#endif
  // Left and right border coefficients
  for (int xi = fNc - 1; xi < fGrid.nx - fNc + 1; xi++) {
    for (int yi = fNc - 1; yi < fGrid.border - fNc + 1; yi++) {
      for (int zi = fNc - 1; zi < fGrid.ny - fNc + 1; zi++) {
        // Left
        ind = (xi * fGrid.ny + yi) * fGrid.nz + zi;
        indaux = (xi * fGrid.border + yi) * fGrid.nz + zi;
        dudq = fC[0] * wf.current[ind];
        for (int d = 1; d < fNc; d++) {
          dudq += fC[d] * (wf.current[ind + d * fGrid.nz] - wf.current[ind - d * fGrid.nz]);
        }
        dudq /= fGrid.dy;
        fPsi.y[indaux] = fCoeffa.y[yi] * fPsi.y[indaux] + fCoeffb.y[yi] * dudq;
        // Right
        indl = (xi * fGrid.ny + (fGrid.end_y + yi)) * fGrid.nz + zi;
        dudq = fC[0] * wf.current[indl];
        for (int d = 1; d < fNc; d++) {
          dudq += fC[d] * (wf.current[indl + d * fGrid.nz] - wf.current[indl - d * fGrid.nz]);
        }
        dudq /= fGrid.dy;
        fPsir.y[indaux] = fCoeffar.y[yi] * fPsir.y[indaux] + fCoeffbr.y[yi] * dudq;
      }
    }
  }
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(3)
#endif
  // Left and right border apply boundary
  for (int xi = fNc - 1; xi < fGrid.nx - fNc + 1; xi++) {
    for (int yi = fNc - 1; yi < fGrid.border - fNc + 1; yi++) {
      for (int zi = fNc - 1; zi < fGrid.nz - fNc + 1; zi++) {
        // Left
        ind = (xi * fGrid.ny + yi) * fGrid.nz + zi;
        indaux = (xi * fGrid.border + yi) * fGrid.nz + zi;
        dpsidq = fC[0] * fPsi.y[indaux];
        for (int d = 1; d < fNc; d++) {
          dpsidq += fC[d] * (fPsi.y[indaux + d * fGrid.nz] - fPsi.y[indaux - d * fGrid.nz]);
        }
        dpsidq /= fGrid.dy;
        fEta.y[indaux] = fCoeffa.y[yi] * fEta.y[indaux] + fCoeffb.y[yi] * (dpsidq + dudy[ind]);
        wf.p_new[ind] += fModel.fExpandedModel[ind] * (fEta.y[indaux] + dpsidq);
        // Right
        indl = (xi * fGrid.ny + (fGrid.end_y + yi)) * fGrid.nz + zi;
        dpsidq = fC[0] * fPsir.y[indaux];
        for (int d = 1; d < fNc; d++) {
          dpsidq += fC[d] * (fPsir.y[indaux + d * fGrid.nz] - fPsir.y[indaux - d * fGrid.nz]);
        }
        dpsidq /= fGrid.dy;
        fEtar.y[indaux] = fCoeffar.y[yi] * fEtar.y[indaux] + fCoeffbr.y[yi] * (dpsidq + dudy[indl]);
        wf.p_new[indl] += fModel.fExpandedModel[indl] * (dpsidq + fEtar.y[indaux]);
      }
    }
  }
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(3)
#endif
  // Top and bottom border coefficients
  for (int xi = fNc - 1; xi < fGrid.nx - fNc + 1; xi++) {
    for (int yi = fNc - 1; yi < fGrid.ny - fNc + 1; yi++) {
      for (int zi = fNc - 1; zi < fGrid.border - fNc + 1; zi++) {
        // Top
        ind = (xi * fGrid.ny + yi) * fGrid.nz + zi;
        indaux = (xi * fGrid.ny + yi) * fGrid.border + zi;
        dudq = fC[0] * wf.current[ind];
        for (int d = 1; d < fNc; d++) {
          dudq += fC[d] * (wf.current[ind + d] - wf.current[ind - d]);
        }
        dudq /= fGrid.dz;
        fPsi.z[indaux] = fCoeffa.z[zi] * fPsi.z[indaux] + fCoeffb.z[zi] * dudq;
        // Bottom
        indl = (xi * fGrid.ny + yi) * fGrid.nz + (fGrid.end_z + zi);
        dudq = fC[0] * wf.current[indl];
        for (int d = 0; d < fNc; d++) {
          dudq += fC[d] * (wf.current[indl + d] - wf.current[indl - d]);
        }
        dudq /= fGrid.dz;
        fPsir.z[indaux] = fCoeffar.z[zi] * fPsir.z[indaux] + fCoeffbr.z[zi] * dudq;
      }
    }
  }
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(3)
#endif
  // Top and bottom border apply boundary
  for (int xi = fNc - 1; xi < fGrid.nx - fNc + 1; xi++) {
    for (int yi = fNc - 1; yi < fGrid.ny - fNc + 1; yi++) {
      for (int zi = fNc - 1; zi < fGrid.border - fNc + 1; zi++) {
        // Top
        ind = (xi * fGrid.ny + yi) * fGrid.nz + zi;
        indaux = (xi * fGrid.ny + yi) * fGrid.border + zi;
        dpsidq = fC[0] * fPsi.z[indaux];
        for (int d = 1; d < fNc; d++) {
          dpsidq += fC[d] * (fPsi.z[indaux + d] - fPsi.z[indaux - d]);
        }
        dpsidq /= fGrid.dz;
        fEta.z[indaux] = fCoeffa.z[zi] * fEta.z[indaux] + fCoeffb.z[zi] * (dpsidq + dudz[ind]);
        wf.p_new[ind] += fModel.fExpandedModel[ind] * (dpsidq + fEta.z[indaux]);
        // Bottom
        indl = (xi * fGrid.ny + yi) * fGrid.nz + (fGrid.end_z + zi);
        dpsidq = fC[0] * fPsir.z[indaux];
        for (int d = 1; d < fNc; d++) {
          dpsidq += fC[d] * (fPsir.z[indaux + d] - fPsir.z[indaux - d]);
        }
        dpsidq /= fGrid.dz;
        fEtar.z[indaux] = fCoeffar.z[zi] * fEtar.z[indaux] + fCoeffbr.z[zi] * (dpsidq + dudz[indl]);
        wf.p_new[indl] += fModel.fExpandedModel[indl] * (dpsidq + fEtar.z[indaux]);
      }
    }
  }
}

void CPML::Reset() {
  int size_x = fGrid.border * fGrid.ny * fGrid.nz;
  int size_y = fGrid.nx * fGrid.border * fGrid.nz;
  int size_z = fGrid.nx * fGrid.ny * fGrid.border;

  // TODO: It is possible to optmize this code with openmp parallel for, if necessary
  std::memset(fEta.x, 0, size_x * sizeof(double));
  std::memset(fEta.y, 0, size_y * sizeof(double));
  std::memset(fEta.z, 0, size_z * sizeof(double));
  std::memset(fEtar.x, 0, size_x * sizeof(double));
  std::memset(fEtar.y, 0, size_y * sizeof(double));
  std::memset(fEtar.z, 0, size_z * sizeof(double));
  std::memset(fPsi.x, 0, size_x * sizeof(double));
  std::memset(fPsi.y, 0, size_y * sizeof(double));
  std::memset(fPsi.z, 0, size_z * sizeof(double));
  std::memset(fPsir.x, 0, size_x * sizeof(double));
  std::memset(fPsir.y, 0, size_y * sizeof(double));
  std::memset(fPsir.z, 0, size_z * sizeof(double));
}

CPML::~CPML() {
  delete[] fCoeffa.x;
  delete[] fCoeffa.y;
  delete[] fCoeffa.z;
  delete[] fCoeffb.x;
  delete[] fCoeffb.y;
  delete[] fCoeffb.z;
  delete[] fCoeffar.x;
  delete[] fCoeffar.y;
  delete[] fCoeffar.z;
  delete[] fCoeffbr.x;
  delete[] fCoeffbr.y;
  delete[] fCoeffbr.z;
  delete[] fEta.x;
  delete[] fEta.y;
  delete[] fEta.z;
  delete[] fEtar.x;
  delete[] fEtar.y;
  delete[] fEtar.z;
  delete[] fPsi.x;
  delete[] fPsi.y;
  delete[] fPsi.z;
  delete[] fPsir.x;
  delete[] fPsir.y;
  delete[] fPsir.z;
}
#endif
