#ifndef MODEL_H
#define MODEL_H

#include "Config.h"
#include "Grid.h"

/**
 *
 *  \brief The Model class contains methods to set VelocityModel and DensityModel classes
 */
class Model {
public:
  Grid &fgrid;
  double *fExpandedModel = nullptr; /* Expanded model (model + borders) */
  double *fOriginalModel = nullptr; /* Original model */
  /**
   * @brief Set the model from the binary file
   *
   * @param model model's filename
   */
  void SetModel(const std::string &proj_dir, const std::string &model_filename);
  /**
   * @brief Expand the original model to the model with borders
   */
  virtual void ExpandModel();
  /**
   * @brief Construct a new Model object
   *
   * @param config
   * @param grid
   */
  explicit Model(const std::string &proj_dir, const std::string &model_filename, Grid &grid);
  /**
   * @brief Destroy the Model object
   */
  virtual ~Model();
  Model &operator=(const Model &model) = delete;

private:
  Model(const Model &model) = delete;
};
#endif