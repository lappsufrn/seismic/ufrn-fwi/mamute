#include "SimpleConfigTextFile.h"
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

// Timming strings: http://www.martinbroadhurst.com/how-to-trim-a-stdstring.html
std::string &ltrim(std::string &str, const std::string &chars = "\t\n\v\f\r ") {
  str.erase(0, str.find_first_not_of(chars));
  return str;
}

std::string &rtrim(std::string &str, const std::string &chars = "\t\n\v\f\r ") {
  str.erase(str.find_last_not_of(chars) + 1);
  return str;
}

std::string &trim(std::string &str, const std::string &chars = "\t\n\v\f\r ") {
  return ltrim(rtrim(str, chars), chars);
}

// Adapted from Solution 1.3:
// https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/
std::vector<std::string> split(const std::string &s, char delimiter) {
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter)) {
    token = trim(token);
    if (token.size() != 0) {
      tokens.push_back(token);
    }
  }
  return tokens;
}

void SimpleConfigTextFile::parser(void) {
  int count_line = 0;
  std::string line;
  std::ifstream file(GetFileName());
  if (!file.is_open()) {
    throw std::runtime_error("Failed to open config file.");
  }
  while (std::getline(file, line)) {
    ++count_line;
    line = trim(line);
    if (line.size() == 0) {   // Empty string
      continue;
    } else if (line.compare(0, 2, "//") == 0) {   // Comments
      continue;
    } else {   // Parse parameters
      std::vector<std::string> strings = split(line, '=');
      try {
        if (strings.size() == 0) {
          throw "Unknown error parsing config file";
        } else if (strings.size() < 2) {
          throw "Must be <key>=<value>";
        } else {
          auto pair = fFlags.insert(std::make_pair(strings[0], strings[1]));
          if (!pair.second) {
            throw "Key already defined";
          }
        }
      } catch (const char *e) {
        throw std::runtime_error("Parsing error at line " + std::to_string(count_line) + '\n' + e);
      }
    }
  }
  file.close();
}
