#ifndef _UTILS_H
#define _UTILS_H

#define BESSEL_TOL 1.0e-06

#include "SimpleConfigTextFile.h"
#include "utilities/BinaryFileHandler.h"
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mpi.h>
#include <omp.h>
#include <unistd.h>
#ifdef SINC
#include <fftw3.h>
#endif
#include "Grid.h"

/** --------------------------------
 * namespace to MPI functions
 *
 **/
namespace My_MPI {
void printTT(double *tt, int id, int npt);
/** Print time of reduciton **/
void printTimes(double *tt, int id, int npt, int nsrc, int comm_sz);
/** Save File in MPI **/
void SaveFile(const std::string &fileName, int ishot, int nsrc, int comm_sz, int id, double *data,
              int nf, std::string projectPath);

}   // namespace My_MPI

/** --------------------------------
 *  Namespace to manipulation of files
 * */
namespace MyFile {
void Save(double *img, const char name[], int size);
void SaveI(double *img, const char name[], int size);
}   // namespace MyFile

namespace {
template <typename T> void SetZero(T *buffer, size_t size, T data = 0) {
#ifdef _OPENMP
#pragma omp for simd schedule(auto)
#endif
  for (size_t i = 0; i < size; ++i) {
    buffer[i] = data;
  }
}
}   // namespace

/** --------------------------------
Function: D2uLast

Information:
Second derivative

Inputs:
fwdwf: forward wavefield
fwdm1: forward wavefield -1
dt :
ntotal: Size of model
Output: fwD2u
*/
void d2uLast(double *fwD2u, double *fwdwf, double *fwdm1, int ntotal, double dt);

/** --------------------------------
Function: d2uCenter

Information:
Second derivative

Inputs:
fwdwf: forward wavefield
fwdm1: forward wavefield -1
fwdp1: forward wavefield +1
dt :
ntotal: Size of model
Output: fwD2u
*/
void d2uCenter(double *fwD2u, double *fwdwf, double *fwdp1, double *fwdm1, int ntotal, double dt);

/** --------------------------------
Function: d2uFirst

Information:
Second derivative
Inputs:
fwdwf: forward wavefield
fwdp1: forward wavefield +1
dt :
ntotal: Size of model
Output: fwD2u
*/
void d2uFirst(double *fwD2u, double *fwdwf, double *fwdp1, int ntotal, double dt);

/** --------------------------------
Function: cfileexists

Information:
Check if a files exists
Inputs:
filename : Name of the file
Output: true if file exists; false otherwise
*/
bool FileExists(const char *filename);

/**
 * Check for invalid entries in the input data
 * This method is not thread safe
 */
void checkInputData(const std::string &fileNameSourceCoordinates,
                    const std::string &fileNameReceiverCoordinates, const std::string &proj_dir,
                    int *rcv_per_shot, double ox, double oy, double oz, double dx, double dy,
                    double dz, int nx, int ny, int nz, int n_src);

/**
 * Source location struct
 */
struct SL {
  int *x;   //! x coordinate
  int *y;   //! y coordinate
  int *z;   //! z coordinate

  explicit SL(int n) {
    try {
      // cppcheck-suppress noCopyConstructor
      // cppcheck-suppress noOperatorEq
      this->x = new int[n];
      this->y = new int[n];
      this->z = new int[n];
    } catch (std::bad_alloc &e) {
      std::cout << "Allocation failed: " << e.what() << std::endl;
    }
  };
  SL(const SL &sl) = delete;

  ~SL() {
    delete[] this->x;
    delete[] this->y;
    delete[] this->z;
  };

  /**
   * Read sources coordinates in meters from file proj_dir/src_coord.bin
   * x, y, z are cartesian coordinates do not interpolate.
   */
  void setPositionFromFile(const std::string &fileNameSourceCoordinates,
                           const std::string &proj_dir, const double ox, const double oy,
                           const double oz, const double dx, const double dy, const double dz,
                           const int n_src);
};
// dot product
double Dot(double *vetor1, double *vetor2, int n);

// vector norm
double Norm(double *vetor, int n);

// aux for BesselSmoothing3D
void OneMinusWeightedLaplacian(double *saida, double *vetor, int nx, int ny, int nz, double dx,
                               double dy, double dz, double Lx, double Ly, double Lz);

// Bessel Filter
void BesselSmoothing3D(double *data, int nx, int ny, int nz, double dx, double dy, double dz,
                       double Lx, double Ly, double Lz);

// Laplace Filter
void LaplaceSmoothing3D(double *data, int nx, int ny, int nz, double dx, double dy, double dz,
                        double Lx, double Ly, double Lz);

// nearest neighbor interpolation
void NNInterp(double *input, int ons, double *output, int ns);

// sinc interpolation
#ifdef SINC
void SincInterp(double *input, int ons, double *output, int ns, fftw_complex *ffty,
                fftw_complex *fftyp);
#endif

namespace Utils {
void Laplacian(double *arr, double *nabla2, Grid &grid, double *dudx, double *dudy, double *dudz,
               int omp_chunk = 500);
}   // namespace Utils
#endif