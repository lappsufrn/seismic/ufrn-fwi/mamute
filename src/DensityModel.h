#ifndef _DENSITYMODEL_H
#define _DENSITYMODEL_H

#include "Config.h"
#include "Grid.h"
#include "Model.h"

/**
 *  \brief The DensityModel class contains methods to apply modeling with density
 */
class DensityModel : public Model {
private:
  /**
   * @brief Takes the square root of the model and set fInvSqrtRho
   */
  void SetDensityRequisits();
  /**
   * @brief Construct a new Density Model object
   *
   * @param model
   */
  DensityModel(const DensityModel &model) = delete;

public:
  double *fMass = nullptr;          /* Represents the 'mass' of the wavefield */
  double *fInvSqrtRho = nullptr;    /* Inverse square root of the density model */
  double *fDensityNabla2 = nullptr; /* Laplacian of the inverse square root */
  /**
   * @brief Construct a new Density Model object
   *
   * @param config
   * @param grid
   */
  explicit DensityModel(const std::string &proj_dir, const std::string &density_filename,
                        Grid &grid);
  /**
   * @brief Set fMass
   */
  void SetMass();
  /**
   * @brief Destroy the Density Model object
   */
  ~DensityModel();
};
#endif