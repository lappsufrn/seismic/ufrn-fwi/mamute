#ifndef GRID_H
#define GRID_H

#include "Config.h"
#include "SimpleConfigTextFile.h"
#include <cstddef>
#include <fstream>
#include <iostream>
#include <mpi.h>

class Grid {
public:
  int start_x, end_x;
  int start_y, end_y;
  int start_z, end_z;
  int onx, ony, onz;
  int n, on;
  int nx, ny, nz;
  double dx, dy, dz;
  int border;
  int stencil;

  explicit Grid(int _stencil, double _dx, double _dy, double _dz, int _border, int _nx, int _ny,
                int _nz);
  void ComputeDampingCoefficients(double *coef1, double *coef2, double fpeak, double dt,
                                  int id) const;
};
#endif
