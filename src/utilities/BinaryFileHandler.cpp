#include "BinaryFileHandler.h"

/* Constructors and Destructors */

BinaryFileHandler::BinaryFileHandler(const std::string &projDir) : fProjDir(projDir) {}

BinaryFileHandler::~BinaryFileHandler() {}

/* Private methods */

void BinaryFileHandler::BuildFilePath(const std::string &fileName) {
  // Construct file path
  this->fFilePath = this->fProjDir + "/" + fileName;
}

std::string BinaryFileHandler::FormatFileName(const std::string &fileName, int id) {
  // Find placeholder index
  int replacePosition = fileName.find("#");

  // Construct file path
  return fileName.substr(0, replacePosition) +   // Before `#`
         std::to_string(id) +                    // Add id
         fileName.substr(replacePosition + 1,    // After `#`
                         fileName.length() - replacePosition);
}

/* Public methods */

bool BinaryFileHandler::ExistFile(const std::string &fileName) {
  bool exists = false;
  FILE *file = nullptr;

  BuildFilePath(fileName);

  // Try to open file for read
  file = fopen(this->fFilePath.c_str(), "r");
  if (file) {
    exists = true;
    fclose(file);
  }

  return exists;
}

bool BinaryFileHandler::ExistFile(const std::string &fileName, int id) {
  return ExistFile(FormatFileName(fileName, id));
}

std::string BinaryFileHandler::GetFilePath(const std::string &fileName) {
  BuildFilePath(fileName);
  return this->fFilePath;
}

std::string BinaryFileHandler::GetFilePath(const std::string &fileName, int id) {
  return GetFilePath(FormatFileName(fileName, id));
}

void BinaryFileHandler::DeleteFile(const std::string &fileName) {
  BuildFilePath(fileName);
  remove(this->fFilePath.c_str());
}

void BinaryFileHandler::DeleteFile(const std::string &fileName, int id) {
  DeleteFile(FormatFileName(fileName, id));
}
