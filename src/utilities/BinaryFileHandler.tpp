/**
 * Count the number of data of type `T`.
 * @tparam T type of data in the file.
 * @param fileName File name in the project directory.
 * @return Number of elements of type `T` in the file.
 */
template <typename T>
long CountData(const std::string &fileName)
{
  long count = 0;
  FILE *file;

  // Check if file exists
  if (!ExistFile(fileName)) {
    std::cerr << "File not found: " << this->fFilePath << std::endl;
    return count;
  }

  // Build complete path to file name
  BuildFilePath(fileName);

  file = fopen(this->fFilePath.c_str(), "rb");
  if (!file) {
    std::cerr << std::string("Cannot open file: ") + this->fFilePath;
    return count;
  }

  // Count data of type `T` in the file
  fseek(file, 0, SEEK_END);
  count = ftell(file) / sizeof(T);
  fclose(file);

  return count;
}
// cppcheck-suppress functionStatic
/**
 * Count the number of data of type `T`.
 * @tparam T type of data in the file.
 * @param fileName File name in the project directory. Example of pattern in the
 * file name: `dobs_#.bin`. The char `#` indicates where the `id`parameter will
 * be included.
 * @param id shot id to be concataned to in the file name.
 * @return Number of elements of type `T` in the file.
 */
template <typename T>
long CountData(const std::string &fileName, int id)
{
  return CountData<T>(FormatFileName(fileName, id));
}

/**
 * Read data from a binary file that is inside the project directory.
 * This method allocate data of the size of the file. Beware to free the data
 * after using it.
 *
 * *Beware to free the data after using it.*
 *
 * @tparam T type of data stored in the file.
 * @param fileName Name of the file to be read in the project directory.
 * @return Pointer to the beginning of an 1D array of type `T` which the length
 * is the amount of data in the file. Or `nullptr` if file does not exist or
 * can't be read.
 */
template <typename T>
T *ReadFile(const std::string &fileName)
{
  T *data = nullptr;
  int length = 0;

  // Check if file exists
  if (!ExistFile(fileName)) {
    // Not throwing exception.
    // Let the caller decide if the exception is necessary.
    std::cerr << "File not found: " << fFilePath << std::endl;
    return data;
  }

  // Get file size
  length = CountData<T>(fileName);

  // Construct file path
  BuildFilePath(fileName);

  // Get point to the binary file
  std::ifstream file(this->fFilePath, std::ios::binary);

  // Error creating file stream
  if (!file.is_open()) {
    std::cerr << "Cannot open file: " << this->fFilePath;
    return data;
  }

  // Allocate data
  data = new T[length];

  // Read data from file
  file.read((char *)data, length * sizeof(T));
  file.close();

  return data;
}

/**
 * Read data from a binary file that is inside the project directory.
 * This method allocate data of the size of the file.
 *
 * *Beware to free the data after using it.*
 *
 * @tparam T type of data stored in the file.
 * @param fileName File name in the project directory. Example of pattern in the
 * file name: `dobs_#.bin`. The char `#` indicates where the `id`parameter will
 * be included.
 * @param id shot id to be concataned to in the file name.
 * @return Pointer to the beginning of an 1D array of type `T` which the length
 * is the amount of data in the file. Or `nullptr` if file does not exist or
 * can't be read.
 */
template <typename T>
T *ReadFile(const std::string &fileName, int id)
{
  return ReadFile<T>(FormatFileName(fileName, id));
}

/**
 * Save data into a binary file inside the project directory.
 * This method overwrites existing files with the same name.
 *
 * @param fileName Name of the file to be saved in project directory.
 * @param data Pointer of an array of type `T` with the beginning of data to be
 * saved.
 * @param length Size of `data` to be written in the file.
 */
template <typename T>
void WriteFile(const std::string &fileName, T *data, int length)
{
  FILE *file;

  // Build complete file path from given file name
  BuildFilePath(fileName);

  file = fopen(this->fFilePath.c_str(), "wb");
  if (!file) {
    std::cerr << "Couldn't write file " << this->fFilePath << std::endl;
    return;
  }
  // Write data
  fwrite(data, sizeof(T), length, file);
  fclose(file);
}
// cppcheck-suppress functionStatic
/**
 * Save data into a binary file inside the project directory. This method
 *  overwrites existing files with the same name.
 *
 * @param fileName File name in the project directory. Example of pattern in the
 * file name: `dobs_#.bin`. The char `#` indicates where the `id`parameter will
 * be included.
 * @param id shot id to be concataned to in the file name.
 * @param data Pointer of an array of type `T` with the beginning of data to be
 * saved.
 * @param length Size of `data` to be written in the file.
 */
template <typename T>
void WriteFile(const std::string &fileName, int id, T *data, int length)
{
  WriteFile<T>(FormatFileName(fileName, id), data, length);
}