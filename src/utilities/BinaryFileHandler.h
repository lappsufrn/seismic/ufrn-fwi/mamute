#ifndef BINARY_FILE_HANDLER_H
#define BINARY_FILE_HANDLER_H

#include <fstream>
#include <iostream>
#include <stdexcept>

#define PATH_MAX    4096 /* # chars in a path name including nul */
#define NAME_MAX    255  /* # chars in a file name */
#define PLACEHOLDER "#"  /*Placeholder for id concatenation */

/**
 * Responsible to handle binary input data in mamute.
 */
class BinaryFileHandler {
private:
  /**
   * Path to the project directory.
   */
  std::string fProjDir;
  /**
   * Path to a file inside project directory.
   */
  std::string fFilePath;
  /**
   * Construct the complete file path to the target file.
   * This method updates the private variable `fFilePath`.
   *
   * @param fileName Name of the file in the project directory.
   */
  void BuildFilePath(const std::string &fileName);
  /**
   * Construct a file name using the formatted string with placeholder.
   *
   * @param fileName File name with placeholder located in the project
   * directory. Example of pattern in the file name: `dobs_#.bin`. The char
   * `#` indicates where the `id` parameter will be included.
   * @param id ID to be concatened after fileName.
   * @return String with the formatted string.
   */
  static std::string FormatFileName(const std::string &fileName, int id);

public:
  /**
   * Constructor.
   * @param projDir Path to the project input directory.
   */
  explicit BinaryFileHandler(const std::string &projDir);
  /**
   * Default destructor.
   */
  ~BinaryFileHandler();
  /**
   * Check if a file exists.
   *
   * @param fileName  Name of the file to be checked inside project directory.
   * @return `true` if the given filePath is a File, `false` otherwise.
   */
  bool ExistFile(const std::string &fileName);
  /**
   * Check if a file exists.
   * File name is built replacing the placeholder with `id`.
   *
   * @param fileName F ile name with placeholder located in the project
   * directory. Example of pattern in the file name: `dobs_#.bin`. The char
   * `#` indicates where the `id` parameter will be included.
   * @param id Integer to form the file name.
   * @return `true` if the given filePath is a File, `false` otherwise.
   */
  bool ExistFile(const std::string &fileName, int id);
  /**
   * Return the complete file path to the filename specified.
   * @param fileName Name of the file in the project directory
   */
  std::string GetFilePath(const std::string &fileName);
  /**
   * Return the complete file path to the filename specified.
   * @param fileName  File name with placeholder located in the project
   * directory. Example of pattern in the file name: `dobs_#.bin`. The char
   * `#` indicates where the `id` parameter will be included.
   * @param id Integer to form the file name.
   */
  std::string GetFilePath(const std::string &fileName, int id);
  /**
   * Delete a file located in the input directory with the given file name.
   * @param fileName Name of the file to be deleted.
   */
  void DeleteFile(const std::string &fileName);
  /**
   * Delete a file located in the input directory with the given file name.
   * @param fileName  File name with placeholder located in the project
   * directory. Example of pattern in the file name: `dobs_#.bin`. The char
   * `#` indicates where the `id` parameter will be included.
   * @param id Integer to form the file name.
   */
  void DeleteFile(const std::string &fileName, int id);

  // Include template functions
#include "BinaryFileHandler.tpp"
};

#endif   // BINARY_FILE_HANDLER_H