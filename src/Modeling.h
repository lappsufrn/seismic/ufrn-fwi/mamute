#ifndef MODELING_H
#define MODELING_H

#ifdef SINC
#include <fftw3.h>
#endif
#include "Config.h"
#include "DensityModel.h"
#include "SeismicData.h"
#include "VelocityModel.h"
#include "Wavefield.h"
#ifdef MAMUTE_CPML
#include "CPML.h"
#endif
#ifdef MAMUTE_AUTOTUNING
#define OMP_SCHEDULE_AUTOTUNING schedule(dynamic, omp_chunk)
#else
#define OMP_SCHEDULE_AUTOTUNING schedule(auto)
#endif

class Modeling {
private:
  int fNs;                                // modeling time samples
  int fOns;                               // i/o data's time samples
  int fTimeFactor;                        // dt/dtp
  double fDt;                             // time sampling
  double fOdt;                            // i/o data's time sampling
  double *fInterpolationData = nullptr;   // auxiliar for interpolation
  double *fNabla2 = nullptr;              // laplacian
  double *fDudx = nullptr;                // partial derivative in x direction
  double *fDudy = nullptr;                // partial derivative in y direction
  double *fDudz = nullptr;                // partial derivative in z direction
#ifdef MAMUTE_SPHERE
  int *xinf = nullptr;
  int *xsup = nullptr;
  int **yinf = nullptr;
  int **ysup = nullptr;
  int **zinf = nullptr;
  int **zsup = nullptr;
#endif

  Grid &fGrid;
  VelocityModel &fVelModel;
  Modeling(const Modeling &modl) = delete;
  Modeling &operator=(const Modeling &modl) = delete;
  inline void SourceInsertion(const SeismicData &in, Wavefield &wave, const int ti,
                              const int direction, const int ind, const int iin, const double dh3);
  inline void ModelWave(Wavefield &wave, const int ind);
#ifdef MAMUTE_FREE_SURFACE
  void FreeSurface(Wavefield &wave) const;
#endif
#ifdef MAMUTE_DENSITY
  DensityModel *fDensity = nullptr;
  Wavefield *fWave_q = nullptr;
  void RestorePressureField(Wavefield &wave) const;
#endif
#ifdef SINC
  fftw_complex *fFfty = nullptr;
  fftw_complex *fFftyp = nullptr;
#endif

public:
#ifdef MAMUTE_CPML
  CPML *fCPML = nullptr;
#endif
#ifdef MAMUTE_AUTOTUNING
  int omp_chunk;
#endif

#ifdef MAMUTE_DAMPING
  double *fG1 = nullptr, *fG2 = nullptr;
#endif
#ifdef MAMUTE_SPHERE
  void Sphere(const SeismicData &in, VelocityModel &vmodel);
#endif
  static constexpr int gNc = 5;   // order/2 + 1
  static constexpr double gC[gNc] = {-205.0 / 72, 8.0 / 5, -1.0 / 5, 8.0 / 315,
                                     -1.0 / 560};   // coefficients of the finite difference
  explicit Modeling(Grid &grid, VelocityModel &vmodel, int ns, double dt, double fpeak,
                    DensityModel &density);
  void ModelingStep(const SeismicData &in, Wavefield &wave, int ti, int direction);
  int GetNs() const { return fNs; }
  void SetNs(int modTimeSamples) { fNs = modTimeSamples; }
  int GetOns() const { return fOns; }
  void SetOns(int origTimeSamples) { fOns = origTimeSamples; }
  double GetDt() const { return fDt; }
  void SetDt(double modTimeSampling) { fDt = modTimeSampling; }
  double GetOdt() const { return fOdt; }
  void SetOdt(double origTimeSampling) { fOdt = origTimeSampling; }
  int GetTimeFactor() const { return fTimeFactor; }
  void SetTimeFactor(int timeFactor) { fTimeFactor = timeFactor; }
  int ValidConditions(double fpeak, int id = 0);
  void ReadReceiver(SeismicData &seisd, Wavefield &wave, int ti) const;
  void Interpolate(SeismicData *seisd);
  double *GetNabla2() const { return fNabla2; }
  ~Modeling();
};
#endif /* MODELING_H */