#ifdef MAMUTE_CPML
#ifndef CPML_H
#define CPML_H

#include "Config.h"
#include "VelocityModel.h"
#include "Wavefield.h"

/**
 *
 *  \brief The CPML class contains methods to apply absorbing boundary
 */
class CPML {
private:
  // Struct to represent the three dimensions of space
  struct cpml_dim {
    double *x = nullptr;
    double *y = nullptr;
    double *z = nullptr;
  };
  static constexpr int fNc = 5;   // Finite difference order/2 + 1
  static constexpr double fC[fNc] = {0.0, 4.0 / 5, -1.0 / 5, 4.0 / 105,
                                     -1.0 / 280};   // Finite difference Coefficients
  static constexpr double fCoeff = 1e-14;           // Necessary constant for CPML
  cpml_dim fCoeffa, fCoeffb, fCoeffar, fCoeffbr, fEta, fEtar, fPsi,
      fPsir;   // CPML's structs
  VelocityModel &fModel;
  Grid &fGrid;
  // Sets CPML's necessary coefficients to use in ApplyCPML
  void SetCPMLCoefficients(double dt, double fpeak);

public:
  /**
   * Constructor
   * @param config
   * @param grid
   * @param vmodel
   */
  explicit CPML(Grid &grid, VelocityModel &vmodel, double dt, double fpeak);
  // Apply CPML boundary effect
  void ApplyCPML(Wavefield &wf, double *dudx, double *dudy, double *dudz);

  /*!
   * @brief Reset CPML's arrays
   */
  void Reset();

  ~CPML();
};
#endif
#endif
