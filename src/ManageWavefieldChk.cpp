#include "ManageWavefieldChk.h"

ManageWavefieldChk::ManageWavefieldChk(Wavefield &wf, const Grid &grid, Adjoint &adj,
                                       WorkStealing &ws, SeismicData &src, int ns,
                                       bool use_default_check_mem, double custom_check_mem)
    : ManageWavefield{wf, grid}, fAdj(adj), fWs(ws), fSrc(src), fNs(ns) {
  if (use_default_check_mem) {
    fwicheck = new FWI::Checkpoint(fNs, fGrid.n);
  } else {
    fwicheck = new FWI::Checkpoint(fNs, fGrid.n, custom_check_mem);
  }
  fCheck_wf = new Wavefield(fGrid.n);
}

void ManageWavefieldChk::SaveWavefield(int ti, int ishot) {
  fwicheck->saveCheck(&fWf, fGrid.n, ti);
}

void ManageWavefieldChk::RetrieveWavefield(double *wf, int ti, int ishot) {
  fwicheck->getFwdChk(&fAdj, &fWs, wf, fCheck_wf, ti, fGrid, &fSrc);
}

void ManageWavefieldChk::EndShot(int ishot) {
#ifdef _OPENMP
#pragma omp single
#endif
  { fwicheck->resetCheck(); }
}

void ManageWavefieldChk::BackwardSetup() { fwicheck->bwdChkSetup(); }

void ManageWavefieldChk::Print(int n, int id, int rep, int chk_verb) {
#ifdef VERBOSE
  if (!id && !rep) {
    fwicheck->print(n, chk_verb);
  }
#endif
}

ManageWavefieldChk::~ManageWavefieldChk() {
  delete fwicheck;
  delete fCheck_wf;
}