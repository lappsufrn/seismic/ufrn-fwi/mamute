#include "WorkStealing.h"
#include <cmath>
#include <cstdlib>
#include <iostream>

//!
//! Constructor of WorkStealing wich defines the Windows and set class
//! parameters.
//!
WorkStealing::WorkStealing(int n_task, int n_process, int id, int ws_flag) {
  int lnt, mft, i, aux = n_task % n_process;
  // Windows
  MPI_Win_allocate(sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &(token), &(tokenW));
  MPI_Win_allocate(n_process * sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &(n_task_p),
                   &(ntpW));
  MPI_Win_allocate(sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &(list_head), &(lhW));
  MPI_Win_allocate(sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &(list_tail), &(ltW));
  // ws flag
  wsf = ws_flag;
  // number of process
  this->n_process = n_process;
  // process id
  this->id = id;
  // token
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, this->id, 0, tokenW);
  if (this->id) {
    token[0] = 0;
  } else {
    token[0] = 1;
  }
  MPI_Win_unlock(this->id, tokenW);
  // next process id
  next_id = (id + 1) % n_process;
  // maximum number of tasks per process
  max_p_node = ceil(((float) n_task) / n_process);
  // list of number of tasks per process
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, this->id, 0, ntpW);
  for (i = 0; i < n_process; i++) {
    n_task_p[i] = n_task / n_process + (i < aux);
  }
  MPI_Win_unlock(this->id, ntpW);
  // list of tasks of each process
  MPI_Win_allocate(max_p_node * sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &(list_tp),
                   &(ltpW));
  if (id < aux) {
    lnt = n_task / n_process + 1;
    mft = this->id * lnt;
  } else {
    lnt = n_task / n_process;
    mft = aux * (lnt + 1) + (id - aux) * lnt;
  }
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, this->id, 0, ltpW);
  for (i = 0; i < lnt; i++) {
    list_tp[i] = mft + i;
  }
  MPI_Win_unlock(this->id, ltpW);
  // list head window (lhw)
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, this->id, 0, lhW);
  list_head[0] = 0;
  MPI_Win_unlock(this->id, lhW);
  // list tail window (ltw)
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, this->id, 0, ltW);
  list_tail[0] = lnt - 1;
  MPI_Win_unlock(this->id, ltW);
  this->n_task = n_task;
}

//!
//! Method return a task from your List of task or from WorkStealing.
//!
// cppcheck-suppress unusedFunction
void WorkStealing::getTask(int &tid) {
#ifdef _OPENMP
#pragma omp single
#endif
  {
    tid = -1;      // task id
    int pws = 0;   // perform work stealing flag

    // --- GET TASK FROM ITS OWN LIST
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, lhW);
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ltW);
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ltpW);
    if (list_tail[0] >= list_head[0]) {
      tid = list_tp[list_head[0]];
      list_head[0]++;
    } else {
      pws = wsf;
    }
    MPI_Win_unlock(id, lhW);
    MPI_Win_unlock(id, ltW);
    MPI_Win_unlock(id, ltpW);
    // --- WORK STEALING
    if (pws) {
      int busy;   // flag
      std::cout << "id " << id << " - busy" << std::endl;
      do {
        // busy waiting
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, tokenW);
        busy = !(token[0]);
        MPI_Win_unlock(id, tokenW);
      } while (busy);
      std::cout << "id " << id << " - not busy" << std::endl;
      MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, tokenW);
      if (token[0] == 1) {   // token not set to finish (1)
        int stolen = 0;      // flag
        int vid;             // victim id
        do {
          vid = findVictim();
          if (vid != -1) {
            stolen = stealTasks(vid, tid);
          }
        } while ((vid != -1) && !stolen);
        if (vid == -1) {
          token[0] = -1;
          passToken();
        }
      } else {   // token == -1
        passToken();
      }
      MPI_Win_unlock(id, tokenW);
    }
  }
}

// cppcheck-suppress unusedFunction
void WorkStealing::checkToken() {
#ifdef _OPENMP
#pragma omp single
#endif
  {
    if (wsf) {
      MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, tokenW);
      if (token[0] == 1) {
        // compute number of tasks left
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ntpW);
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, lhW);
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ltW);
        n_task_p[id] = list_tail[0] - list_head[0] + 1;

        MPI_Win_unlock(id, lhW);
        MPI_Win_unlock(id, ltW);
        MPI_Win_unlock(id, ntpW);
        // update n_task_p at next rank

        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, next_id, 0, ntpW);
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ntpW);
        MPI_Put(n_task_p, n_process, MPI_INT, next_id, 0, n_process, MPI_INT, ntpW);
        MPI_Win_unlock(id, ntpW);
        MPI_Win_unlock(next_id, ntpW);

        passToken();
        token[0] = 0;
      } else if (token[0] == -1) {
        passToken();
        wsf = 0;
      }
      MPI_Win_unlock(id, tokenW);
    }
  }
}

int WorkStealing::findVictim() {
  int i;
  int victim = (id + 1) % n_process;   // victim id
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ntpW);
  for (i = 0; i < n_process; i++) {
    if ((i != id) && (n_task_p[i] > n_task_p[victim])) {
      victim = i;
    }
  }
  if (n_task_p[victim] < 1) {
    victim = -1;
  }
  MPI_Win_unlock(id, ntpW);

  return victim;
}

int WorkStealing::stealTasks(int victim, int &tid) {
  int stolen;   // flag
  int vlh, vlt;
  int *vltp = (int *) malloc(max_p_node * sizeof(int));

  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, victim, 0, lhW);
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, victim, 0, ltW);
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, victim, 0, ltpW);
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, lhW);
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ltW);
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ltpW);
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, id, 0, ntpW);

  MPI_Get(&vlh, 1, MPI_INT, victim, 0, 1, MPI_INT, lhW);
  MPI_Win_flush(victim, lhW);
  MPI_Get(&vlt, 1, MPI_INT, victim, 0, 1, MPI_INT, ltW);
  MPI_Win_flush(victim, ltW);

  if (vlh <= vlt) {
    MPI_Get(vltp, max_p_node, MPI_INT, victim, 0, max_p_node, MPI_INT, ltpW);
    MPI_Win_flush(victim, ltpW);
    int nts;   // nts: number of tasks to stole
    nts = ((int) ceil((vlt - vlh + 1) / 2.0));
    for (int i = 0; i < nts; i++) {
      list_tp[i] = vltp[vlh + i];
    }
    tid = list_tp[0];
    list_head[0] = 1;
    list_tail[0] = nts - 1;
    vlh += nts;
    n_task_p[victim] = vlt - vlh + 1;
    MPI_Put(&vlh, 1, MPI_INT, victim, 0, 1, MPI_INT, lhW);
    stolen = 1;
    std::cout << "STOLEN: id=" << id << " vid=" << victim << " nts=" << nts << " ltp=("
              << list_tp[0] << ".. " << list_tp[list_tail[0]] << ")" << std::endl;
  } else {
    std::cout << "NOT STOLEN: id=" << id << " vid=" << victim << std::endl;
    n_task_p[victim] = 0;
    stolen = 0;
  }

  MPI_Win_unlock(id, ntpW);
  MPI_Win_unlock(id, ltpW);
  MPI_Win_unlock(id, ltW);
  MPI_Win_unlock(id, lhW);
  MPI_Win_unlock(victim, ltpW);
  MPI_Win_unlock(victim, ltW);
  MPI_Win_unlock(victim, lhW);

  free(vltp);

  return stolen;
}

void WorkStealing::passToken() {
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, next_id, 0, tokenW);
  MPI_Put(token, 1, MPI_INT, next_id, 0, 1, MPI_INT, tokenW);
  MPI_Win_unlock(next_id, tokenW);
}

#ifdef VERBOSE
// cppcheck-suppress unusedFunction
void WorkStealing::printWindows() const {
  if (wsf && id) {
    int aux = list_tail[0] - list_head[0] + 1;
    std::cout << "----- Window info " << id << " out of " << n_process << std::endl;
    std::cout << "Token=" << token[0] << "\t";
    std::cout << "Max_p_Node=" << max_p_node << "\t";
    std::cout << "List head=" << list_head[0] << "\t";
    std::cout << "List tail=" << list_tail[0] << "\t";
    std::cout << "Next_id=" << next_id << "\t";
    switch (n_process) {
    case 1:
      std::cout << "n_task_p : " << n_task_p[0] << "\t";
      break;
    case 2:
      std::cout << "n_task_p : " << n_task_p[0] << " " << n_task_p[1] << "\t";
      break;
    case 3:
      std::cout << "n_task_p : " << n_task_p[0] << " " << n_task_p[1] << " " << n_task_p[2] << "\t";
      break;
    default:
      std::cout << "n_task_p : " << n_task_p[0] << " " << n_task_p[1] << "..."
                << n_task_p[n_process - 2] << n_task_p[n_process - 1] << "\t";
    }
    if (list_tail[0] >= list_head[0]) {
      switch (aux) {
      case 1:
        std::cout << "list_tp : " << list_tp[list_head[0]] << "\t";
        break;
      case 2:
        std::cout << "list_tp : " << list_tp[list_head[0]] << " " << list_tp[list_tail[0]] << "\t";
        break;
      case 3:
        std::cout << "list_tp : " << list_tp[list_head[0]] << " " << list_tp[list_tail[0] - 1]
                  << " " << list_tp[list_tail[0]] << "\t";
        break;
      default:
        std::cout << "list_tp : " << list_tp[list_head[0]] << " " << list_tp[list_head[0] + 1]
                  << "..." << list_tp[list_tail[0] - 1] << " " << list_tp[list_tail[0]] << "\t";
      }
    }
    std::cout << "" << std::endl;
  }
}
#endif
