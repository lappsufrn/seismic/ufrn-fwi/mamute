#include "SeismicData.h"

double fd;

SeismicData::SeismicData(int n, int ns, double dt) {
  // cppcheck-suppress noCopyConstructor
  // cppcheck-suppress noOperatorEq
  x = new int[n];
  if (x == NULL)
    std::cout << "Memory not allocated for SeismicData.x " << std::endl;
  y = new int[n];
  if (y == NULL)
    std::cout << "Memory not allocated for SeismicDataData.y " << std::endl;
  z = new int[n];
  if (z == NULL)
    std::cout << "Memory not allocated for SeismicDataData.z " << std::endl;
  n_trace = n;
  n_samples = ns;
  this->dt = dt;
  nf = n_trace * n_samples;
  data = new double[nf];
  if (data == NULL)
    std::cout << "Memory not allocated for seisData.data " << std::endl;
};

SeismicData::~SeismicData() {
  delete[] y;
  delete[] x;
  delete[] z;
  delete[] data;
}

//
// Subtract a SeismicData sdb.
//
// cppcheck-suppress unusedFunction
void SeismicData::subData(SeismicData const &sdb) {
  int ni, nsi, ind;
#ifdef _OPENMP
#pragma omp for schedule(auto) collapse(2)
#endif
  for (ni = 0; ni < n_trace; ni++) {
    for (nsi = 0; nsi < n_samples; nsi++) {
      ind = ni * n_samples + nsi;
      data[ind] -= sdb.data[ind];
    }
  }
}

#ifdef VERBOSE
// cppcheck-suppress unusedFunction
void SeismicData::Print(std::string const &s, int id) const {
  if (!id) {
    std::cout << "***********************" << std::endl;
    std::cout << "Printing " << s << std::endl;
    std::cout << "n:" << n_trace << "  ";
    std::cout << "ns:" << n_samples << "  ";
    std::cout << "dt:" << dt << "  ";
    std::cout << "nf:" << nf << std::endl;
    switch (n_trace) {
    case 1:
      std::cout << "x:" << x[0] << "   y:" << y[0] << "   z:" << z[0] << std::endl;
      break;
    case 2:
      std::cout << "x:" << x[0] << x[1] << " y:" << y[0] << y[1] << "  z:" << z[0] << z[1]
                << std::endl;
      break;
    case 3:
      std::cout << "x:" << x[0] << "|" << x[1] << "|" << x[2] << " ";
      std::cout << "y:" << y[0] << "|" << y[1] << "|" << y[2] << " ";
      std::cout << "z:" << z[0] << "|" << z[1] << "|" << z[2] << std::endl;
      break;
    default:
      std::cout << "x:" << x[0] << "|" << x[1] << "..." << x[n_trace - 2] << "|" << x[n_trace - 1]
                << " ";
      std::cout << "y:" << y[0] << "|" << y[1] << "..." << y[n_trace - 2] << "|" << y[n_trace - 1]
                << " ";
      std::cout << "z:" << z[0] << "|" << z[1] << "..." << z[n_trace - 2] << "|" << z[n_trace - 1]
                << std::endl;
    }
    std::cout << "***********************" << std::endl;
  }
}
#endif

/**
 *  Read seismic source signature from a file proj_dir/source.bin
 */
void SeismicData::ReadSourceFromFile(std::string projDir) {
  BinaryFileHandler fileHandler(projDir);
  this->data = fileHandler.ReadFile<double>(FILENAME_SOURCE_DATA);
}

// Read receivers coordinates for shot ishot from file
// proj_dir/rcv_coord_ishot.bin
void SeismicData::setPositionFromFile(const std::string &projDir, double ox, double oy, double oz,
                                      double dx, double dy, double dz, int ishot, int n_rcv) {
  double rcv_coord[n_rcv][3];
  double *coords1D = nullptr;
  BinaryFileHandler fileHandler(projDir);

  n_trace = n_rcv;

  coords1D = fileHandler.ReadFile<double>(FILENAME_RECEIVER_COORDINATES, ishot);

  // Convert from 1D to 2D.
  for (int i = 0; i < n_rcv; i++) {
    for (int j = 0; j < 3; j++) {
      rcv_coord[i][j] = coords1D[i * 3 + j];
    }
  }

  // Free memory
  if (coords1D != nullptr) {
    delete[] coords1D;
  }

  for (int i = 0; i < n_trace; i++) {
    this->x[i] = int((rcv_coord[i][0] - ox) / dx);
    this->y[i] = int((rcv_coord[i][1] - oy) / dy);
    this->z[i] = int((rcv_coord[i][2] - oz) / dz);
  }
}

// cppcheck-suppress unusedFunction
double SeismicData::misfit() const {
  int ind;
#ifdef _OPENMP
#pragma omp single
#endif
  fd = 0.0;
#ifdef _OPENMP
#pragma omp for reduction(+ : fd)
#endif
  for (ind = 0; ind < nf; ind++) {
    fd += (data[ind] * data[ind]);
  }
  return 0.5 * fd;
}

void SeismicData::Resize(int p_ns, double p_dt) {
  if (p_ns == n_samples)
    return;
#ifdef _OPENMP
#pragma omp single
#endif
  try {
    delete[] data;
    data = new double[n_trace * p_ns];
    n_samples = p_ns;
    dt = p_dt;
    nf = n_trace * n_samples;
  } catch (const std::exception &e) {
    std::string s("SeismicData::Resize failed: ");
    throw s + e.what();
  }
}
