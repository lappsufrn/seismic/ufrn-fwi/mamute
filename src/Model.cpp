#include "Model.h"
#include "utilities/BinaryFileHandler.h"

Model::Model(const std::string &proj_dir, const std::string &model_filename, Grid &grid)
    : fgrid(grid) {
  try {
    fExpandedModel = new double[fgrid.n];
    fOriginalModel = new double[fgrid.on];
  } catch (const std::exception &e) {
    std::string s("Model::Model allocation failed: ");
    throw s + e.what();
  }
}

void Model::SetModel(const std::string &proj_dir, const std::string &model_filename) {
  BinaryFileHandler fileHandler(proj_dir);
  this->fOriginalModel = fileHandler.ReadFile<double>(model_filename);
}

void Model::ExpandModel() {
  int iy, ix, iz;
  double aux;
  // copies model
  for (ix = fgrid.start_x; ix < fgrid.end_x; ix++) {
    for (iy = fgrid.start_y; iy < fgrid.end_y; iy++) {
      for (iz = fgrid.start_z; iz < fgrid.end_z; iz++) {
        aux = fOriginalModel[((ix - fgrid.border) * fgrid.ony + (iy - fgrid.border)) * fgrid.onz +
                             (iz - fgrid.border)];
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] = aux;
      }
    }
  }
  // expands to -y
  for (ix = fgrid.border; ix < fgrid.end_x; ix++) {
    for (iy = 0; iy < fgrid.border; iy++) {
      for (iz = fgrid.border; iz < fgrid.end_z; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(ix * fgrid.ny + fgrid.border) * fgrid.nz + iz];
      }
    }
  }
  // expands to +y
  for (ix = fgrid.border; ix < fgrid.end_x; ix++) {
    for (iy = fgrid.end_y; iy < fgrid.ny; iy++) {
      for (iz = fgrid.border; iz < fgrid.end_z; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(ix * fgrid.ny + (fgrid.end_y - 1)) * fgrid.nz + iz];
      }
    }
  }
  // expands to -x
  for (ix = 0; ix < fgrid.border; ix++) {
    for (iy = 0; iy < fgrid.ny; iy++) {
      for (iz = fgrid.border; iz < fgrid.end_z; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(fgrid.border * fgrid.ny + iy) * fgrid.nz + iz];
      }
    }
  }
  // expands to +x
  for (ix = fgrid.end_x; ix < fgrid.nx; ix++) {
    for (iy = 0; iy < fgrid.ny; iy++) {
      for (iz = fgrid.border; iz < fgrid.end_z; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[((fgrid.end_x - 1) * fgrid.ny + iy) * fgrid.nz + iz];
      }
    }
  }
  // expands to -z
  for (ix = 0; ix < fgrid.nx; ix++) {
    for (iy = 0; iy < fgrid.ny; iy++) {
      for (iz = 0; iz < fgrid.border; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + fgrid.border];
      }
    }
  }
  // expands to +z
  for (ix = 0; ix < fgrid.nx; ix++) {
    for (iy = 0; iy < fgrid.ny; iy++) {
      for (iz = fgrid.end_z; iz < fgrid.nz; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + fgrid.end_z - 1];
      }
    }
  }
}

Model::~Model() {
  delete[] fExpandedModel;
  delete[] fOriginalModel;
}