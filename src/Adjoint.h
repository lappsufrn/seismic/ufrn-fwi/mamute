#ifndef ADJOINT_H
#define ADJOINT_H

#include "Checkpoint.h"
#include "Config.h"
#include "DensityModel.h"
#include "Modeling.h"
#include "VelocityModel.h"
#include "Wavefield.h"

#ifdef MAMUTE_AUTOTUNING
#include "Autotuning.hpp"
#endif

#ifdef FT
#include "DeLIA.h"
#include <set>
#endif

#define FILENAME_GRADIENT "gradient.bin"

#ifdef FT
#define OMP_SHARED_DELIA shared(gradient_parcial, std::cerr, id)
#else
#define OMP_SHARED_DELIA
#endif

namespace FWI {
class Checkpoint;
}

class Adjoint {
private:
  std::string proj_dir;                ///< project directory
  double fpeak;                        ///< frequency of peak
  int n_rcv;                           ///< number of receivers for current shot
  int max_rcv;                         ///< maximum number of receivers per shot
  int *rcv_per_shot = nullptr;         /// number of receivers per shot
  int n_src;                           ///< number of sources
  double ox;                           ///< origin of coordinate x
  double oy;                           ///< origin of coordinate y
  double oz;                           ///< origin of coordinate z
  double dx;                           ///< discretization of coordinate x
  double dy;                           ///< discretization of coordinate y
  double dz;                           ///< discretization of coordinate z
  int gradient_preconditioning_mode;   ///< case 1: BesselSmoothing3D, case 2: LaplaceSmoothing3D
  double Lx;                           ///< gradient preconditioning bessel filter lx
  double Ly;                           ///< gradient preconditioning bessel filter ly
  double Lz;                           ///< gradient preconditioning bessel filter lz
  int nplanes = 0;                     ///< number of planes near the surface to set gradient as 0
  short int WS_FLAG;
#ifdef MAMUTE_AUTOTUNING
  Autotuning *at;
#endif
#ifdef MAMUTE_WF_MGMT_CHK
  bool use_default_check_mem;
  double check_mem = 0.8;
  int chk_verb = 0;
#endif

  void MultiplyAdjoint();
  void imgCond(double *grad, double *fwdwf) const;
  void GradientPreconditioning();
  void ZeroesNearSurface(double *gradient);
  Adjoint(const Adjoint &adj) = delete;

public:
  VelocityModel *model = nullptr;
  Wavefield *wf = nullptr;
  DensityModel *density = nullptr;
  Modeling *modl = nullptr;
  double *gradient = nullptr;
  double misfit;
  double t_misfit;
#ifdef FT
  std::set<int> shots_processed;
  double misfit_parcial;
  double *gradient_parcial = nullptr;
#endif

public:
  Adjoint(VelocityModel *m, const std::string &proj_dir, int n_src, double fpeak, int ws_flag,
          double ox, double oy, double oz, double dx, double dy, double dz, int ns, double dt,
          int gradient_preconditioning_mode, double Lx = -1.0, double Ly = -1.0, double Lz = -1.0,
          int zeroes_nplanes_gradient = 0, bool use_default_check_mem = true,
          double check_mem = 0.8, int chk_verb = 0, DensityModel *density = nullptr);
  ~Adjoint();

  void Run();
  static void StaticRun(double *gradient, double *total_misfit, const std::string &proj_dir,
                        const std::string &velocity_filename, int n_src, double fpeak, int ws_flag,
                        double ox, double oy, double oz, double dx, double dy, double dz, int nx,
                        int ny, int nz, int border, int stencil, int ns, double dt,
                        int gradient_preconditioning_mode, double Lx = -1.0, double Ly = -1.0,
                        double Lz = -1.0, int zeroes_nplanes_gradient = 0,
                        bool use_default_check_mem = true, double check_mem = 0.8, int chk_verb = 0,
                        const std::string &density_filename = "");
  void readShotFromFiles(int ishot, SeismicData *src, SL *src_pos, SeismicData *dobs);
  ///< set parameters
  void setProjDir(const std::string &projdir) { proj_dir = projdir; }
  void setFreq(double fpeak) { this->fpeak = fpeak; }
  void setNumberSrc(int nsrc) { n_src = nsrc; }
  void setNumberRcv(int nrcv) { n_rcv = nrcv; }
  void setOrigins(double _ox, double _oy, double _oz) {
    ox = _ox;
    oy = _oy;
    oz = _oz;
  }
  void setDiscretization(double _dx, double _dy, double _dz) {
    dx = _dx;
    dy = _dy;
    dz = _dz;
  }
  void setGradientPreconditioningMode(int mode) { gradient_preconditioning_mode = mode; }
  void setGradientPreconditioningFilters(double _Lx, double _Ly, double _Lz) {
    Lx = _Lx;
    Ly = _Ly;
    Lz = _Lz;
  }
  void setNplanes(int _nplanes) { nplanes = _nplanes; }
  double getFpeak() const { return fpeak; }
  void setFlagWS(short int flag) { WS_FLAG = flag; }
};

#endif   // ADJOINT_H
