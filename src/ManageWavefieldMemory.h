#ifndef MAMAGE_WAVEFIELD_MEMORY_H
#define MAMAGE_WAVEFIELD_MEMORY_H

#include "ManageWavefield.h"
#include "utilities/BinaryFileHandler.h"

class ManageWavefieldMemory : public ManageWavefield {
private:
  double *fForwardWavefield = nullptr;

public:
  ManageWavefieldMemory(Wavefield &wf, const Grid &grid, int Ti);
  ManageWavefieldMemory(const ManageWavefieldMemory &manageWavefieldMemory) = delete;
  ManageWavefieldMemory &operator=(const ManageWavefieldMemory &manageWavefieldMemory) = delete;
  void SaveWavefield(int ti, int ishot) override;
  void RetrieveWavefield(double *wf, int ti, int ishot) override;
  void EndShot(int ishot) override;
  ~ManageWavefieldMemory();
};

#endif
