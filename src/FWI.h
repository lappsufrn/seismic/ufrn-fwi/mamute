#ifndef FWI_H
#define FWI_H

#include "Adjoint.h"
#include "Config.h"
#include "DensityModel.h"
#include "FaultTolerance.h"
#include "Grid.h"
#include "Optimizer.h"
#include "SimpleConfigTextFile.h"
#include "Utils.h"
#include "VelocityModel.h"

#ifdef FT
#include "DeLIA.h"
#include <vector>
#endif

namespace FWI {
class FWI {
private:
  Config *fConfig = nullptr;
  Optimizer *fOptimizer = nullptr;
  Adjoint *fAdjoint = nullptr;
  VelocityModel *fVelocityModel = nullptr;
  DensityModel *fDensity = nullptr;
  Grid *fgrid = nullptr;
  FWI(const FWI &fwi) = delete;
  FWI &operator=(const FWI &fwi) = delete;

public:
  explicit FWI(Config *config);

  ~FWI() {
    delete fAdjoint;
    delete fVelocityModel;
    delete fOptimizer;
    delete fgrid;
#ifdef MAMUTE_DENSITY
    delete fDensity;
#endif
  }

  void Run();
};

};   // namespace FWI

#endif   // FWI_H
