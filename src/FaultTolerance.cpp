#ifdef FT
#include "FaultTolerance.h"

//!
//! FaultTolerance initialization:
//! Checks whether the last saved data matches the current configuration,
//! otherwise it saves the current data configuration
//!
FaultTolerance::FaultTolerance(Config *config, VelocityModel *model, Optimizer *optimizer,
                               Adjoint *adjoint) {
  std::string ft_params;
  std::string fwi_params;
  try {
    // Fault-Tolerance -> path to json input
    ft_params = config->Get("ft_config");
    fwi_params = config->Get("fwi_config");
    int id, comm_sz;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    DeLIA_Init(id, comm_sz, ft_params, fwi_params);

    if (!DeLIA_CanWork()) {
      std::cout << "WITHOUT FAULT TOLERANCE" << std::endl;
      std::cout << "FAULT TOLERANCE CONFIGURATION INCORRECT" << std::endl;
      return;
    }
    DeLIA_SetGlobalData(adjoint->gradient, model->fgrid.on, DOUBLE_CODE);
    DeLIA_SetGlobalData(&(adjoint->t_misfit), 1, DOUBLE_CODE);

    DeLIA_SetGlobalData(model->fOriginalModel, model->getSize(), DOUBLE_CODE);

    DeLIA_SetGlobalData(&(optimizer->fConverged), 1, BOOL_CODE);
    DeLIA_SetGlobalData(optimizer->wa, optimizer->nwa, DOUBLE_CODE);
    DeLIA_SetGlobalData(optimizer->dsave, 29, DOUBLE_CODE);
    DeLIA_SetGlobalData(optimizer->iwa, optimizer->nmax * 3, LONG_INT_CODE);
    DeLIA_SetGlobalData(optimizer->task, 1, LONG_INT_CODE);
    DeLIA_SetGlobalData(optimizer->csave, 1, LONG_INT_CODE);
    DeLIA_SetGlobalData(optimizer->isave, 44, LONG_INT_CODE);
    DeLIA_SetGlobalData(optimizer->lsave, 4, LONG_INT_CODE);

    DeLIA_SetLocalData(adjoint->gradient_parcial, model->fgrid.on, DOUBLE_CODE);
    DeLIA_SetLocalData(&(adjoint->misfit_parcial), 1, DOUBLE_CODE);

    DeLIA_SetTasksData(&(adjoint->shots_processed));

    bool temp = DeLIA_CanRecoverGlobalCheckpointing();
    MPI_Barrier(MPI_COMM_WORLD);
    if (temp) {
      DeLIA_ReadGlobalData();
    } else {
      MPI_Barrier(MPI_COMM_WORLD);
      DeLIA_SaveSettings();
    }
    return;

  } catch (const std::exception &e) {
    std::cout << "WITHOUT DELIA" << std::endl;
    return;
  }
}
#endif