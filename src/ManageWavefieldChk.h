#ifndef MAMAGE_WAVEFIELD_CHK_H
#define MAMAGE_WAVEFIELD_CHSK_H

#include "Checkpoint.h"
#include "Config.h"
#include "ManageWavefield.h"

class ManageWavefieldChk : public ManageWavefield {
private:
  FWI::Checkpoint *fwicheck;
  Adjoint &fAdj;
  WorkStealing &fWs;
  SeismicData &fSrc;
  Wavefield *fCheck_wf;
  int fNs;
  ManageWavefieldChk(const ManageWavefieldChk &manageWavefieldChk) = delete;
  ManageWavefieldChk &operator=(const ManageWavefieldChk &manageWavefieldChk) = delete;

public:
  ManageWavefieldChk(Wavefield &wf, const Grid &grid, Adjoint &adj, WorkStealing &ws,
                     SeismicData &src, int ns, bool use_default_check_mem, double custom_check_mem);
  void SaveWavefield(int ti, int ishot) override;
  void RetrieveWavefield(double *wf, int ti, int ishot) override;
  void EndShot(int ishot) override;
  void BackwardSetup();
  void Print(int n, int id, int rep, int chk_verb);
  ~ManageWavefieldChk();
};

#endif
