#include "Wavefield.h"
#include "utilities/BinaryFileHandler.h"

/**
 * Contructor of class  Wavefield
 * Allocation and Inicialization.
 */
Wavefield::Wavefield(int n) {
  // cppcheck-suppress noCopyConstructor
  // cppcheck-suppress noOperatorEq
  p_new = new double[n];
  if (p_new == NULL)
    std::cout << "Memory not allocated for wavefield.p_new " << std::endl;
  current = new double[n];
  if (current == NULL)
    std::cout << "Memory not allocated for wavefield.current" << std::endl;
  Reset(n);
}

void Wavefield::Reset(int n) {
  int ind;
#ifdef _OPENMP
#pragma omp for schedule(auto)
#endif
  for (ind = 0; ind < n; ind++) {
    p_new[ind] = 0;
    current[ind] = 0;
  }
}

void Wavefield::Save(const std::string &projDir, const Grid &grid, int ti, int ishot) const {
#ifdef _OPENMP
#pragma omp single
#endif
  {
    BinaryFileHandler fileHandler(projDir);
    std::string waveFieldFilePath = fileHandler.GetFilePath(FILENAME_WAVEFIELD_SHOT, ishot);
    int xi, yi, flag = 0;
    FILE *wff = fopen(waveFieldFilePath.c_str(), "ab");
    unsigned int z = grid.onz;
    for (xi = grid.start_x; xi < grid.end_x; xi++) {
      for (yi = grid.start_y; yi < grid.end_y; yi++) {
        if (fwrite(&(current[(xi * grid.ny + yi) * grid.nz + grid.border]), sizeof(double),
                   grid.onz, wff) != z) {
          flag = 1;
        }
      }
    }
    fclose(wff);
    if (flag) {
      std::cout << "Failed writing: " << waveFieldFilePath << " ti = " << ti << " shot = " << ishot
                << std::endl;
    }
  }
}
/**
 * Get the Forward wavefield from DISK
 * Output: fwdwf
 */
void Wavefield::getFwd(double *fwdwf, const std::string &projDir, const Grid &grid, int ti,
                       int ishot) {
#ifdef _OPENMP
#pragma omp single
#endif
  {

    BinaryFileHandler fileHandler(projDir);
    std::string waveFieldFilePath = fileHandler.GetFilePath(FILENAME_WAVEFIELD_SHOT, ishot);

    FILE *wff = fopen(waveFieldFilePath.c_str(), "rb");
    if (wff == NULL) {
      std::cout << "Failed opening" << waveFieldFilePath << "(fopen) ti=" << ti << " shot=" << ishot
                << std::endl;
    } else {
      long int shift = ((long int) ti) * grid.on * sizeof(double);
      // cppcheck-suppress ConfigurationNotChecked
      if (fseek(wff, shift, SEEK_SET) != 0) {
        std::cout << "Failed reading " << waveFieldFilePath << " (fseek) ti=" << ti
                  << " shot=" << ishot << " shift=" << shift << std::endl;
      } else {
        if (fread(fwdwf, sizeof(double), grid.on, wff) != (unsigned) grid.on) {
          std::cout << "Failed reading " << waveFieldFilePath << " (fread) ti=" << ti
                    << " shot=" << ishot << std::endl;
        }
      }
      fclose(wff);
    }
  }
}
