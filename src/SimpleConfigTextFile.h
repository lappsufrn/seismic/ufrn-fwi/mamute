#ifndef SIMPLECONFIGTEXTFILE_H
#define SIMPLECONFIGTEXTFILE_H

#include "Config.h"
#include <map>
#include <type_traits>

class SimpleConfigTextFile : public ConfigHelper<SimpleConfigTextFile> {
public:
  SimpleConfigTextFile(int argc, char *argv[]) : ConfigHelper<SimpleConfigTextFile>(argc, argv) {
    parser();
  }
  ~SimpleConfigTextFile() {
    // FIXME: there is still some mem leak inside fFlags
    fFlags.clear();
  }

  template <typename T>
  typename std::enable_if<std::is_integral<T>::value, T>::type Get(const std::string &key) {
    return std::stoi(find(key));
  }

  template <typename T>
  typename std::enable_if<std::is_floating_point<T>::value, T>::type Get(const std::string &key) {
    return std::stof(find(key));
  }

  // cppcheck-suppress unusedFunction
  const std::string Get(const char *key) override { return find(key); }

  bool exist(const std::string &key) {
    auto pair = fFlags.find(key);
    return (pair != fFlags.end());
  }

private:
  std::map<std::string, std::string> fFlags = {};
  void parser(void);

  const std::string find(const std::string &key) {
    auto pair = fFlags.find(key);
    if (pair == fFlags.end()) {
      throw std::runtime_error("Key \'" + key + "\' not found at \'" + std::string(GetFileName()) +
                               "\'.");
    }
    return pair->second;
  }
};

REGISTER_CONFIG("txt", SimpleConfigTextFile)

#endif   // SIMPLECONFIGTEXTFILE_H
