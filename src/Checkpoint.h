#ifndef CHECKPOINT_H
#define CHECKPOINT_H
#include "Adjoint.h"
#include "SeismicData.h"
#include "Wavefield.h"
#include "WorkStealing.h"
#include <cstring>
#include <vector>

class Adjoint;
namespace FWI {
class Checkpoint {
private:
  int snaps;
  int *chk = nullptr;       // int *chk;
  int *ord_chk = nullptr;   // int *i_buffers;
  static double MEM_PERCENT;
  static int getNBuffers(int n, int ns);
  static void getWavefieldWithoutBorders(double *out, double *in, const Grid &grid);

public:
  int n_check;     // number of checkpoints
  int n_buffers;   // number of buffers
  int current;     // current buffer
  int *ib_to_ck = nullptr;
  std::vector<Wavefield *> buffer;

public:
  explicit Checkpoint(int s, int gridN, double mem_percent = MEM_PERCENT);
  void resetCheck();
  void saveCheck(Wavefield *wf, int n, int ti);
  void UpdateBuffers(Adjoint *adj, WorkStealing *ws, Wavefield *cwf, SeismicData *src, int n);
  void print(int n, int verb);
  void getFwdChk(Adjoint *adj, WorkStealing *ws, double *fwdwf, Wavefield *cwf, int ti,
                 const Grid &grid, SeismicData *src);
  void bwdChkSetup();

  ~Checkpoint() {
    for (u_long i = 0; i < buffer.size(); i++) {
      delete buffer[i];
    }
    buffer.clear();
    if (chk != nullptr)
      delete[] chk;
    if (ord_chk != nullptr)
      delete[] ord_chk;
    delete[] ib_to_ck;
  };
};
};   // namespace FWI

#endif   // CHECKPOINT_H
