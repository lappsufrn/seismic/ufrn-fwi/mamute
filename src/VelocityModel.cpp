#include "VelocityModel.h"
#include "utilities/BinaryFileHandler.h"
#include <cmath>

/* Constructors */

VelocityModel::VelocityModel(const std::string &proj_dir, const std::string &velocity_filename,
                             Grid &grid)
    : Model(proj_dir, velocity_filename, grid) {
  // cppcheck-suppress noOperatorEq
  SetModel(proj_dir, velocity_filename);
  FindMaxMin();
}

// cppcheck-suppress unusedFunction
void VelocityModel::Print() const {
  std::cout << "------------Print Model Information-------------" << std::endl;
  std::cout << "n:" << fgrid.n << "  \t";
  std::cout << "nx + border:" << fgrid.nx << " \t";
  std::cout << "ny + border:" << fgrid.ny << " \t";
  std::cout << "nz + border:" << fgrid.nz << " \n";
  std::cout << "border.size:" << fgrid.border << "\t";
  std::cout << "border.x:" << fgrid.end_x << "\t";
  std::cout << "border.y:" << fgrid.end_y << "\t";
  std::cout << "border.z:" << fgrid.end_z << "\n";
  std::cout << "nx:" << fgrid.onx << " \t";
  std::cout << "ny:" << fgrid.ony << " \t";
  std::cout << "nz:" << fgrid.onz << " \t";
  std::cout << "n original:" << fgrid.on << " \n";
  std::cout << "dx:" << fgrid.dx << " ";
  std::cout << "dy:" << fgrid.dy << " ";
  std::cout << "dz:" << fgrid.dz << " \t";
  std::cout << "fMin:" << fMin << " ";
  std::cout << "fMax:" << fMax << " ";
  std::cout << "\n-------------------------" << std::endl;
}

void VelocityModel::ExpandModel(double dt) {
  int iy, ix, iz;
  double aux, dt2 = dt * dt;
  // copies and optimize model
  for (ix = fgrid.start_x; ix < fgrid.end_x; ix++) {
    for (iy = fgrid.start_y; iy < fgrid.end_y; iy++) {
      for (iz = fgrid.start_z; iz < fgrid.end_z; iz++) {
        aux = fOriginalModel[((ix - fgrid.border) * fgrid.ony + (iy - fgrid.border)) * fgrid.onz +
                             (iz - fgrid.border)];
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] = dt2 * aux * aux;
      }
    }
  }
  // expands to -y
  for (ix = fgrid.border; ix < fgrid.end_x; ix++) {
    for (iy = 0; iy < fgrid.border; iy++) {
      for (iz = fgrid.border; iz < fgrid.end_z; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(ix * fgrid.ny + fgrid.border) * fgrid.nz + iz];
      }
    }
  }
  // expands to +y
  for (ix = fgrid.border; ix < fgrid.end_x; ix++) {
    for (iy = fgrid.end_y; iy < fgrid.ny; iy++) {
      for (iz = fgrid.border; iz < fgrid.end_z; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(ix * fgrid.ny + (fgrid.end_y - 1)) * fgrid.nz + iz];
      }
    }
  }
  // expands to -x
  for (ix = 0; ix < fgrid.border; ix++) {
    for (iy = 0; iy < fgrid.ny; iy++) {
      for (iz = fgrid.border; iz < fgrid.end_z; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(fgrid.border * fgrid.ny + iy) * fgrid.nz + iz];
      }
    }
  }
  // expands to +x
  for (ix = fgrid.end_x; ix < fgrid.nx; ix++) {
    for (iy = 0; iy < fgrid.ny; iy++) {
      for (iz = fgrid.border; iz < fgrid.end_z; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[((fgrid.end_x - 1) * fgrid.ny + iy) * fgrid.nz + iz];
      }
    }
  }
  // expands to -z
  for (ix = 0; ix < fgrid.nx; ix++) {
    for (iy = 0; iy < fgrid.ny; iy++) {
      for (iz = 0; iz < fgrid.border; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + fgrid.border];
      }
    }
  }
  // expands to +z
  for (ix = 0; ix < fgrid.nx; ix++) {
    for (iy = 0; iy < fgrid.ny; iy++) {
      for (iz = fgrid.end_z; iz < fgrid.nz; iz++) {
        fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + iz] =
            fExpandedModel[(ix * fgrid.ny + iy) * fgrid.nz + fgrid.end_z - 1];
      }
    }
  }
}
//!
//! Find maximum and minimum velocities
//!
void VelocityModel::FindMaxMin() {
  int nf = fgrid.onx * fgrid.ony * fgrid.onz;
  int ind;
  fMin = fOriginalModel[0];
  fMax = fOriginalModel[0];
  for (ind = 1; ind < nf; ind++) {
    if (fOriginalModel[ind] < fMin) {
      fMin = fOriginalModel[ind];
    } else if (fOriginalModel[ind] > fMax) {
      fMax = fOriginalModel[ind];
    }
  }
}

#ifdef VERBOSE
void VelocityModel::SaveVModelIter(int iter_number, std::string projectPath) {

  BinaryFileHandler fileHandler = BinaryFileHandler(projectPath);

  fileHandler.WriteFile<double>(FILENAME_VELOCITY_ITER, iter_number, fOriginalModel, fgrid.on);
}
#endif

void VelocityModel::SaveVModelFinal(std::string projectPath) {
  BinaryFileHandler fileHandler(projectPath);
  fileHandler.WriteFile<double>(FILENAME_VELOCITY_FINAL, fOriginalModel, fgrid.on);
}