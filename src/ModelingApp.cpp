#include "Modeling.h"
#include "VelocityModel.h"
#include "WorkStealing.h"
#include "utilities/BinaryFileHandler.h"
#include <dirent.h>
#include <fstream>
#include <iostream>
#ifdef MAMUTE_AUTOTUNING
#include "Autotuning.hpp"
#endif
#ifdef MAMUTE_DENSITY
#include "DensityModel.h"
#endif

#ifdef MAMUTE_AUTOTUNING
#define OMP_SHARED_PATSMA shared(patsma)
#else
#define OMP_SHARED_PATSMA
#endif

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " <config_file>" << std::endl;
    return 1;
  }

  try {

    // Read configuration file
    Config *config = Config::ReadConfigFile(argc, argv);
    // cppcheck-suppress variableScope
    int provided, id, comm_sz;

    MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

    SimpleConfigTextFile *sconfig = static_cast<SimpleConfigTextFile *>(config);
    int nsrc = sconfig->Get<double>("n_src");
    double fpeak = sconfig->Get<double>("fpeak");
    double dt = sconfig->Get<double>("dt");
    std::string proj_dir = config->Get("proj_dir");
    int max_rcv;
    int *rcv_per_shot = nullptr;   // number of receivers
    int ns = sconfig->Get<double>("ns");
    short int WS_FLAG = sconfig->Get<int>("ws_flag");
    int ishot;

// Print information about execution parameters.
#ifdef VERBOSE
    if (!id) {
      std::cout << "---------------- 3D MODELING ---------------" << std::endl;
      if (WS_FLAG) {
        std::cout << "--- WORK STEALING ----" << std::endl;
      } else {
        std::cout << "--- WITHOUT WORK STEALING ----" << std::endl;
      }
      std::cout << "-------------------------------------------" << std::endl;
    }
#endif

    // Compute number of receivers from files
    try {
      // cppcheck-suppress noOperatorEq
      rcv_per_shot = new int[nsrc];
    } catch (const std::exception &e) {
      std::string s("rcv_per_shot allocation failed: ");
      throw s + e.what();
    }
    max_rcv = 0;
    BinaryFileHandler fileHandler = BinaryFileHandler(proj_dir);

    for (int i = 0; i < nsrc; i++) {
      long fileSize = fileHandler.CountData<double>(FILENAME_RECEIVER_COORDINATES, i);
      // Each receiver's coordinate has 3 components X, Y, and Z.
      rcv_per_shot[i] = fileSize / 3;
      if (rcv_per_shot[i] > max_rcv) {
        max_rcv = rcv_per_shot[i];
      }
    }
    double ox, oy, oz, dx, dy, dz;
    int nx, ny, nz, border, stencil;
    ox = sconfig->Get<double>("ox");
    oy = sconfig->Get<double>("oy");
    oz = sconfig->Get<double>("oz");
    dx = sconfig->Get<double>("dx");
    dy = sconfig->Get<double>("dy");
    dz = sconfig->Get<double>("dz");
    nx = sconfig->Get<int>("nx");
    ny = sconfig->Get<int>("ny");
    nz = sconfig->Get<int>("nz");
    border = sconfig->Get<int>("border");
    stencil = sconfig->Get<int>("stencil");
    checkInputData(FILENAME_SOURCE_COORDINATES, FILENAME_RECEIVER_COORDINATES, proj_dir,
                   rcv_per_shot, ox, oy, oz, dx, dy, dz, nx, ny, nz, nsrc);

    Grid *grid = new Grid(stencil, dx, dy, dz, border, nx, ny, nz);
    Wavefield *wf = new Wavefield(grid->n);
    VelocityModel *model = new VelocityModel(proj_dir, config->Get("vel"), *grid);
    DensityModel *density = nullptr;
#ifdef MAMUTE_DENSITY
    density = new DensityModel(proj_dir, config->Get("density"), *grid);
#endif
    Modeling *modeling = new Modeling(*grid, *model, ns, dt, fpeak, *density);

    SeismicData *src = new SeismicData(1, ns, dt);
    SL *location = new SL(nsrc);
    location->setPositionFromFile(FILENAME_SOURCE_COORDINATES, proj_dir, ox, oy, oz, dx, dy, dz,
                                  nsrc);
    src->x[0] = location->x[0];
    src->y[0] = location->y[0];
    src->z[0] = location->z[0];
    src->ReadSourceFromFile(proj_dir);

    if (!modeling->ValidConditions(fpeak, id)) {
      throw std::runtime_error("Check CFL (Courant–Friedrichs–Lewy) criteria!");
    }
#ifdef MAMUTE_SPHERE
    modeling->Sphere(*src, *model);
#endif
    modeling->Interpolate(src);
    model->ExpandModel(modeling->GetDt());
#ifdef MAMUTE_DAMPING
    model->fgrid.ComputeDampingCoefficients(modeling->fG1, modeling->fG2, modeling->GetDt(), fpeak,
                                            id);
#endif
#ifdef VERBOSE
    if (!id) {
      model->Print();
    }
#endif

    WorkStealing *ws = new WorkStealing(nsrc, comm_sz, id, WS_FLAG);
#ifdef VERBOSE
    ws->printWindows();
#endif

    SeismicData *dobs = new SeismicData(max_rcv, ns, dt);

    // Delete output file if it exists.
    if (id == 0) {
      for (ishot = 0; ishot < nsrc; ishot++) {
        fileHandler.DeleteFile(FILENAME_OBSERVED_DATA, ishot);
      }
    }
    ishot = -1;

#ifdef MAMUTE_AUTOTUNING
    int n_threads = omp_get_max_threads();
    Autotuning *patsma = new Autotuning(1, 1, model->fgrid.n / (n_threads * 2), 1, 4, 100);
#endif

#ifdef _OPENMP
#pragma omp parallel default(none)                                                                 \
    shared(src, wf, nsrc, modeling, model, grid, location, dobs, comm_sz, id, std::cout, config,   \
               rcv_per_shot, sconfig, ishot, ws, proj_dir, ox, oy, oz, dx, dy, dz)                 \
    OMP_SHARED_PATSMA
#endif
    {
#ifdef MAMUTE_AUTOTUNING
      wStart(patsma, &(modeling->omp_chunk));
      modeling->ModelingStep(*src, *wf, 0, 0);
      wEnd(patsma);
#endif
      ws->getTask(ishot);
      while (ishot != -1) {
        wf->Reset(grid->n);
#ifdef MAMUTE_CPML
        modeling->fCPML->Reset();
#endif
#ifdef _OPENMP
#pragma omp single
#endif
        {
          std::cout << "MODELING SHOT: " << ishot << " PROCESS: " << id << std::endl;
          src->x[0] = location->x[ishot];
          src->y[0] = location->y[ishot];
          src->z[0] = location->z[ishot];
          dobs->setPositionFromFile(proj_dir, ox, oy, oz, dx, dy, dz, ishot, rcv_per_shot[ishot]);
        }

#ifdef _OPENMP
#pragma omp barrier
#endif
        for (int ti = 0; ti < modeling->GetNs(); ti++)   // nearest-neighbor interpolation
        {
          ws->checkToken();
          modeling->ModelingStep(*src, *wf, ti, 1);
          modeling->ReadReceiver(*dobs, *wf, ti);
// Wait for swap at each time iteration
#ifdef _OPENMP
#pragma omp barrier
#endif
        }

#ifdef _OPENMP
#pragma omp single
#endif
        {
          My_MPI::SaveFile(FILENAME_OBSERVED_DATA, ishot, nsrc, comm_sz, id, dobs->data, dobs->nf,
                           sconfig->Get("proj_dir"));
        }

        ws->getTask(ishot);
      }   // While
    }   // _OPENMP

    delete dobs;
    delete src;
    delete location;
    delete ws;
    delete[] rcv_per_shot;
    delete grid;
    delete wf;
    delete modeling;
    delete model;
#ifdef MAMUTE_AUTOTUNING
    delete patsma;
#endif
#ifdef MAMUTE_DENSITY
    delete density;
#endif
    MPI_Finalize();

  } catch (std::exception &e) {
    std::cerr << "Exception caught: " << e.what() << std::endl;
    return -1;
  } catch (std::string &e) {
    std::cerr << "Exception caught: " << e << std::endl;
    return -1;
  } catch (const char *e) {
    std::cerr << "Exception caught: " << e << std::endl;
    return -1;
  } catch (...) {
    std::cerr << "Unknown error running FWI@main()!" << std::endl;
    return -1;
  }

  return 0;
}
