#ifndef _VELOCITYMODEL_H
#define _VELOCITYMODEL_H

#include <string>

#include "Model.h"

// File name of last velocity model
#define FILENAME_VELOCITY_FINAL "v-final.bin"
// File name of velocity model in the iteration `#`
#define FILENAME_VELOCITY_ITER "v-iter-#.bin"
/**
 *
 *  \brief The Velocity Model class conteiner information velocity model on
 * grid.
 */
class VelocityModel : public Model {
private:
  double fMin;   // minimum velocity
  double fMax;   // maximum velocity
  VelocityModel(const VelocityModel &model) = delete;

public:
  /**
   * Constructor
   * @param proj_dir
   * @param velocity_filename
   * @param grid
   */
  explicit VelocityModel(const std::string &proj_dir, const std::string &velocity_filename,
                         Grid &grid);

  void Print() const;
  void FindMaxMin();
  void ExpandModel(double dt);
  int getSizeWB() const { return fgrid.n; }   // get size with borders
  int getSize() const { return fgrid.on; }    // get size without borders
  double getVMin() const { return fMin; }
  double getVMax() const { return fMax; }
#ifdef VERBOSE
  // Function to save the velocity model at each iteration
  void SaveVModelIter(int iter_number, std::string projectPath);
#endif
  // Function to save the final velocity model
  void SaveVModelFinal(std::string projectPath);
  ~VelocityModel() {}
};
#endif
