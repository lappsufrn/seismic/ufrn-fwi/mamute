\page dev Developer Guide

# Prerequisites on supercomputers
<a id="supercomputers">

## NPAD

If using the provided python scripts, you also need to load Python module:
```
module load softwares/python/3.6-anaconda-5.0.1;
```
* You also need to install the python libraries, as described [here](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/overview.html#optional-requirements).
* you have to first load the Python library and then load the other softwares described bellow.

### Run on CPUs

```
module load softwares/git/2.11.0-gnu-4.4
module load softwares/cmake/3.6.2
module load compilers/gnu/8.3
module load libraries/openmpi/4.0.3-gnu-8.3
```

> ⚠️ IMPORTANT: Use the partition in `$SCRATCH_GLOBAL` to execute large scale problems. The folder `$HOME` is limited in space and the access is slow. Meanwhile, the `$SCRATCH_GLOBAL` supports a parallel filesystem with 60 Terabytes.


## SDumont

```
module load cmake/3.12
module load gcc/8.3
module load openmpi/gnu/4.0.1
```

If using the provided python scripts, you also need to load Python module:
```
module load python/3.6.9
```

> ⚠️ IMPORTANT: Use the partition in `$SCRATCH` to execute large scale problems.