\page dev Developer Guide

# Coding style for Mamute
<a id="coding-style">

To maintain code standardization, a clang-format file has been defined to ensure the code formatting is consistent. Additionally, a clang-tidy file has been implemented to assess the quality of the resulting code.

## Clang-tidy

"Clang-tidy" diagnoses and fixes typical programming errors, such as style violations or bugs that can be identified through static analysis. Make sure to address any clang-tidy errors before submitting a merge request.

## Clang-format

"Clang-format" will check the code formatting. You can configure your text editor to format the code according to the .clang-format file in the project root.

Ensure that you format all the changes before submitting a merge request; otherwise, the CI will detect errors.

A simple way to format all the code is by running the following command in project root:

```
find . \( -name '*.h' -o -name '*.cpp' -o -name '*.hpp' \) -type f -print0 | grep -zZvFf clang-format-exclude.txt | xargs -0 clang-format -i
```

Important: You will need to install `clang-format` version 12 or later.

## Python

To maintain code standardization in Python, we follow **PEP 8** guidelines to ensure consistent code formatting and style. To assist with this process, we utilize the following tools:

- **Flake8**: This tool checks for compliance with PEP 8 standards and highlights any style violations.

- **Black**: This is an automatic code formatter that reformats your Python code to adhere to PEP 8 conventions.

Ensure that you run Flake8 to check your code for any compliance issues and use Black to format your code before submitting a merge request.

### Tools

- **Flake8**: Use this tool to check compliance with PEP 8.

**Usage**:
```
flake8 scripts/
```

- **Black**: Use this tool to automatically format the code.

**Usage**:
```
black scripts/
```