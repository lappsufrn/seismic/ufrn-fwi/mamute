\page dev Developer Guide

# Folders organization
<a id="organization">

This tutorial details the main folders of the Mamute.

- PROJECTS

Keep the input files to execute the Modeling or the FWI.

- SCRIPTS

Keep the python scripts to generate or visualize the data of the Modeling or the FWI.

- SRC

Keep the C++ code of the Mamute.

- TEST

Keep the files input to the test to CI.

- DOC

Keep the documentations and tutorials about the use of the mamute.