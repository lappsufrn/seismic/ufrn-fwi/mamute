\page mamute Mamute

# Known issues
<a id="known-issues">

## Error: no such instruction

### Output

```
/tmp/cckx4TTL.s:2819: Error: suffix or operands invalid for `vbroadcastsd'
/tmp/cckx4TTL.s:2822: Error: no such instruction: `vpbroadcastd %xmm6,%ymm6'
```

### Solution

If you see errors like the ones above when running make, re-run `cmake` adding `-DNATIVE=OFF`.