\page mamute Mamute

# Quick start
<a id="quick-start">

### 1. Installation

Before installing Mamute, make sure your machine meets the software requirements presented [here](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/overview.html#optional-requirements).

If you are using either NPAD or SDumont supercomputers, there is [a tutorial for the modules you can load to attend Mamute's requirements](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/dev.html#supercomputers).

Now, clone the repository:

```sh
$ git clone https://gitlab.com/ufrn-fwi/mamute.git
$ cd mamute
```

Then, switch to the branch `dev`:

```sh
$ git checkout dev
```

Finally, you can follow the steps below to install Mamute. You can add a number after `-j` limiting the amount of cores during compilation.

```sh
$ mkdir build && cd build
$ cmake ../ -DCMAKE_INSTALL_PREFIX=../ -DTEST=ON -DVERBOSE=ON -DOPENMP=ON
$ make -j
$ make install
$ cd ..
```

That is it! You must be ready to use Mamute. If, however, you have seen any issue during the steps above, please check if it is a [known issues](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#known-issues).

### 2. Data input

Before running _modeling_ or _FWI_ on Mamute, you must place the binary input data files in your project folder. In this example, the project folder is `./projects/example`.

If you want to build your own input data, please see the specification of the [data format at this page](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#data-format). You also can use the scripts provided in Mamute to generate the input data of this example:

```sh
# Copy the configuration files of this tutorial to the folder './projects/example'
$ cp projects/quick-start/* projects/example/
# Save a ricker source at the binary file 'source.bin' with the values of fpeak, dt and ns defined in the ./projects/example/modeling.txt (fpeak=10, dt=0.001, ns=500 and amplitude=100000)
$ python ./scripts/generate_data/source.py ./projects/example/modeling.txt
# Plot source signature
$ python ./scripts/plot_data/plot_trace.py source.bin
# Create sources and receivers input files
$ python ./scripts/generate_data/rcv_scr.py ./projects/example/rcv_scr.json
# Plot the distribution of sources and receivers
$ python ./scripts/plot_data/plot_src_rcv.py src_coord.bin rcv_coord_0.bin
# Print the coordinates of sources and receivers
$ od -tfD rcv_coord_0.bin
$ od -tfD src_coord.bin
```

**Note:**
In the Python plotting scripts provided in the tutorial, you can use the optional -s flag at the end of the command to save the generated plot. Additionally, you can use -s <custom_name> to save the plot with a custom file name. For example:

```sh
$ python ./scripts/plot_data/plot_trace.py ./projects/example/dobs_0.bin -s
```
This will save the plot with the default name "trace".

```sh
$ python ./scripts/plot_data/plot_trace.py ./projects/example/dobs_0.bin -s my_plot
```
This will save the plot with the custom name my_plot.

---

The default distribution of sources and receivers should be as follows:

<p align="center">
  <img src="./img/acquisition_map.png" width="500" title="Default distribution of sources and receivers">
</p>

*If you want change the distribution of the sources and receivers see [Sources and receivers distribution](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#sources-receivers)*

```sh
# Create the velocity model defined by './projects/example/velocity_modeling.json' and use it when running 'modeling' with parameters defined in './projects/example/modeling.txt'
$ python ./scripts/generate_data/velocity.py ./projects/example/velocity_modeling.json ./projects/example/modeling.txt
# Plot the velocity model for the modeling
$ python ./scripts/plot_data/plot_3d.py 25 25 25 velocity_model.bin
```
The default velocity model should be as follows:

<p align="center">
  <img src="./img/velocity_modeling.png" width="500" title="Default velocity model">
</p>

```sh
# Create the velocity model defined by './projects/example/velocity_fwi.json' and use it to run 'FWI' with parameters defined in './projects/example/fwi.txt'
$ python ./scripts/generate_data/velocity.py ./projects/example/velocity_fwi.json ./projects/example/fwi.txt
# Plot the velocity model for the FWI
$ python ./scripts/plot_data/plot_3d.py 25 25 25 velocity_initial.bin
```
The default velocity model should be as follows:

<p align="center">
  <img src="./img/velocity_initial.png" width="500" title="Default velocity model">
</p>

*If you want change the velocity model see [Velocity Model](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#velocity-model)*

```sh
# Move the binary files to the project folder
$ mv *.bin projects/example/
```

You also must provide a configuration file in text format. The information present in this file should be set accordingly with the binary files you provided. You can use the configuration files available on Mamute in:

- `./projects/example/modeling.txt`, for modeling;
- and `./projects/example/fwi.txt`, for FWI.

Now, you are ready to run `modeling` and `FWI` on Mamute.

Before running Mamute, make sure your system will use one OpenMP thread per physical core. For that, run the following command in your terminal replacing `n` by the number of physical cores you have in your machine.

```sh
$ export OMP_NUM_THREADS=n
```

If you only want to try FWI, you can skip to [FWI section](#fwi).

### 3. Modeling

Running with 2 MPI ranks:

```sh
$ mpirun -n 2 ./bin/modeling ./projects/example/modeling.txt
```

Running on a single node:

```sh
$ ./bin/modeling ./projects/example/modeling.txt
```

The generated seismograms will be available in `./projects/example` as `dobs_i.bin` where $`i`$ is the shot id. You can use the following provided script to plot a single trace of a seismogram:

```sh
$ python ./scripts/plot_data/plot_trace.py ./projects/example/dobs_0.bin
```

### 4. FWI

Running with 2 MPI ranks:

```sh
$ mpirun -n 2 ./bin/fwi ./projects/example/fwi.txt
```

Running on a single node:

```sh
$ ./bin/fwi ./projects/example/fwi.txt
```

The final velocity model file will be available in `./projects/example/v-final.bin`. You can plot a 3D of this generated velocity model using the following provided script:
```sh
$ python ./scripts/plot_data/plot_3d.py 25 25 25 ./projects/example/v-final.bin
```

This script will open a web page in your default browser to display the 3D model. You can use the mouse to move, rotate, and zoom in/out the model.
