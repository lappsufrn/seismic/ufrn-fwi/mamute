\page mamute Mamute

# Velocity model
<a id="velocity-model">

If you want use another type of velocity model as input for the Mamute you can see the specification of the [data format at this page](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#data-format). Otherwise, you can choose three different types of synthetic velocity model using Mamute's script: constant velocity, a circle or a circle with gaussian perturbation. You just have to change a parameter in the configuration file.

The script uses two input files: the first is the configuration file that describes the velocity model. The second file is the configuration file for the Mamute. This tutorial uses the `./projects/example/modeling.txt` file, but you can use your own file.

The script also suggests a `dt` of the `velocity max` from the generated velocity model. You can use these information your configuration file for the Mamute.

## 1. Input file

In the input file configuration, you can set the type of the velocity model and its attributes.

How to execute:
```sh
$ python ./scripts/generate_data/velocity.py ./projects/example/velocity_modeling.json ./projects/example/modeling.txt
```

### 1.1. Constant velocity model

The input configuration file has only two parameters: the $type=constant$ and the value of the velocity.

```javascript
{
    "type" : "constant",
    "v" : 2500
}
```

This example, which you can name `constant-vm.json`, describes the following velocity model:

<p align="center">
  <img src="./img/vel_constant.png" width="1000" title="Constant velocity model">
</p>

To reproduce this plot, you can execute:
```sh
$ python ./scripts/generate_data/velocity.py constant-vm.json ./projects/example/modeling.txt
```

This script outputs:
```
------------------------------------------------------
Following the CFL(Courant–Friedrichs–Lewy) criteria!
The maximum value for the dt is 0.001470
when dx= 10.0  dz= 10.0 and dy= 10.0
with the maximum value for the velocity model = 2500.00
------------------------------------------------------
```

### 1.2. Velocity with circle

The input configuration file below has four parameters: the $type=circle$, the value of the velocity inside the circle, the value of the velocity outside the circle, and the radius of the circle.

`#vel_circle.json`
```javascript
{
    "type" : "circle",
    "v_in" : 2500,
    "v_out" : 3500,
    "r" : 50
}
```

This example, which you can name `circle-vm.json`, describes the following velocity model:

<p align="center">
  <img src="./img/vel_circle.png" width="1000" title="Velocity with circle"/>
</p>

To reproduce this plot, you can execute:
```sh
$ python ./scripts/generate_data/velocity.py circle-vm.json ./projects/example/modeling.txt
```

This script outputs:
```
------------------------------------------------------
Following the CFL(Courant–Friedrichs–Lewy) criteria!
The maximum value for the dt is 0.001050
when dx= 10.0  dz= 10.0 and dy= 10.0
with the maximum value for the velocity model = 3500.00
------------------------------------------------------
```

### 1.3. Velocity with gaussian pertubation

The input configuration file below has three parameters: the $type=gaussian_pertubation$, the value of gaussian standard deviation ($sigma$), and the value of the velocity inside the circle.

#vel_gaussian_pertubation.json
```javascript
{
    "type" : "gaussian_pertubation",
    "sigma" : 5,
    "v" : 2500
}
```

This example, which you can name `circle-g-vm.json`, describes the following velocity model:

<p align="center">
  <img src="./img/vel_gaussian_pertubation.png" width="1000" title="Velocity with gaussian pertubation">
</p>

To reproduce this plot, you can execute:
```sh
$ python ./scripts/generate_data/velocity.py circle-g-vm.json ./projects/example/modeling.txt
```

This script outputs:
```
------------------------------------------------------
Following the CFL(Courant–Friedrichs–Lewy) criteria!
The maximum value for the dt is 0.001050
when dx= 10.0  dz= 10.0 and dy= 10.0
with the maximum value for the velocity model = 3500.00
------------------------------------------------------
```
