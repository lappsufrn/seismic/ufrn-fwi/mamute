\page mamute Mamute

# Sources and receivers distribution
<a id="sources-receivers">

If you want to build your custom distribution of sources and receivers, please see the specification of the [data format at this page](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#data-format). Otherwise, you can choose from three different types of distribution using Mamute's script: a line, a 2D-mesh, or a 3D-mesh. You just have to change a parameter in the configuration file.

## 1. Input File

In the input configuration file, you can set for the receivers and sources as follows:
* First position in $`x`$, $`y`$ and $`z`$ .
* Last position in $`x`$, $`y`$ and $`z`$ .
* Delta among the elements in $`x`$, $`y`$ and $`z`$ .

How to execute:
```sh
$ python ./scripts/generate_data/rcv_scr.py ./projects/example/rcv_scr.json
```
Ps.: If you want to update the number of sources automatically in the configuration file of the Mamute, you just need to add the configuration file as in the example:
```sh
$ python ./scripts/generate_data/rcv_scr.py ./projects/example/rcv_scr.json ./projects/example/modeling.txt
$ python ./scripts/generate_data/rcv_scr.py ./projects/example/rcv_scr.json ./projects/example/fwi.txt
```

### 1.1. 3D-Mesh
In the input configuration file below, the first receiver is in the position $`x=1`$, $`y=2`$ and $`z=5`$. The next receiver is located in $`x=1+10=11`$, $`y=2+10=12`$ and $`z=5+10=15`$, since a $`delta`$ is added to the previous receiver position. All generated receivers follow this pattern. The script generates receivers until the $`last=30`$ value.

```javascript
{
    "receivers" : {
        "x": {
            "first": 1,
            "last" : 30,
            "delta": 10
        },
        "y": {
            "first": 2,
            "last" : 30,
            "delta": 10
        },
        "z": {
            "first": 5,
            "last" : 30,
            "delta": 10
        }
    },
    "sources" : {
        "x": {
            "first": 7,
            "last" : 30,
            "delta": 8
        },
        "y": {
            "first": 9,
            "last" : 30,
            "delta": 10
        },
        "z": {
            "first": 11,
            "last" : 30,
            "delta": 12
        }
    }
}
```
This example, which you can name `3d-mesh.json`, describes the following 3D-mesh:

<p align="center">
  <img src="./img/mesh3D.png" width="800" title="3D-Mesh" />
</p>

To reproduce this plot, you can execute:
```sh
$ python ./scripts/generate_data/rcv_scr.py 3d-mesh.json
$ python ./scripts/plot_data/plot_src_rcv.py src_coord.bin rcv_coord_0.bin
```

### 1.2. 2D-Mesh
You can generate a 2D-mesh when an axis has only the $`first`$ parameter. In the following example, the _y-axis_ has only $`first=2`$ parameter.

```javascript
{
    "receivers" : {
        "x": {
            "first": 1,
            "last" : 30,
            "delta": 10
        },
        "y": {
            "first": 2
        },
        "z": {
            "first": 5,
            "last" : 30,
            "delta": 10
        }
    },
    "sources" : {
        "x": {
            "first": 7,
            "last" : 30,
            "delta": 8
        },
        "y": {
            "first": 9
        },
        "z": {
            "first": 11,
            "last" : 30,
            "delta": 12
        }
    }
}
```
This example, which you can name `2d-mesh.json`, describes the following 2D-mesh:

<p align="center">
  <img src="./img/mesh2D.png" width="800" title="2D-Mesh"/>
</p>

### 1.3. Line
You can also generate a single line when two axis has only the $`first`$ parameter. You can see this setup in the following example:

```javascript
{
    "receivers" : {
        "x": {
            "first": 1,
            "last" : 30,
            "delta": 10
        },
        "y": {
            "first": 2
        },
        "z": {
            "first": 5
        }
    },
    "sources" : {
        "x": {
            "first": 7,
            "last" : 30,
            "delta": 8
        },
        "y": {
            "first": 9
        },
        "z": {
            "first": 11
        }
    }
}
```

This example, which you can name `1d-mesh.json`, describes the following 1D-mesh:

<p align="center">
  <img src="./img/mesh1D.png" width="800" title="Line" />
</p>

### 1.4. Different distribution of receivers and sources
The distribution of the receivers does not have to be the same as the sources. For example, you may have receivers spread out like a 2D-mesh and the sources in a line. You can see this configuration in the example below.

```javascript
{
    "receivers" : {
        "x": {
            "first": 1,
            "last" : 30,
            "delta": 10
        },
        "y": {
            "first": 2,
            "last" : 30,
            "delta": 10
        },
        "z": {
            "first": 5
        }
    },
    "sources" : {
        "x": {
            "first": 7,
            "last" : 30,
            "delta": 8
        },
        "y": {
            "first": 9
        },
        "z": {
            "first": 11
        }
    }
}
```

This example, which you can name `2Dr-1Ds-mesh.json`, describes the receivers and sources distribution as follows:

<p align="center">
  <img src="./img/mesh1D_2D.png" width="800" title="Different distribution of receivers and sources"/>
</p>
