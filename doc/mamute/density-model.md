\page mamute Mamute

# Density Modeling
<a id="density-model">

Density modeling utilizes the Acoustic Klein-Gordon wave equation to conduct acoustic wave modeling, incorporating density considerations. Density data must be provided as a binary file sized according to the grid. For specifications on the data format, refer to [this page](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#data-format). Alternatively, you can generate a density model from a velocity model binary file using Gardner's equation. In this scenario, generating the density model requires a velocity model binary file.

> For further insights into Gardner's equation:
> - Gardner, G.H.F.; Gardner L.W. & Gregory A.R. (1974). "Formation velocity and density -- the diagnostic basics for stratigraphic traps". Geophysics 39: 770–780. doi:10.1190/1.1440465.
> - [SEG Wiki - Dictionary: Gardner’s equation](https://wiki.seg.org/wiki/Dictionary:Gardner%E2%80%99s_equation).

## Generating Binary Density File Using Gardner's Equation

The script requires two input files: the first being the binary file of the velocity model, and the second being the configuration file for Mamute. This tutorial references the `./projects/example/modeling.txt` file, but you're free to utilize your own.

To execute:

```sh
$ python ./scripts/generate_data/density.py velocity_model.bin ./projects/example/modeling.txt
```

The script output is as follows:

```
-------------------------------------------------------------
Density model has been generated following Gardner's equation
-------------------------------------------------------------
```

To enable density modeling, include the flag -DDENSITY=ON during compilation with cmake. When compiling with -DDENSITY=ON, it is always necessary to generate a binary file containing the density information for the mamute to function. In other compilation scenarios, that is, when the flag -DDENSITY=OFF, this binary file is not needed. For a comprehensive list of flags, refer to [this page](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/overview.html#compilation-options).