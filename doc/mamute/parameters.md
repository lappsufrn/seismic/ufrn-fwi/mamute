\page mamute Mamute

# Parameters
<a id="parameters">

This tutorial details each parameter of the input file of the Mamute.

## Input file to the Modeling

Below is an example file:
```
nx=3
ny=3
nz=3
dx=1.0
dy=1.0
dz=1.0
border=1
ox=0.0
oy=0.0
oz=0.0
ns=500
dt=0.001
fpeak=10
amplitude=100
n_src=27
proj_dir=./projects/example
vel=velocity_model.bin
stencil=4
ws_flag=0
```
- `nx` refers to the number of points in the velocity model we have in the dimension $`x`$; `ny` refers to the dimension $`y`$, and `nz` to $`z`$;
- `dx` is the distance from one point to another in the $`x`$ dimension in *meters*; `dy` refers to the dimension $`y`$, and `dz` to $`z`$;
- `border` is the number of points of the border we are considering;
- `ox` is the coordinate of the origin in *meters* in $`x`$ dimension, `oy` in $`y`$ and `oz` in $`z`$;

In this example, the velocity model will have this configuration:

<p align="center">
  <img src="./img/velocity_config.png" width="500" title="Default distribution of sources and receivers">
</p>

- `ns` is the number of timesteps;
- `dt` is the time sampling in *seconds*;
- `fpeak`  is the peak frequency in *hertz*;
- `amplitude` is a constant factor that multiplies the source wavelet, in order to scale the source signature. amplitude=1 applies no scaling factor;

> `ns`, `dt`,`fpeak` and `amplitude`, should be the same of used to generate the source signature (`source.bin`). More details about Source signature in [Data-formathttps://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#data-format), an example how to generate it in [Quick start](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#quick-start).

- `n_src` is the number of sources (or shots);
- `proj_dir` is the path to the folder where the input binaries are located;
- `vel` is the name of the input velocity model data file used for modeling. More details about velocity model files in [Data-format](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#data-format), and examples how to generate a synthetic one in [Velocity Model](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#velocity-model);


> `vel` must be in binary file with double precision located in `proj_dir`.

> All binary files used as input to the *modeling* or *fwi* should be located in `proj_dir`.

- `ws_flag` is the flag used to active cyclic token-based work-stealing. More details  in [Workload Scheduling](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/hpc.html#schedulers);

## Input file to the FWI

Below is an example file:
```
nx=3
ny=3
nz=3
dx=1.0
dy=1.0
dz=1.0
border=1
ox=0.0
oy=0.0
oz=0.0
ns=500
dt=0.001
fpeak=10
amplitude=100
n_src=27
proj_dir=./projects/example
stencil=4
ws_flag=0
vel=velocity_initial.bin
ft_config=./projects/example/ft.json
fwi_config=./projects/example/fwi.txt
lbfgsb = 2
lbfgsb_lower_bound = 2000.0
lbfgsb_upper_bound = 3500.0
n_iter = 10
max_viter = 5
gradient_preconditioning_mode = 2
bessel_filter_lx = 40.0
bessel_filter_ly = 40.0
bessel_filter_lz = 40.0
check_mem = 0.8
chk_verb = 0
```

- `check_mem` is the value between (0,1] that limit usage memory by checkpointing. More details  in [Checkpointing](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/hpc.html#checkpointing);
- `chk_verb` is the checkpointing verbosity level. If `chk_verb=1` and cmake option `VERBOSE` is `ON`, checkpointing buffers will be written to a file `chkbuf.bin`. `chk_verb=0` (default) will avoid writing checkpointing buffers to a file.

- `nx`, `ny`, `nz`, `dx`, `dy`, `dz`, `border`, `ox`, `oy`, `oz`, `ns`, `dt`, `fpeak`, `amplitude`, `n_src`, `proj_dir`, `stencil` and `ws_flag` must be set as previously explained in the modeling tutorial;
- `vel` is the name of the velocity model used as the initial model to FWI. More details about velocity model format in [Data-format](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#data-format), and examples how to generate a synthetic one in [Velocity Model](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#velocity-model);

> The `vel` should be in binary and located in `proj_dir`.

- `ft_config` and `fwi_config` should be initialized to use the fault tolerance (DeLIA). `ft_config` corresponds to the file with DeLIA parameters, and `fwi_config` to the FWI parameters. *(OPTIONAL PARAMETER)*. More details in [Fault Tolerance](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/hpc.html#fault-tolerance).
- `lbfgsb` refers sets the boundary type in the LBFGS-b algorithm:
- - `lbfgsb=0` unbounded;
- - `lbfgsb=1` only lower bound; 
- - `lbfgsb=2` lower and upper bounds; 
- - `lbfgsb=3` only upper bound;
- `lbfgsb_lower_bound` is the lowest value (lower bound) that the velocity model can reach during the updates of the LBFGS-b;
- `lbfgsb_upper_bound` is the highest value (upper bound) that the velocity model can achieve during updates of the LBFGS-b;
- `n_iter` is the maximum number of iterations of the LBFGS-b *(OPTIONAL PARAMETER)*;
- `max_viter` is the maximum number of velocity model updates *(OPTIONAL PARAMETER)*;

> For more details about the used LBFGS-b algorithm, please see  Nocedal, J. (1980). Updating quasi-Newton matrices with limited storage. Mathematics of computation, 35(151), 773-782. https://doi.org/10.1090/S0025-5718-1980-0572855-7 

- `gradient_preconditioning_mode` defines which method will be used with the gradient to smooth the velocity model at the end of each iteration.
- - `gradient_preconditioning_mode=0` no preconditioning;
- - `gradient_preconditioning_mode=1` Bessel filter; 
- - `gradient_preconditioning_mode=2` Laplace filter; 
- `bessel_filter_lx` coherent lengths in the $`x`$ direction, `bessel_filter_ly` in $`y`$ and  `bessel_filter_lz` in $`z`$ *(OPTIONAL PARAMETERS)*;

If you choose to use the Bessel filter or Laplace filter, you can set the lengths in the $`x`$, $`y`$,and $`z`$ directions using the parameters `bessel_filter_lx`, `bessel_filter_ly` and `bessel_filter_lz`. Default values are $`lx=2*dx`$, $`ly=2*dy`$, and $`lz=2*dz`$.

> More details of gradient preconditioning in FWI are found in Butzer, S. (2015). 3D elastic time-frequency full-waveform inversion (Doctoral dissertation, Verlag nicht ermittelbar). https://doi.org/10.5445/IR/1000047328
