\page mamute Mamute

# Data format
<a id="data-format">

This tutorial details the format of input and output files for Mamute.
First, let us define some variables:
- `nsrc` is the number of shots;
- `i` is a shot index such that $`0 \leq i \leq nsrc-1`$;
- `ntr` is a vector containing the number of traces per shot, i.e., `ntr[i]` is the number of traces for the `i-th` shot;
- `ns` is the number of samples;
- `ox`, `oy`, and `oz` are the coordinates of the origin of the mesh grid;
- `dx`, `dy`, and `dz` are the mesh grid's spatial resolutions.

> All numerical data in the binary files must be provided as 64 bits (**8 bytes**) float.

## Output data

The output data of modelling is a set of seismograms following the format described [here](#Observed-data).

For an FWI, the main output data is a model called `v-final.bin`. Mamute also provides the output model for each FWI iteration as `v-iter-k.bin` where `k` is the iteration. Each of those models is saved in the file considering the following sequence of dimensions:  $`z \times y \times x`$.

## Setting up

Create a folder for your project and place your textual configuration file(s) in it. See [`projects/example/modeling.txt`](./../projects/example/modeling.txt) and [`projects/example/fwi.txt`](./../projects/example/fwi.txt) for examples of configuration files for modeling and FWI, respectively.

All coordinates in Mamute are in meters.

All input files must be in your project folder. Also, all outputs will be saved there as well.

## Receivers coordinates

One file per shot called `rcv_coord_i.bin` where `i` is the shot ID. Each file should contain the receivers' 3D coordinates following the order $`(x,y,z)`$. The order of the 3D coordinates should match the order of the traces in the observed data.

Let us say the seismic survey comprises two shots. 

Also, the receivers' coordinates for the first shot are: $`[\lparen 50.0,60.0,10.0 \rparen,\lparen 100.0,40.0,10.0 \rparen]`$ 

And for the second shot we have: $`[\lparen 70.0,55.0,10.0 \rparen,\lparen 145.0,200.0,10.0 \rparen, \lparen 95.0,20.0,10.0 \rparen ]`$

That means $`ntr = [2,3]`$ and the folder `data/input` must contain the files:
- `rcv_coord_0.bin` with size of $`ntr[0]*3*8 = 2*24 = 48`$ bytes;
- `rcv_coord_1.bin` with size of $`ntr[1]*3*8 = 3*24 = 72`$ bytes.

## Observed data

One file per shot called `dobs_i.bin` where `i` is the shot ID. The file content should be ordered by traces in the same order as the traces' coordinates in the receivers' coordinates file. That way, the first `ns*8` bytes in `dobs_i.bin` represent the `ns` samples of the first trace of shot `i`.

Let's say the seismic survey comprises three shots with 100, 200, and 300 traces, respectively. Also, `ns = 1000`. There must be the following three files in your project folder:
- `dobs_0.bin` with size of $`ntr[0]*ns*8 = 100*1000*8 = 8*10^5`$ bytes;
- `dobs_1.bin` with size of $`ntr[1]*ns*8 = 200*1000*8 = 16*10^5`$ bytes;
- and `dobs_2.bin` with size of $`ntr[2]*ns*8 = 300*1000*8 = 24*10^5`$ bytes.

The number of traces per shot is automatically computed from those files. This allows Mamute to work with different amounts of receivers per shot. 

## Source signature

One file called `source.bin` containing the `ns` samples of the source signature. Let `ns` be equal to 1250. There must be a file `source.bin` in your project folder with the size of $`ns*8 = 1250*8 = 10^4`$ bytes.

## Sources coordinates

One file called `src_coord.bin` containing one 3D coordinate per shot ordered by the shots IDs. Each 3D coordinate is represented in meters following the order $`(x,y,z)`$.

Let's say the positions of the shots are $`\[(10.0,20.0,50.0),(20.0,30.0,50.0),(30.0,40.0,50.0)\]`$. That means $`nsrc=3`$ and there must be a file `src_coord.bin` in your project folder with size of $`nsrc*3*8 = 3*24 = 72`$ bytes.

Please note that the number of shots is not computed automatically and should be informed in the configuration file.

## Velocity model

In addition to the already illustrated Mamute's input files, the code also supports initialization with a velocity model from a binary file. The velocity model name needs to be referenced in parameter `vel` in the input parameters file (example the `fwi.txt` or `modeling.txt` in `projects/example`).

The 3-dimensional velocity model must be saved in the file considering the following sequence of dimensions:  $`z \times y \times x`$. 

Mamute provides the generation of some synthetic velocity model examples. More details in [Velocity Model](https://lappsufrn.gitlab.io/seismic/ufrn-fwi/mamute/mamute.html#velocity-model)

## Density Model

In addition to the already illustrated input files, Mamute also supports initialization with a density model from a binary file. The density model file needs to be referenced in the density parameter in the configuration file [(click here for an example)](../projects/example/modeling.txt). The parameter must be mandatory when density modeling is performed. Otherwise, it is optional to provide it.

## Final considerations

As you may have noticed, all coordinates must be given in meters. The actual position they have in the grid is computed using the origin coordinate $`(ox,oy,oz)`$ and the spatial resolution $`(dx,dy,dz)`$.