\page hpc HPC Techniques

# Workload Scheduling
<a id="schedulers">

The workload scheduling *(the distribution of the tasks among the computational nodes)* is necessary for high-performance computing environments to guarantee that the nodes will have similar execution times, resulting in a short runtime. In Mamute, we have two options of workload scheduling: Decentralized static and Decentralized Dynamic (Cyclic token-based work-stealing-stealing).

In decentralized static (DS), the computation of the tasks (seismic shots is distributed among the supercomputer nodes. In cyclic token-based work-stealing-stealing (CTWS), the distribution of the tasks is initially similar to DS, but if a node becomes idle, it can steal the task of another node.

> More details about DS and CTWS:
> - Assis, Í. A., Oliveira, A. D., Barros, T., Sardina, I. M., Bianchini, C. P., & De-Souza, S. X. (2019). Distributed-memory load balancing with cyclic token-based work-stealing applied to reverse time migration. IEEE Access, 7, 128419-128430.
https://doi.org/10.1109/ACCESS.2019.2939100
> - Blumofe, R. D., & Leiserson, C. E. (1999). Scheduling Multithreaded Computations by Work Stealing. J. ACM, 46(5), 720–748. https://doi.org/10.1145/324133.324234

For using DS scheduling, the parameter `ws_flag` must be $`0`$. For using CTWS `ws_flag` must be $`1`$.