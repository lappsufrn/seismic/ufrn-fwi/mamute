\page hpc HPC Techniques

# Fault Tolerance
<a id="fault-tolerance">

Resilience is an essential feature of an application that requires significant computation. Dealing with interruptions is critical to running an application in some high-performance computing environments. An interruption may occur, for example, due to failures in the hardware or software, a time limit exceeded in the environment used, or execution in a preemptive environment.

An unrecoverable failure in FWI 3D can produce a meaningful financial impact, as it may take several days or weeks to recompute the lost data. Because of that, Mamute has fault tolerance using the [`DeLIA`](https://lappsufrn.gitlab.io/delia) library. By applying DeLIA, it is possible to recover the state of the execution at a time before the execution was interrupted. When the FWI runs with DeLIA and there is an interruption, it can be restarted using the same configuration and number of nodes.

To use this feature, you should :
* Compile with `-DFT=ON.` 
* The folder [`DeLIA_lib`](https://lappsufrn.gitlab.io/delia/config.html) should be in the folder mamute. 
* The parameters `ft_config` and `fwi_config` should be initialized to use the fault tolerance method. `ft_config` corresponds to the file with DeLIA parameters, and `fwi_config` with Mamute parameters. Example:

```
ft_config=./projects/example/ft.json
fwi_config=./projects/example/fwi.txt
```
* The file of `ft_config` should have the `DeLIA` parameters you want to use. ([More details about `DeLIA` parameters](https://lappsufrn.gitlab.io/delia/params.html)). Example:
```javascript
{
    "FT_FOLDER" : "./projects/delia_data",
    "CHECKPOINTING_GLOBAL_ITERATION": 1,
    "TRIGGER_SIGNAL" : true,
    "CHECKPOINTING_TIME_DIFF_LOCAL": 120,
    "WORKLOAD_SCHEDULER" : "DS",
    "TRIGGER_HEARTBEAT_MONITORING": {
        "TIME_MAX_WAIT" : 120,
        "SLEEP_THREAD_TIME" : 5
    }
}
```

> More details about DeLIA:
> * C. Santana et al., ‘DeLIA: A Dependability Library for Iterative Applications applied to parallel geophysical problems’, Computers & Geosciences, vol. 191, p. 105662, 2024.
> * DeLIA site: https://lappsufrn.gitlab.io/delia/index.html