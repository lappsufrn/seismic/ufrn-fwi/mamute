\page hpc HPC Techniques

# Checkpointing
<a id="checkpointing">

FWI computes the wavefield in forward and reverse time. We can reuse the forward data when computing the reverse wavefield; in these circumstances, a high amount of storage is needed. Otherwise, if we do not use the calculated data, we must recompute them again. To avoid saving the whole forward wavefield or recomputing it from the scratch, you can use optimal checkpointing to save selected timestamps from the forward wavefield and then recompute just the missing data.

> More details about Optimal checkpointing:
> - Symes, W. W. (2007). Reverse time migration with optimal checkpointing. GEOPHYSICS, 72(5), SM213–SM221. https://doi.org/10.1190/1.2742686
> - Griewank, A., & Walther, A. (2000). Algorithm 799: revolve: an implementation of checkpointing for the reverse or adjoint mode of computational differentiation. ACM Transactions on Mathematical Software. https://doi.org/10.1145/347837.347846

To use the optimal checkpointing method, use `-DWAVEFIELD_MANAGEMENT=CHECKPOINTING` as a `cmake` option.
It is possible to delimit how much memory the checkpoint can use. For this, `check_mem` must be set and should be $(0.0,1.0]$ where $1$ is $100\%$ of the memory. It is strongly recommended that `check_mem` be used below $0.85$ since the FWI has other memory allocations beyond the checkpoint. If the user does not define `check_mem`, the program will use the default value of $0.8$.