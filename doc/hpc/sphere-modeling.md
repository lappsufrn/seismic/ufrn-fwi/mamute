\page hpc HPC Techniques

# Sphere modeling
<a id="sphere-modeling">

A common issue when using modeling to collect data is the high computational cost, which requires a large amount of precise information. As problems become more complex, more operations and computational storage are also necessary.

Some techniques can use geometries to optimize operations. In this case, one proposed optimization method involves using sphere geometry for modeling. With this method, the coordinates of the upper and lower limits, as well as the radius of the sphere in the three-dimensional plane, are calculated. Although the Sphere method is currently used in modeling, it is not yet applicable to FWI.

This approach to the sphere method involves performing calculations within the `Sphere` method and applying them to the spatial loops. To use this approach, enabling the sphere modeling flag in the `cmake` is essential, as demonstrated in the following example:

```
$ cmake ... other parameters ... -DSPHERE
```