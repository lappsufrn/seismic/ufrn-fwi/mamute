\page hpc HPC Techniques

# Auto-tuning
<a id="autotuning">

Programs with high levels of complexity often face challenges in adjusting execution parameters, particularly when the ideal value for these parameters may change based on the execution context. These dynamic parameters significantly impact the program's performance.

Optimization techniques can employ meta-heuristics to explore the parameter search space, avoiding testing all possible solutions. Here, we propose a strategy to use the Parameter Auto-Tuning for Shared Memory Algorithms (PATSMA), with Coupled Simulated Annealing (CSA) as its optimization method, to automatically adjust the chunk size for the dynamic scheduling of wave propagation, one of the most expensive steps in FWI.

Our approach consists of running a PATSMA where the objective function is the execution time of the first iteration of the first seismic shot of the first iteration of the FWI. The resulting chunk size is then employed in all wave propagations involved in an FWI. Example:
```cpp
/**
 * This function starts the autotuning process by calling `wStart` with the `autotuning` parameter
 * and the `omp_chunk` value of the `modl` object. Then, it calls the `ModelingStep` function,
 * the method being optimized. Finally, close the
 * autotuning process by calling `wEnd` with the `autotuning` parameter.
 * 
 * @param autotuning: is the PASTMA instance.
 * @param omp_chunk: is the cost of the auto-tuning method.
 *
 */
wStart(autotuning, &(modl->omp_chunk));
    modl->ModelingStep(*src, *wf, 0, 0);
wEnd(autotuning);

// In Modeling.cpp:
#define OMP_SCHEDULE_AUTOTUNING schedule(dynamic, omp_chunk)

```

> More details about PATSMA:
> - J. B. Fernandes, F. H. Santos-da-Silva, T. Barros, I. A. S. Assis, and S. Xavier-de-Souza, ‘PATSMA: Parameter Auto-tuning for Shared Memory Algorithms’, SoftwareX, vol. 27, p. 101789, 2024..
> - da Silva, F. H. S., Fernandes, J. B., Sardina, I. M., Barros, T., Xavier-de-Souza, S., and Assis, I. A. S., “Auto Tuning for OpenMP Dynamic Scheduling applied to FWI”, <i>arXiv e-prints</i>, 2024. doi:10.48550/arXiv.2402.16728.
> - Í. A. S. Assis, J. B. Fernandes, T. Barros, and S. Xavier-De-Souza, ‘Auto-Tuning of Dynamic Scheduling Applied to 3D Reverse Time Migration on Multicore Systems’, IEEE Access, vol. 8, pp. 145115–145127, 2020.
> - PATSMA repository - https://gitlab.com/lappsufrn/patsma